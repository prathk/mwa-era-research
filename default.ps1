properties {
	$projectName = "ERA-Research"
	
	if(-not $version)
    {
        $version = "1.0.0.0"
    }
	
	if (-not $baseServer) {
		$baseServer = ".\sqlexpress"
	}

# paths
    $baseDir = resolve-path .\
    $sourceDir = "$baseDir\src\ERA-Research"
    $nugetDir = "$baseDir\tools\nuget"
    $dbDir = "$baseDir\src\Databases"	
    $se2files= "$baseDir\SE2 Files"
    $directory_name = "c:\SE2"

# database related properties
    $rhExe = "$baseDir\tools\roundhouse\rh.exe"
    $rhOutputDir = "$baseDir"
    $rhVersionFile = "$dbDir\_BuildInfo.xml"
    $dbServer = $baseServer
    $DbName = "MWA_ERA"

    $projectConfig = "Release"
}

# ---[ Root level tasks ]--------------


task default -depends Compile

task ResetDb -depends CopyTempFiles,DropExistingDb, MigrateDb

task VersionDb {
"<?xml version=""1.0"" ?>
<buildInfo>
  <projectName>USAC</projectName>
  <companyName>USAC</companyName>
  <version>$version</version>
</buildInfo>" | out-file "$rhVersionFile" -encoding "ASCII"
}


task MigrateDb {
   Migrate
}

task DropExistingDb{
    Drop
}

task CommonAssemblyInfo {
        $yearStamp = get-date -Format yyyy
    "using System.Reflection;
    using System.Runtime.InteropServices;

    [assembly: AssemblyVersionAttribute(""$version"")]
    [assembly: AssemblyFileVersionAttribute(""$version"")]
    [assembly: AssemblyCopyrightAttribute(""Copyright $yearStamp"")]
    [assembly: AssemblyProductAttribute(""UDB"")]
    [assembly: AssemblyCompanyAttribute(""Headspring"")]
    [assembly: AssemblyConfigurationAttribute(""$projectConfig"")]
    [assembly: AssemblyInformationalVersionAttribute(""$version"")]"  | out-file "$sourceDir\CommonAssemblyInfo.cs" -encoding "ASCII"

    create-commonAssemblyInfoVb "$version" $projectName "$sourceDir\CommonAssemblyInfo.vb"
}

task Compile {
    RunMsBuild
}

task CopyTempFiles{
    CopySE2Files
}

function CopySE2Files(){
        mkdir $directory_name  -ErrorAction SilentlyContinue  | out-null
        Get-ChildItem -Path "$se2files\*" | Copy-Item -dest $directory_name -force
}

function RunMsBuild() {
    # Restore NuGet packages, in case the packages have never been pulled before
    exec { & $nugetDir\nuget.exe restore $sourceDir\$projectName.sln }
    
    # Clean and compile
    exec { msbuild.exe /t:clean /v:q /m /p:Configuration=$projectConfig /nologo /nr:false $sourceDir\$projectName.sln /tv:14.0}
    exec { msbuild.exe /t:rebuild /v:q /m /p:Configuration=$projectConfig /nologo /nr:false $sourceDir\$projectName.sln /tv:14.0}
}

function Migrate {
    $presynonyms = "Presynonyms"
    Write-Host "Running database creation and any scripts that don't depend on synonyms to other databases"
    Roundhouse -server $dbServer -db $DbName -scriptDir "$dbDir\$presynonyms" -env "DEV"
}

function Drop{
    RoundhouseDrop -server $dbServer -db $DbName
}

function Roundhouse($server, $db, $scriptDir, $env) {
    # We are forcing the running of anytime scripts even without changes.
    # This is because there are views/procs that do "select *" that will not get new columns in tables unless they are recreated.
    # Rather than figure out what needs to be updated when adding a column to a table, just recreate all views/procs/functions each time.
    
    Write-Host "About to attempt:  $rhExe -s $server -d $db -f $scriptDir -racd $rhRacdDir -vf $rhVersionFile -o $rhOutputDir --runallanytimescripts --silent --env $env"
    exec { & $rhExe -s $server -d $db -f $scriptDir -racd $rhRacdDir -vf $rhVersionFile -o $rhOutputDir --runallanytimescripts --silent --env $env --commandtimeout 600 }
}

function RoundhouseDrop($server, $db) {

    Write-Host "About to attempt:  $rhExe -s $server -d $db --drop -o $rhOutputDir --silent"
    exec { &$rhExe -s $server -d $db --drop -o $rhOutputDir --silent }
}