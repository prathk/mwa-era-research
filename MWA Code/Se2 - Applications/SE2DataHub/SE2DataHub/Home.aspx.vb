﻿
Imports mwa.ITOps.DeveloperTools

Partial Class MasterPages_Default
    Inherits System.Web.UI.Page
    ' TODO: Change the ApplicationName Variable to reflect your application
    Private ApplicationName As String = "SE2_Data_Hub"
    ' TODO: Set WebSecurityAccess Enum to reflect your applications attributes
    Private Enum WebSecurityAccess
        readAccess = 1
        UpdateAccess = 2
        ' any other Access = 3
        Denied = 9
    End Enum
    Private UserName As String = ""
    Private UserAccess As WebSecurityAccess = WebSecurityAccess.Denied
    Private DeniedMsg As String = "<div style=""text-align: center;font-size: 90%;color: red;""><b>You are not authorized to access this application. </br> If you believe this is in error please contact the Help Desk.</b></div>"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Master.HTTP_BASE = System.Configuration.ConfigurationManager.AppSettings("HTTP_BASE").ToString()
        Master.APP_BASE = System.Configuration.ConfigurationManager.AppSettings("APP_BASE").ToString()
        Master.ServerLabel = System.Configuration.ConfigurationManager.AppSettings("ServerLabel").ToString()
        Try
            If Not Page.IsPostBack Then
                'Perform web security checking
                If WebSecChk() = False Then
                    ' Response.Clear()
                    Response.Write(DeniedMsg)
                    Response.End()
                End If
            End If
            Me.UserAccess = Me.ViewState.Item("UserAccess")
            Me.UserName = Me.ViewState.Item("UserName")

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Function WebSecChk() As Boolean
        Dim WebSecObj As WebSecurity.Service = Nothing
        Try
            Me.UserName = mwautils.GenericUtilities.UserName
            Me.ViewState.Item("UserName") = Me.UserName

            WebSecObj = New WebSecurity.Service
            '  WebSecObj.Url = mwautils.GenericUtilities.WebServiceBaseUrl & "prjWebSecurityWS/Service.asmx"
            WebSecObj.Url = ConfigurationManager.AppSettings("WebSecurity.Service")
            'TODO: Add calls to WebSecurityAccess to reflect your applications attributes
            If WebSecObj.UserAccess(ApplicationName, "", UserName) = False Then
                'Exit the function if the user does not have application access.
                Me.UserAccess = WebSecurityAccess.Denied
                Return False
            ElseIf WebSecObj.UserAccess(ApplicationName, "Update", UserName) Then
                Me.UserAccess = WebSecurityAccess.UpdateAccess
            Else
                Me.UserAccess = WebSecurityAccess.readAccess
            End If
            Me.ViewState.Add("UserAccess", Me.UserAccess)
            Return True
        Catch ex As Exception
            Throw
        Finally
            If Not IsNothing(WebSecObj) Then
                WebSecObj.Dispose()
                WebSecObj = Nothing
            End If
        End Try
    End Function

End Class
