﻿var app = app || {};



app.vm = (function () {



  //Helpers
  var hideFieldSets = function () {
    showTransactions(false);
    showMain(false);
    showBeneficiaries(false);
    showAbc(false);
    showMappings(false);
  };

  //Visibility
  var showInformation = ko.observable(false);
  var showSearchSpinner = ko.observable(false);
  var showTransactions = ko.observable(false);
  var showMain = ko.observable(false);
  var showBeneficiaries = ko.observable(false);
  var showAbc = ko.observable(false);
  var showAdditionalInsured = ko.observable(false);
  var showAdditionalOwners = ko.observable(false);
  var showMappings = ko.observable(false);

  var showMappingText = ko.computed(function () {

    if (showMappings()) {
      return "Hide Mappings";
    } else {
      return "Mappings";
    }

  });

  var hasLoadedTransactions = ko.observable(false);
  var hasLoadedBene = ko.observable(false);

  var searchCertNumber = ko.observable();
  var additionalOwnerText = ko.computed(function () {
    if (showAdditionalOwners() === true) {
      return "Hide Additional Owners";
    } else {
      return "Show Additional Owners";
    }
  });
  var additionalInsuredText = ko.computed(function () {
    if (showAdditionalInsured() === true) {
      return "Hide Additional Insured";
    } else {
      return "Show Additional Insured";
    }
  });

  //Transaction Filters
  var selectedTransType = ko.observable();
  var startPostedDate = ko.observable('');
  var endPostedDate = ko.observable('');
  var startEffectiveDate = ko.observable('');
  var endEffectiveDate = ko.observable('');
  var popOverTransType = ko.observable();

  var ClearFiltersClick = function () {
    selectedTransType('');
    startPostedDate('');
    endPostedDate('');
    startEffectiveDate('');
    endEffectiveDate('');
    $(".date").val('');
  };

  //Left Nav Button Clicks
  var newCertClick = function () {
    showInformation(false);
    searchCertNumber('');
    hasLoadedBene(false);
    hasLoadedTransactions(false);
  };
  var showTransactionClick = function () {
    hideFieldSets();
    GetTransactions();
    showTransactions(true);


  };
  var showMainClick = function () {
    hideFieldSets();
    showMain(true);
  };
  var showBeneficiariesClick = function () {
    hideFieldSets();
    GetBeneficiaries();
  };
  var showAbcClick = function () {
    hideFieldSets();
    showAbc(true);
  };
  var AdditionalOwnersClick = function () {
    showAdditionalOwners(!showAdditionalOwners());
  };
  var AdditionalInsuredClick = function () {
    showAdditionalInsured(!showAdditionalInsured());
  };
  var ShowMappingClick = function () {
    //window.open("http://portal.modern-woodmen.org/era/Test/Forms/AllItems.aspx?RootFolder=%2Fera%2FTest%2FSe2%20DataHub&FolderCTID=0x012000421684FB08CA8C47A8A0AA62513A6EAE&View={906C6244-09ED-4FCB-ACA8-0465C767F077}");

    showMappings(!showMappings());
  };
  var FilterOnTypeClick = function () {
    $("#aTypeFilter").popover("hide");
  };
  var ClearTypeFilterClick = function () {
    selectedTransType('');
    $("#aTypeFilter").popover("hide");
  };
  var FilterOnPostedDateClick = function () {
    $("#aPostedFilter").popover("hide");
  };
  var ClearOnPostedDateClick = function () {
    startPostedDate('');
    endPostedDate('');
    $("#aPostedFilter").popover("hide");
  };
  var hasPostedFilter = ko.computed(function () {
    if (!startPostedDate() && !endPostedDate()) {
      return false;
    } else {
      return true;
    }
  });
  var FilterOnEffectiveDateClick = function () {
    $("#aEffectiveFilter").popover("hide");
  };
  var ClearOnEffectiveDateClick = function () {
    startEffectiveDate('');
    endEffectiveDate('');
    $("#aEffectiveFilter").popover("hide");
  };
  var hasEffectiveFilter = ko.computed(function () {
    if (!startEffectiveDate() && !endEffectiveDate()) {
      return false;
    } else {
      return true;
    }
  });

  //DataModels
  var certInfo = app.data.CertInformation;
  var certTrans = app.data.CertTransactions;

  var certTypes = app.data.CertTransactionTypes;
  var certBene = app.data.CertBeneficiaries;
  var certAbc = app.data.CertAbc;


  //Get Data
  var GetCertInformation = function () {

    if (searchCertNumber() === undefined || searchCertNumber() === '' || searchCertNumber().length !== 7) {
      toastr.error("Please enter a valid certificate number");
    } else {

      var GetCertInfo = new app.data.GetCertificateInformation();
      GetCertInfo.getCert(searchCertNumber());

      //if (GetCertInfo.HasErrors() && GetCertInfo.HasErrors().length > 0) {
      //  ko.utils.arrayForEach(GetCertInfo.HasErrors(), function (error) {
      //    toastr.error(error);
      //  });
      //}
      //if (GetCertInfo.ShowMain()){
      //  hideFieldSets();
      //  showMain(true);
      //  showInformation(true);
      //}
    }

  };
  var GetTransactions = function () {
    if (!hasLoadedTransactions()) {
      var GetTransactionInfo = new app.data.GetTransactions();
      GetTransactionInfo.getTrans(searchCertNumber());
      hasLoadedTransactions(true);
      if (GetTransactionInfo.HasErrors()) {
        //Show Error Message
        toastr.error("Error occurred when retrieving transaction information");
      } else {
        try{
          certInfo().FileImportDates().TransImportedDate(certTrans()[0].transactionImportDate());
        }catch(err){
          certInfo().FileImportDates().TransImportedDate("N/A");
        }
        
      }
    }
  };
  var GetBeneficiaries = function () {

    if (!hasLoadedBene()) {
      var GetBeneficiaries = new app.data.GetBeneficiaries();
      GetBeneficiaries.getBene(certInfo().BaseCert().CertNumber().Value());

      hasLoadedBene(true);
      if (GetBeneficiaries.HasErrors() && GetBeneficiaries.HasErrors().length > 0) {
        hideFieldSets();
        ko.utils.arrayForEach(GetBeneficiaries.HasErrors(), function (error) {
          toastr.error(error);
        });
      }
    }
    showBeneficiaries(true);

  };
  var GetAbc = function () { };

  var filterTable = ko.computed(function(){

    if (!startPostedDate() && !endPostedDate() && !startEffectiveDate() && !endEffectiveDate() && !selectedTransType()) {
      return certTrans();
    } else {
      var filters = certTrans();

      if (startPostedDate() !== undefined && startPostedDate() !== '') {
        filters = ko.utils.arrayFilter(filters, function (item) {
          return moment(startPostedDate()) <= moment(item.postedDate());
        });
      }
      if (endPostedDate() !== undefined && endPostedDate() !== '') {
        filters = ko.utils.arrayFilter(filters, function (item) {
          return moment(endPostedDate()) >= moment(item.postedDate());
        });
      }

      if (startEffectiveDate() !== undefined && startEffectiveDate() !== '') {
        filters = ko.utils.arrayFilter(filters, function (item) {
          return moment(startEffectiveDate()) <= moment(item.effectiveDate());
        });
      }
      if (endEffectiveDate() !== undefined && endEffectiveDate() !== '') {
        filters = ko.utils.arrayFilter(filters, function (item) {
          return moment(endEffectiveDate()) >= moment(item.effectiveDate());
        });
      }

      if (selectedTransType() !== undefined && selectedTransType() !== '') {
        filters = ko.utils.arrayFilter(filters, function (item) {
          return selectedTransType() === item.type();
        });
      }
      return filters;

    }


  });

  var vm = {
    searchCertNumber: searchCertNumber,
    additionalOwnerText: additionalOwnerText,
    additionalInsuredText: additionalInsuredText,
    GetCertInformation: GetCertInformation,
    showMappingText:showMappingText,

    certInfo: certInfo,
    certTrans: certTrans,
    certBene: certBene,
    certAbc: certAbc,
    certTypes: certTypes,

    newCertClick: newCertClick,
    showTransactionClick: showTransactionClick,
    showMainClick: showMainClick,
    showBeneficiariesClick: showBeneficiariesClick,
    showAbcClick: showAbcClick,
    AdditionalOwnersClick: AdditionalOwnersClick,
    AdditionalInsuredClick: AdditionalInsuredClick,
    ClearFiltersClick: ClearFiltersClick,
    ShowMappingClick: ShowMappingClick,
    FilterOnTypeClick: FilterOnTypeClick,
    ClearTypeFilterClick: ClearTypeFilterClick,
    FilterOnPostedDateClick: FilterOnPostedDateClick,
    ClearOnPostedDateClick: ClearOnPostedDateClick,
    FilterOnEffectiveDateClick: FilterOnEffectiveDateClick,
    ClearOnEffectiveDateClick: ClearOnEffectiveDateClick,


    showInformation: showInformation,
    showSearchSpinner: showSearchSpinner,
    showMain: showMain,
    showTransactions: showTransactions,
    showBeneficiaries: showBeneficiaries,
    showAbc: showAbc,
    showAdditionalInsured: showAdditionalInsured,
    showAdditionalOwners: showAdditionalOwners,
    showMappings:showMappings,

    filterTable: filterTable,
    selectedTransType: selectedTransType,
    startPostedDate: startPostedDate,
    endPostedDate: endPostedDate,
    startEffectiveDate: startEffectiveDate,
    endEffectiveDate: endEffectiveDate,

    hasLoadedTransactions: hasLoadedTransactions,
    hasPostedFilter: hasPostedFilter,
    hasEffectiveFilter: hasEffectiveFilter,

    hideFieldSets: hideFieldSets

  };

  return vm;




})();


$(document).ready(function () {
  ko.applyBindings(app.vm);
});