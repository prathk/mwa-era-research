﻿var app = app || {};


app.data = (function () {

  var CertInformation = ko.observable();
  var CertTransactions = ko.observableArray();
  var CertTransactionTypes = ko.observableArray();
  var CertBeneficiaries = ko.observableArray([]);
  var CertAbc = ko.observable();

  var GetCertificateInformation = function () {
    var getCert = function (certNumber) {
      app.vm.showSearchSpinner(true);
      $.ajax({
        type: "POST",
        url: "Se2DataHub.asmx/GetCertInfo",
        contentType: "application/json; charset=utf-8",
        data: "{ 'aCertificateNumber': '" + certNumber + "'}",
        //async: false,
        dataType: "json"
      }).done(function (jsonObject) {
        if (jsonObject.d !== null && jsonObject.d !== undefined) {
          var noPolicyFile = false;
          if (jsonObject.d.ErrorMessage !== null && jsonObject.d.ErrorMessage !== undefined && jsonObject.d.ErrorMessage.length > 0) {
            $.each(jsonObject.d.ErrorMessage, function (key, value) {
              if (value === 'No current records in the Policy File') {
                noPolicyFile = true
              }
              toastr.error(value);
            });
          }
          if (!noPolicyFile){
            LoadModels(jsonObject.d);
            app.vm.hideFieldSets();
            app.vm.showMain(true);
            app.vm.showInformation(true);
          } 
        }
        app.vm.showSearchSpinner(false);
      }).fail(function (error, errorType, xhr) {
        toastr.error(error.responseJSON.Message);
        app.vm.showSearchSpinner(false);
      });


    };

    return {
      getCert: getCert
    };

    function LoadModels(jsonObject) {

      if (jsonObject !== undefined && jsonObject !== null) {

        var insuredInformation = LoadInsuredInformation(jsonObject);
        var ownerInformation = LoadOwnerInformation(jsonObject);
        var baseCert = LoadBaseCertInfo(jsonObject);
        var cashvalue = LoadCashValue(jsonObject);
        var death = LoadDeathBenefit(jsonObject);
        var loan = LoadLoan(jsonObject);
        var certOpt = LoadCertOptions(jsonObject);
        var gain = LoadGain(jsonObject);
        var billing = LoadBilling(jsonObject);
        var withdrawal = LoadWithdrawal(jsonObject);
        var payor = LoadPayer(jsonObject);
        var riders = LoadRiders(jsonObject);
        var contributions = LoadContributions(jsonObject);
        var importDate = LoadFileImportedDates(jsonObject);

        CertInformation('');
        CertInformation(new app.CertInfo(insuredInformation, ownerInformation, baseCert, cashvalue, death, loan, certOpt, gain, billing, withdrawal, payor, riders, contributions, importDate));

      }



    }


  };

  var GetTransactions = function () {
    var HasErrors = ko.observable(false);
    CertTransactions([]);
    CertTransactionTypes([]);
    var getTrans = function (certNumber) {
      $.ajax({
        type: "POST",
        url: "Se2DataHub.asmx/GetTransactions",
        contentType: "application/json; charset=utf-8",
        data: "{ 'aCertificateNumber': '" + certNumber + "'}",
        async: false,
        dataType: "json"
      }).done(function (jsonObject) {
        LoadTransactions(jsonObject.d);
      }).fail(function (error, errorType, xhr) {
        HasErrors(true);
      });
    };

    return {
      getTrans: getTrans,
      HasErrors: HasErrors
    };

    function LoadTransactions(jsonObject) {
      CertTransactions([]);
      CertTransactionTypes([]);
      if (jsonObject !== undefined && jsonObject !== null ){

        if (jsonObject.TypeList !== null && jsonObject.TypeList !== undefined) {
          $.each(jsonObject.TypeList, function (key, value) {
            CertTransactionTypes.push(value);
          });
        }

        if (jsonObject.TransactionItems !== null && jsonObject.TransactionItems !== undefined) {
          $.each(jsonObject.TransactionItems, function (key, value) {
            CertTransactions.push(new app.Transactions(value.TransactionId, value.Type, value.PostedDate, value.EffectiveDate, value.ReversalDate, value.Amount, jsonObject.TransactionImportDate));
          });
        }

      }
    }

  };

  var GetBeneficiaries = function () {
    var HasErrors = ko.observableArray([]);

    var getBene = function (certNumber) {
      HasErrors([]);
      $.ajax({
        type: "POST",
        url: "Se2DataHub.asmx/GetBeneficiaries",
        contentType: "application/json; charset=utf-8",
        data: "{ 'aCertificateNumber': '" + certNumber + "'}",
        async: false,
        dataType: "json"
      }).done(function (jsonObject) {
        CertBeneficiaries([]);
        if (jsonObject.d !== null && jsonObject.d !== undefined && jsonObject.d.length > 0) {

          if (jsonObject.d.ErrorMessage !== null && jsonObject.d.ErrorMessage !== undefined) {
            $.each(jsonObject.d.ErrorMessage, function (key, value) {
              HasErrors.push(value);
            });
          }
          LoadBeneficiaries(jsonObject.d);
        }

      }).fail(function (error, errorType, xhr) {
        HasErrors.push(xhr);
      });


    };

    return {
      getBene: getBene,
      HasErrors: HasErrors
    };

    function LoadBeneficiaries(jsonObject) {

      $.each(jsonObject, function (key, value) {
        CertBeneficiaries.push(new app.Beneficiaries(value.Name, value.Address, value.Percent,value.Type));
      });
    }

  };

  return {
    CertInformation: CertInformation,
    GetCertificateInformation: GetCertificateInformation,
    GetTransactions: GetTransactions,
    GetBeneficiaries: GetBeneficiaries,
    CertTransactions: CertTransactions,
    CertTransactionTypes: CertTransactionTypes,
    CertBeneficiaries: CertBeneficiaries,
    CertAbc: CertAbc
  };
})();

function LoadFileImportedDates(jsonObject) {
  var tempNull = { value: '', dbfield: '' }
  var importDate = new app.FileImportDates(jsonObject.PolicyImportedDate, jsonObject.DailyMonthlyImportedDate, jsonObject.LoanImportedDate, jsonObject.RolesImportedDate, tempNull);


  return importDate;
}

function LoadInsuredInformation(jsonObject) {
  var insured = jsonObject.InsuredInformation
  var insuredInformation;
  if (insured !== undefined && insured !== null){
    insuredInformation = new app.InsuredInformation(insured.Name, insured.Dob, insured.Gender);
  } else {
    insured = { value: '', dbfield: '' }
    insuredInformation = new app.InsuredInformation(insured, insured, insured, insured, insured)
  }

  if (jsonObject.AdditionalInsureds !== null) {
    $.each(jsonObject.AdditionalInsureds, function (key, value) {
      var additional = new app.AdditionalInsured(value.Name,value.Dob,value.Address,value.SSN,value.Gender,value.Phone);
      insuredInformation.AdditionalInsured.push(additional);
    });
  }

  return insuredInformation;

}

function LoadOwnerInformation(jsonObject) {
  var ownerInformation
  if (jsonObject.OwnerInformation !== undefined && jsonObject.OwnerInformation !== null) {
    var owner = jsonObject.OwnerInformation
    ownerInformation = new app.OwnerInformation(owner.Name, owner.SSN, owner.Phone, owner.Address, owner.Gender);
  } else {
    var owner = { value: '', dbfield: '' }
    ownerInformation = new app.OwnerInformation(owner, owner, owner, owner, owner)
  }

  if (jsonObject.AdditionalOwners !== undefined && jsonObject.AdditionalOwners !== null) {
    $.each(jsonObject.AdditionalOwners, function (key, value) {
      var additional = new app.AdditionalOwners(value.Name,value.RoleDescription,value.Address,value.SSN,value.Gender,value.Phone);
      ownerInformation.AdditionalOwners.push(additional);
    });
  }

  return ownerInformation;

}

function LoadBaseCertInfo(jsonObject) {
  var baseCert = new app.BaseCertInfo(jsonObject.CertNumber, jsonObject.Status, jsonObject.InsuranceAmount, jsonObject.IssueAge, jsonObject.IssueDate, jsonObject.MaturityDate, jsonObject.GovernmentStatus, jsonObject.PlanCode, jsonObject.PlanDescription, jsonObject.Type, jsonObject.RatingClass);
  return baseCert;
}

function LoadCashValue(jsonObject) {

  var interest;
  if (jsonObject.Interest !== undefined && jsonObject.Interest !== null) {
    interest = jsonObject.Interest.CurrentInterestRates;
  } else {
    interest = jsonObject.CurrentInterestRates;
  }


  var cashValue = new app.CashValue(jsonObject.GuaranteedCashValue, jsonObject.Dividend, jsonObject.TotalCashValue, jsonObject.SurrenderCharge, jsonObject.NetSurrenderValue, jsonObject.CashValue,interest,jsonObject.YearEndCashValue,jsonObject.SurrenderValue);
  return cashValue;
}

function LoadDeathBenefit(jsonObject) {

  var pua = {value: '', dbfield: ''}
  if (jsonObject.Dividend !== undefined && jsonObject.Dividend !== null) {
    pua = jsonObject.Dividend.PuaFaceAmount;
  }

  var death = new app.DeathBenefit(jsonObject.DeathBenefit, pua);

  return death;
}

function LoadLoan(jsonObject) {
  var loan = new app.Loan(jsonObject.MaxLoan, jsonObject.LoanBalance, jsonObject.LoanInterestRate);
  return loan;
}

function LoadCertOptions(jsonObject) {
  var certOption = new app.CertOptions(jsonObject.NonForfeitureOption, jsonObject.RpuAmount, jsonObject.EiAmount, jsonObject.EiDate);
  return certOption;
}

function LoadGain(jsonObject) {
  var arr = {value: '', dbfield: ''}
  var gain = new app.Gain(arr, arr, arr, arr, jsonObject.PwGain, jsonObject.CsGain);
  return gain;
}

function LoadBilling(jsonObject) {
  var arr = { value: '', dbfield: '' }
  var billing = new app.Billing(jsonObject.Mode, jsonObject.LastPaidDate,jsonObject.PaidToDate, arr, jsonObject.LastPaidAmount, jsonObject.BilledAmount);
  return billing;
}

function LoadWithdrawal(jsonObject) {

  var withdrawal = new app.Withdrawal(jsonObject.MaxWithdrawal, jsonObject.CurrentWithdrawals, jsonObject.YtdWithdrawals,jsonObject.WithdrawalCharge);
  return withdrawal;
}

function LoadPayer(jsonObject) {
  var payor;
  var arr = { value: '', dbfield: '' }
  if (jsonObject.Payor !== null && jsonObject.Payor !== undefined){
    payor = new app.Payor(jsonObject.Payor.Name, jsonObject.Payor.Address);
  } else {
    var payor = new app.Payor(arr, arr);
  }

  return payor;
}

function LoadRiders(jsonObject) {
  var riders = new app.Riders(jsonObject.AdRider, jsonObject.AdAmount, jsonObject.WpRider, jsonObject.PayorBenefitRider, jsonObject.TermRider, jsonObject.TermRiderAmount);
  return riders;
}

function LoadContributions(jsonObject) {
  var contributions = new app.Contributions(jsonObject.CurrentContributions, jsonObject.YtdContributions, jsonObject.CurrentTaxContributions, jsonObject.PrevTaxContributions);
  return contributions;
}


