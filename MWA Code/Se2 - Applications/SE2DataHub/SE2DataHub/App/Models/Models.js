﻿var app = app || {};

app.CertInfo = function (Insured, Owner, BaseCert, CashValue, Death, Loan, CertOptions, Gain, Billing, Withdrawal, Payor, Riders, Contributions, FileImports) {
  var self = this;

  self.Insured = ko.observable(Insured);
  self.Owner = ko.observable(Owner);
  self.BaseCert = ko.observable(BaseCert);
  self.CashValue = ko.observable(CashValue);
  self.Death = ko.observable(Death);
  self.Loan = ko.observable(Loan);
  self.CertOptions = ko.observable(CertOptions);
  self.Gain = ko.observable(Gain);
  self.Billing = ko.observable(Billing);
  self.Withdrawal = ko.observable(Withdrawal);
  self.Payor = ko.observable(Payor);
  self.Riders = ko.observable(Riders);
  self.Contributions = ko.observable(Contributions);
  self.FileImportDates = ko.observable(FileImports);

};

app.FileImportDates = function (policy, dailyMonthly, loan, roles, trans) {
  var self = this;

  self.PolicyImportedDate = ko.observable(new app.ValueDbField(policy));
  self.DailyMonthlyImportedDate = ko.observable(new app.ValueDbField(dailyMonthly));
  self.LoanImportedDate = ko.observable(new app.ValueDbField(loan));
  self.RolesImportedDate = ko.observable(new app.ValueDbField(roles));
  self.TransImportedDate = ko.observable(new app.ValueDbField(trans));
};

app.InsuredInformation = function (name,dob,gender) {
  var self = this;
  self.InsuredName = ko.observable(new app.ValueDbField(name));
  self.InsuredDOB = ko.observable(new app.ValueDbField(dob));
  self.InsuredGender = ko.observable(new app.ValueDbField(gender));
  self.AdditionalInsured = ko.observableArray();
  self.Type = ko.observable();
  self.LabelType = ko.computed(function () {
    if (self.Type() !== 'Annuity') {
      return "Annuitant";
    } else {
      return "Insured";
    }
  });
  self.HasAdditional = ko.computed(function () {
    if (self.AdditionalInsured().length > 0) {
      return true;
    } else {
      return false;
    }

  });
};

app.AdditionalInsured = function (name,dob,address,ssn,gender,phone) {
  var self = this;
  self.Name = ko.observable(new app.ValueDbField(name));
  self.DOB = ko.observable(new app.ValueDbField(dob));
  self.Address = ko.observable(new app.ValueDbField(address));
  self.SSN = ko.observable(new app.ValueDbField(ssn));
  self.Gender = ko.observable(new app.ValueDbField(gender));
  self.Phone = ko.observable(new app.ValueDbField(phone));
};

app.OwnerInformation = function (name,ssn,phone,address,gender) {
  var self = this;
  self.OwnerName = ko.observable(new app.ValueDbField(name));
  self.OwnerSSN = ko.observable(new app.ValueDbField(ssn));
  self.OwnerPhone = ko.observable(new app.ValueDbField(phone));
  self.OwnerAddress = ko.observable(new app.ValueDbField(address));
  self.Gender = ko.observable(new app.ValueDbField(gender));
  self.AdditionalOwners = ko.observableArray();
  self.HasAdditional = ko.computed(function () {
    if (self.AdditionalOwners().length > 0) {
      return true;
    } else {
      return false;
    }

  });

};

app.AdditionalOwners = function (name,type,address,ssn,gender,phone,ratingClass,dob) {
  var self = this;
  self.Name = ko.observable(new app.ValueDbField(name));
  self.Type = ko.observable(new app.ValueDbField(type));
  self.Address = ko.observable(new app.ValueDbField(address));
  self.SSN = ko.observable(new app.ValueDbField(ssn));
  self.Gender = ko.observable(new app.ValueDbField(gender));
  self.Phone = ko.observable(new app.ValueDbField(phone));
  self.RatingClass = ko.observable(new app.ValueDbField(ratingClass));
  self.DOB = ko.observable(new app.ValueDbField(dob));
};

app.BaseCertInfo = function (certNum, status,amount,age,issueDate,matDate,govStatus,planCode,planDesc,certType,ratingClass) {
  var self = this;
  self.CertNumber = ko.observable(new app.ValueDbField(certNum));
  self.Status = ko.observable(new app.ValueDbField(status));
  self.InsuranceAmount = ko.observable(new app.ValueDbField(amount));
  self.IssueAge = ko.observable(new app.ValueDbField(age));
  self.IssueDate = ko.observable(new app.ValueDbField(issueDate));
  self.MaturityDate = ko.observable(new app.ValueDbField(matDate));
  self.GovernmentStatus = ko.observable(new app.ValueDbField(govStatus));
  self.PlanCode = ko.observable(new app.ValueDbField(planCode));
  self.PlanDescription = ko.observable(new app.ValueDbField(planDesc));
  self.PlanInformation = ko.computed(function () {
    return self.PlanCode().Value() + " " + self.PlanDescription().Value();
  });

  self.IsAnnuity = ko.observable(false);
  self.IsDividend = ko.observable(false);
  self.IsInterest = ko.observable(false);
  self.IsTerm = ko.observable(false);
  self.Type = ko.observable();
  self.RatingClass = ko.observable(new app.ValueDbField(ratingClass));
  self.ImportedDate = ko.observable();

  self.CertType = ko.computed(function () {
    if (certType === 0) {
      self.IsAnnuity(true);
      self.Type("Annuity");
    } else if (certType === 1) {
      self.IsDividend(true);
      self.Type("Dividend Life");
    } else if (certType === 2) {
      self.IsInterest(true);
      self.Type("Interest Life");
    } else if (certType === 3) {
      self.IsTerm(true);
      self.Type("Term Life");
    }
    return certType;
  });

  self.CertType.subscribe(function (newValue) {
    
  });
  self.InsuredAnnuitant = ko.computed(function () {
    if (self.IsAnnuity()) {
      return "Annuitant";
    } else {
      return "Insured";
    }
  });

};

app.CashValue = function (guarCashValue,dividend,totalCV,surrCharge,netSurrVal, cv,currentIR,yearCV,surrVal) {
  var self = this;

  self.GuaranteedCashValue = ko.observable(new app.ValueDbField(guarCashValue));

  if (dividend !== undefined && dividend !== null) {

    self.DividendCashValue = ko.observable(new app.ValueDbField(dividend.DividendCashValue));
    self.BaseDividend = ko.observable(new app.ValueDbField(dividend.BaseDividend));
    self.DividendOption = ko.observable(new app.ValueDbField(dividend.DividendOption));
  } else {
    var nullArray = { value: '', dbfield: '' }
    self.DividendCashValue = ko.observable(new app.ValueDbField(nullArray));
    self.BaseDividend = ko.observable(new app.ValueDbField(nullArray));
    self.DividendOption = ko.observable(new app.ValueDbField(nullArray));
  }
  self.TotalCashValue = ko.observable(new app.ValueDbField(totalCV));
  self.SurrenderCharge = ko.observable(new app.ValueDbField(surrCharge));
  self.NetSurrenderValue = ko.observable(new app.ValueDbField(netSurrVal));
  self.CashValue = ko.observable(new app.ValueDbField(cv));
  self.CurrentInterestRate = ko.observable(new app.ValueDbField(currentIR));
  self.YearEndCashValue = ko.observable(new app.ValueDbField(yearCV));
  self.SurrenderValue = ko.observable(new app.ValueDbField(surrVal));

};

app.DeathBenefit = function (db, puaFaceAmount) {
  var self = this;
  self.DeathBenefit = ko.observable(new app.ValueDbField(db));
  self.PuaFaceAmount = ko.observable(new app.ValueDbField(puaFaceAmount));
};

app.Loan = function (maxLoan,loanBal, loanRate) {
  var self = this;
  self.MaxLoan = ko.observable(new app.ValueDbField(maxLoan));
  self.LoanBalance = ko.observable(new app.ValueDbField(loanBal));
  self.LoanRate = ko.observable(new app.ValueDbField(loanRate));
};

app.CertOptions = function (nonForOpt,rpuAmount,eiAmount,eiDate) {
  var self = this;
  self.NonForfeitureOptions = ko.observable(new app.ValueDbField(nonForOpt));
  self.RpuAmount = ko.observable(new app.ValueDbField(rpuAmount));
  self.EiAmount = ko.observable(new app.ValueDbField(eiAmount));
  self.EiDate = ko.observable(new app.ValueDbField(eiDate));
};

app.Gain = function (pwGainNq,finalGainNq,csPwGainRoth,deathGainRoth,pwGain,csGain) {
  var self = this;
  self.PwGainNQ = ko.observable(new app.ValueDbField(pwGainNq));
  self.FinalGainNQ = ko.observable(new app.ValueDbField(finalGainNq));
  self.CsPwGainRoth = ko.observable(new app.ValueDbField(csPwGainRoth));
  self.DeathGainRoth = ko.observable(new app.ValueDbField(deathGainRoth));
  self.PwGain = ko.observable(new app.ValueDbField(pwGain));
  self.CsGain = ko.observable(new app.ValueDbField(csGain));
};

app.Billing = function (mode,lastPaidDate,paidToDate,expectAnnual,lastPaidAmount,billedAmount) {
  var self = this;
  self.Mode = ko.observable(new app.ValueDbField(mode));
  self.LastPaidDate = ko.observable(new app.ValueDbField(lastPaidDate));
  self.PaidToDate = ko.observable(new app.ValueDbField(paidToDate));
  self.ExpectedAnnual = ko.observable(new app.ValueDbField(expectAnnual));
  self.LastPaidAmount = ko.observable(new app.ValueDbField(lastPaidAmount));
  self.BilledAmount = ko.observable(new app.ValueDbField(billedAmount));
};

app.Withdrawal = function (maxWith,currentWith,ytdWith,withCharge) {
  var self = this;
  self.MaxWithdrawal = ko.observable(new app.ValueDbField(maxWith));
  self.CurrentWithdrawals = ko.observable(new app.ValueDbField(currentWith));
  self.YtdWithdrawals = ko.observable(new app.ValueDbField(ytdWith));
  self.WithdrawalCharge = ko.observable(new app.ValueDbField(withCharge));
};

app.Payor = function (name,address) {
  var self = this;
  self.PayorName = ko.observable(new app.ValueDbField(name));
  self.PayorAddress = ko.observable(new app.ValueDbField(address));
};

app.Riders = function (adRider,adAmount,wpRider,payorBene,termRider,termRiderAmt) {
  var self = this;
  self.AdRider = ko.observable(adRider);
  self.AdAmount = ko.observable(new app.ValueDbField(adAmount));
  self.WpRider = ko.observable(new app.ValueDbField(wpRider));
  self.PayorBenefit = ko.observable(new app.ValueDbField(payorBene));
  self.TermRider = ko.observable(new app.ValueDbField(termRider));
  self.TermRiderAmount = ko.observable(new app.ValueDbField(termRiderAmt));
};

app.Contributions = function (currentContr,ytdContr,currentTax,previousTax) {
  var self = this;
  self.CurrentContributions = ko.observable(new app.ValueDbField(currentContr));
  self.YtdContributions = ko.observable(new app.ValueDbField(ytdContr));
  self.CurrentTaxYearContributions = ko.observable(new app.ValueDbField(currentTax));
  self.PreviousTaxYearContributions = ko.observable(new app.ValueDbField(previousTax));
};

app.Transactions = function (id, type, postedDate, effectiveDate, reversalDate, amount, transactionImportDate) {
  var self = this;
  self.id = ko.observable(id);
  self.type = ko.observable(type);
  self.postedDate = ko.observable(moment(postedDate).format('MM/DD/YYYY'));
  self.effectiveDate = ko.observable(moment(effectiveDate).format('MM/DD/YYYY'));
  if (reversalDate !== null) {
    self.reversalDate = ko.observable(reversalDate);
  } else {
    self.reversalDate = ko.observable('');
  }
  self.amount = ko.observable(amount);
  self.transactionImportDate = ko.observable(new app.ValueDbField(transactionImportDate));
};

app.Beneficiaries = function (name, address, percent, type) {
  var self = this;
  self.Name = ko.observable(new app.ValueDbField(name));
  self.Address = ko.observable(new app.ValueDbField(address));
  self.Percent = ko.observable(new app.ValueDbField(percent));
  self.Type = ko.observable(type);
};


app.ValueDbField = function(valueField){
  var self = this;
  if (valueField !== null && valueField !== undefined){
    self.Value = ko.observable(valueField.value);
    self.DbField = ko.observable(valueField.dbField);
  } else {
    self.Value = ko.observable('');
    self.DbField = ko.observable('');
  }
}
