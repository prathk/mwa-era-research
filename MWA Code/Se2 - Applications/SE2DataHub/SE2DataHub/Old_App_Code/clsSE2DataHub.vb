﻿Imports System.Data
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System
Imports mwa.DataAccess.Common

Public Class clsSE2DataHub
    Inherits System.Web.UI.UserControl
    Dim VariableAnnuityCs As New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("VariableAnnuityConnectionString").ConnectionString)
    Dim AcquisitionsCs As New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("AcquisitionsConnectionString").ConnectionString)
    Dim ERACs As New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ERAConnectionString").ConnectionString)
#Region "Format Values"

    ''' <summary>
    ''' Change the mode from the code to the description
    ''' </summary>
    ''' <param name="mode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function formatMode(ByVal mode As String) As String
        Try
            If Not (mode Is Nothing) Then
                Select Case mode
                    Case "$"
                        Return "Default"
                    Case "A"
                        Return "Annual"
                    Case "B"
                        Return "Bi-Monthly"
                    Case "M"
                        Return "Monthly"
                    Case "Q"
                        Return "Quarterly"
                    Case "S"
                        Return "Semi-Annual"
                    Case "SA"
                        Return "Semi-Annual"
                    Case "W"
                        Return "Weekly"
                    Case "BW"
                        Return "Bi-Weekly"
                    Case Else
                        Return "Contact se2"
                End Select
            End If
            Return "Contact se2"
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function formatSSN(ByVal ssn As String) As String

        If Not IsNothing(ssn) Then

            Return Left(ssn, 3) & "-" & Mid(ssn, 4, 2) & "-" & Right(ssn, 4)

        Else
            Return "Contact se2"
        End If


    End Function


    ''' <summary>
    ''' Formats address
    ''' </summary>
    ''' <param name="add1"></param>
    ''' <param name="add2"></param>
    ''' <param name="add3"></param>
    ''' <param name="city"></param>
    ''' <param name="state"></param>
    ''' <param name="zip"></param>
    ''' <param name="zipExt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function formatAddress(ByVal add1 As String, ByVal add2 As String, ByVal add3 As String,
                                 ByVal city As String, ByVal state As String, ByVal zip As String,
                                 ByVal zipExt As String) As String
        Try
            Dim address As String = Nothing
            address = add1.Trim
            If (add2.Trim.Length > 0) Then
                address = address & "<br />" & add2.Trim
            End If
            If (add3.Trim.Length > 0) Then
                address = address & "<br />" & add3.Trim
            End If
            address = address & "<br />" & city.Trim & ", " & state.Trim & " " & zip.Trim

            If (zipExt.Trim.Length > 0) Then
                address = address & " " & zipExt.Trim
            End If
            Return address
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Formats the name
    ''' </summary>
    ''' <param name="first"></param>
    ''' <param name="middle"></param>
    ''' <param name="Last"></param>
    ''' <param name="suff"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function formatName(ByVal first As String, ByVal middle As String, ByVal Last As String, ByVal suff As String) As String
        Try
            Dim name As String = ""
            If (middle.Trim.Length > 0) Then
                name = first.Trim & " " & middle.Trim & " " & Last.Trim
            Else
                name = first.Trim & " " & Last.Trim
            End If
            If (suff <> "UNK" AndAlso suff <> "XXXX") Then
                name = name & " " & suff.Trim
            End If

            If (name.Length > 0) Then
                Return name
            Else
                Return "false"
            End If

        Catch ex As Exception
            Throw
        End Try

    End Function

    ''' <summary>
    ''' Converts status code to the correct description
    ''' </summary>
    ''' <param name="statusId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function formatStatus(ByVal statusId As String) As String
        Try
            If Not statusId Is Nothing Then
                Select Case statusId
                    Case "A"
                        Return "Active"
                    Case "B"
                        Return "Accel. Benefit"
                    Case "C"
                        Return "Terminated"
                    Case "D"
                        Return "Claim"
                    Case "E"
                        Return "Pending Issue"
                    Case "F"
                        Return "Free Look Per"
                    Case "G"
                        Return "Surr/Freelook"
                    Case "H"
                        Return "Pnd-Outstnd Req"
                    Case "I"
                        Return "Issued"
                    Case "J"
                        Return "Hardship"
                    Case "K"
                        Return "Pnd-Await Funds"
                    Case "L"
                        Return "Lapse"
                    Case "M"
                        Return "Death Claim Pend."
                    Case "N"
                        Return "Active/Restrict"
                    Case "O"
                        Return "Annuitized"
                    Case "P"
                        Return "Pay-out"
                    Case "Q"
                        Return "Pnd Consrvation"
                    Case "R"
                        Return "Rejected"
                    Case "S"
                        Return "Suspended"
                    Case "T"
                        Return "Extended Term"
                    Case "U"
                        Return "Reduced Paid-Up"
                    Case "V"
                        Return "Lapse Pending"
                    Case "W"
                        Return "Withdrawn"
                    Case "X"
                        Return "Group Policy"
                    Case "Y"
                        Return "Disability"
                    Case "Z"
                        Return "Extended Free Look"
                    Case "&"
                        Return "Cancel/No Premium"
                    Case "("
                        Return "Non-Life Payout"
                    Case "p"
                        Return "Partial Pending"
                    Case "m"
                        Return "Expiry/Maturity"
                    Case "!"
                        Return "Paid Up"
                    Case "5"
                        Return "Suspend Active"
                    Case "6"
                        Return "Suspend Inactive"
                    Case Else
                        Return "Contact se2"
                End Select
            End If
            Return "Contact se2"
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Compares dates and returns true if today is greater than the compare date and false if it doesn't
    ''' </summary>
    ''' <param name="compareDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function dateCompare(ByVal compareDate As Date) As Boolean
        Try
            Dim today As Date = Nothing
            today = DateValue(Now())

            If (today > compareDate) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Formats the variable sent in to currency, returns $0.00 if value is empty
    ''' </summary>
    ''' <param name="num"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function fmtCurrency(ByVal num As Double) As String
        If Not IsNothing(num) Then
            If (num.ToString.Length > 0) Then
                Return FormatCurrency(num)
            Else

            End If
        End If
        Return "$0.00"
    End Function

    ''' <summary>
    ''' Formats the Loan type to full description
    ''' </summary>
    ''' <param name="type"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function formatLoanType(ByVal type As String) As String
        If (type.Length > 0) Then
            Select Case type
                Case "FULL"
                    Return "FULL SURRENDER"
                Case "ANNIV PRCS"
                    Return "ANNIV PRCS"
                Case Else
                    Return type
            End Select
        End If
        Return type
    End Function

    Public Function formatPercent(ByVal percent As String) As String
        If Not (percent Is Nothing) Then
            Return percent & "%"
        Else
            Return "0%"
        End If
    End Function

    ''' <summary>
    ''' Used to calculate the modals based on the plan code
    ''' </summary>
    ''' <param name="annual"></param>
    ''' <param name="planCode"></param>
    ''' <returns>arrayValues of type string array
    ''' arrayValues(0) = semi
    ''' arrayValues(1) = quarterly
    ''' arrayValues(2) = monthly
    ''' </returns>
    ''' <remarks></remarks>
    Public Function calculateModal(ByVal annual As Double, ByVal planCode As String, ByVal modal As String) As String()
        Dim monthlyFactor As Double = Nothing
        Dim quarterlyFactor As Double = Nothing
        Dim semiAnnualFactor As Double = Nothing
        Dim abc As Double = Nothing
        Dim arrayValues() As String = {"0.00", "0.00", "0.00", "0.00", "0", "0", "0", "0"}

        If (annual > 0 AndAlso planCode.Trim.Length > 0) Then
            Select Case planCode
                Case "2000", "3000", "5000"
                    semiAnnualFactor = 0.517
                    monthlyFactor = 0.095
                    quarterlyFactor = 0.259
                    abc = 0.0865
                Case "1153", "1154", "1167", "1169", "1171"
                    semiAnnualFactor = 0.51
                    monthlyFactor = 0.0933
                    quarterlyFactor = 0.26
                    abc = 0.0833
                Case "1168", "1193"
                    semiAnnualFactor = 0.51
                    monthlyFactor = 0.09
                    quarterlyFactor = 0.26
                    abc = 0.0833
                Case "1177", "2177", "3177", "4177", "1181", "1182", "4101", "4201", "1001", "1002", "1007", "1051", "1052", "1053", "1054", "1055", "1056", "1057", "1062"
                    semiAnnualFactor = 0
                    monthlyFactor = 0
                    quarterlyFactor = 0
                Case Else
                    semiAnnualFactor = 0.51
                    monthlyFactor = 0.0933
                    quarterlyFactor = 0.26
                    abc = 0.0875
            End Select

            If (semiAnnualFactor > 0 AndAlso monthlyFactor > 0 AndAlso quarterlyFactor > 0 AndAlso monthlyFactor > 0) Then
                arrayValues(0) = fmtCurrency(annual * semiAnnualFactor)
                arrayValues(1) = fmtCurrency(annual * quarterlyFactor)
                arrayValues(2) = fmtCurrency(annual * abc)
                arrayValues(3) = fmtCurrency(annual * monthlyFactor)

                arrayValues(4) = semiAnnualFactor
                arrayValues(5) = quarterlyFactor
                arrayValues(6) = abc
                arrayValues(7) = monthlyFactor

            End If

        End If
        Return arrayValues
    End Function

    ''' <summary>
    ''' Clears all labels on the screen
    ''' </summary>
    ''' <param name="controlCollection"></param>
    ''' <remarks></remarks>
    Public Sub clearValues(ByVal controlCollection As ControlCollection)
        For Each ctl As Control In controlCollection
            If (TypeOf (ctl) Is Label) Then
                CType(ctl, Label).Text = ""
            End If
        Next
    End Sub

#End Region

#Region "DB Calls"

    ''' <summary>
    ''' Accesses the stored procedure to get the loan information
    ''' </summary>
    ''' <param name="plcyNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getLoanTransInformation(ByVal plcyNumber As String) As DataTable
        If (plcyNumber.Length > 0) Then
            Try
                Dim params As New List(Of System.Data.Common.DbParameter)
                params.Add(New SqlParameter("@TRAN_POLICY_NUMBER", plcyNumber))
                Using dac As New mwa.DataAccess.Sql.SqlDataAccess(VariableAnnuityCs)
                    Return dac.ExecuteDataTable("VariableAnnuity", "SelectSE2_TRANSACTIONS", params)
                End Using
            Catch ex As Exception
                Throw
            End Try
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Grabs policy and monthly value information from VariableAnnuity Db
    ''' </summary>
    ''' <param name="cert"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetCertInfo(ByVal cert As String) As DataTable
        Try
            Dim params As New List(Of System.Data.Common.DbParameter)
            params.Add(New SqlParameter("@PolicyNumber", cert))
            Using dac As New mwa.DataAccess.Sql.SqlDataAccess(VariableAnnuityCs)
                Return dac.ExecuteDataTable("VariableAnnuity", "SelectERADataHubBase", params)
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GetDailyValues(ByVal cert As String) As DataTable
        Try
            Dim params As New List(Of System.Data.Common.DbParameter)
            params.Add(New SqlParameter("@PolicyNumber", cert))
            Using dac As New mwa.DataAccess.Sql.SqlDataAccess(VariableAnnuityCs)
                Return dac.ExecuteDataTable("VariableAnnuity", "SelectMaxSE2_DLY_MON_VALUES", params)
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Grabs Name and Address information
    ''' </summary>
    ''' <param name="cert"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetNameAddressInfo(ByVal cert As String) As DataTable
        Try
            Dim params As New List(Of System.Data.Common.DbParameter)
            params.Add(New SqlParameter("@PolicyNumber", cert))
            Using dac As New mwa.DataAccess.Sql.SqlDataAccess(VariableAnnuityCs)
                Return dac.ExecuteDataTable("VariableAnnuity", "SelectERADataHubAddr", params)
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Grabs ERA Plan Code
    ''' </summary>
    ''' <param name="plcyNum"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPlanCode(ByVal plcyNum As String) As String
        Try
            Dim params As New List(Of System.Data.Common.DbParameter)
            params.Add(New SqlParameter("@MWA_CERT", plcyNum))
            Using dac As New mwa.DataAccess.Sql.SqlDataAccess(ERACs)
                Dim dt As DataTable = dac.ExecuteDataTable("ERA", "Policy_sel", params)
                If (dt.Rows.Count > 0) Then
                    If Not (dt.Rows(0).IsNull("Plan")) Then
                        Return dt.Rows(0)("Plan")
                    Else
                        Return ""
                    End If
                Else
                    Return ""
                End If
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Grabs Plan Description
    ''' </summary>
    ''' <param name="planCode"></param>
    ''' <param name="companyCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPlanDescription(ByVal planCode As Integer, ByVal companyCode As String) As String
        Try
            Dim params As New List(Of System.Data.Common.DbParameter)
            params.Add(New SqlParameter("@PlanCode", planCode))
            params.Add(New SqlParameter("@CompanyCode", companyCode))
            Using dac As New mwa.DataAccess.Sql.SqlDataAccess(AcquisitionsCs)
                Dim dt As DataTable = dac.ExecuteDataTable("Acquisitions", "Plan_sel", params)
                If (dt.Rows.Count > 0) Then
                    Return dt.Rows(0)("Description")
                Else
                    Return ""
                End If
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Grabs Phone Record for Name Id
    ''' </summary>
    ''' <param name="nameId"></param>
    ''' <param name="phoneType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPhone(ByVal nameId As Integer, ByVal phoneType As String) As String
        Try
            Dim params As New List(Of System.Data.Common.DbParameter)
            params.Add(New SqlParameter("@PHN_NAME_ID", nameId))
            params.Add(New SqlParameter("@PHN_PHONE_TYPE", phoneType))
            Using dbObj As New mwa.DataAccess.Sql.SqlDataAccess(VariableAnnuityCs)
                Dim dt As DataTable = dbObj.ExecuteDataTable("VariableAnnuity", "SelectERADataHubPhone", params)
                If (dt.Rows.Count > 0) Then
                    If (dt.Rows(0)("PHN_PHONE_NUMBER") = "() - EXT:") Then
                        If (dt.Rows(0)("PHN_UNF_PHONE").ToString.Trim.Length > 0) Then
                            Return "(" & Left(dt.Rows(0)("PHN_UNF_PHONE"), 3) & ") " & Mid(dt.Rows(0)("PHN_UNF_PHONE"), 4, 3) & "-" & Right(dt.Rows(0)("PHN_UNF_PHONE"), 4)
                        End If
                    Else
                        Return dt.Rows(0)("PHN_PHONE_NUMBER")
                    End If
                End If
                Return "Contact se2"
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Function

#End Region

#Region "Setting User Control Values"

    Public Sub setPayorInformation(ByVal isProduction As Boolean, ByVal dtPayor As DataTable, ByRef payorName As Label, ByRef payorAddress As Label)
        ''Determine Payor information (if it doesn't have a payor or CNSLD payor role then use primary owner)
        If (dtPayor.Rows.Count > 0) Then
            Dim dv As DataView = New DataView
            dv = dtPayor.DefaultView
            dv.RowFilter = "ROLE_ROLE_DESC = 'PAYOR' or ROLE_ROLE_DESC = 'CNSLD PAYOR'"
            If (dv.Count > 0) Then
                dtPayor = dv.ToTable
                payorName.Text = formatName(dtPayor.Rows(0)("NAME_FIRST"), dtPayor.Rows(0)("NAME_MIDDLE"), dtPayor.Rows(0)("NAME_LAST"), dtPayor.Rows(0)("NAME_SUFFIX"))
                'Me.lblPayorNameLabel.Text = dtName.Rows(0)("ROLE_IMPORT_DATE")
                payorAddress.Text = formatAddress(dtPayor.Rows(0)("ADDR_ADDRESS_1"), dtPayor.Rows(0)("ADDR_ADDRESS_2"), dtPayor.Rows(0)("ADDR_ADDRESS_3"), dtPayor.Rows(0)("ADDR_CITY"), dtPayor.Rows(0)("ADDR_STATE"), dtPayor.Rows(0)("ADDR_ZIP"), dtPayor.Rows(0)("ADDR_ZIP_EXT"))
                'Me.lblPayorAddressLabel.Text = dtName.Rows(0)("ROLE_IMPORT_DATE")
            Else
                dv.RowFilter = "ROLE_ROLE_DESC = 'PRIM OWNER'"
                dtPayor = dv.ToTable
                If (dtPayor.Rows.Count > 0) Then
                    payorName.Text = formatName(dtPayor.Rows(0)("NAME_FIRST"), dtPayor.Rows(0)("NAME_MIDDLE"), dtPayor.Rows(0)("NAME_LAST"), dtPayor.Rows(0)("NAME_SUFFIX"))
                    'Me.lblPayorNameLabel.Text = dtName.Rows(0)("ROLE_IMPORT_DATE")
                    payorAddress.Text = formatAddress(dtPayor.Rows(0)("ADDR_ADDRESS_1"), dtPayor.Rows(0)("ADDR_ADDRESS_2"), dtPayor.Rows(0)("ADDR_ADDRESS_3"), dtPayor.Rows(0)("ADDR_CITY"), dtPayor.Rows(0)("ADDR_STATE"), dtPayor.Rows(0)("ADDR_ZIP"), dtPayor.Rows(0)("ADDR_ZIP_EXT"))
                    'Me.lblPayorAddressLabel.Text = dtName.Rows(0)("ROLE_IMPORT_DATE")
                Else
                    payorName.Text = "Contact se2"
                    payorAddress.Text = "Contact se2"
                End If
            End If
        End If
    End Sub

    Public Sub setCashValue(ByVal isProduction As Boolean, ByVal dtDlyValues As DataTable, Optional ByRef guaranteeCashValue As Label = Nothing,
                              Optional ByRef totalCashVal As Label = Nothing, Optional ByRef surrCharge As Label = Nothing,
                              Optional ByRef divCashValue As Label = Nothing, Optional ByRef divOption As Label = Nothing,
                              Optional ByRef netSurrCharge As Label = Nothing, Optional ByRef cashValue As Label = Nothing, Optional ByRef currentInterestRate As Label = Nothing,
                              Optional ByRef baseDividend As Label = Nothing)
        Dim showPreTaxDate As Date = DateSerial(2013, 12, 31)
        If (dtDlyValues.Rows.Count > 0) Then
            If Not (guaranteeCashValue Is Nothing) Then
                guaranteeCashValue.Text = fmtCurrency(dtDlyValues.Rows(0)("DM_BASE_CV"))
            End If
            setDividendOptionCashVal(isProduction, dtDlyValues, divOption, divCashValue)
            If Not (totalCashVal Is Nothing) Then
                totalCashVal.Text = fmtCurrency(dtDlyValues.Rows(0)("DM_SURR_VAL")) 'fmtCurrency(dtPolicy.Rows(0)("MON_SURR_VAL"))
            End If
            If Not (surrCharge Is Nothing) Then
                surrCharge.Text = "Contact se2" ' fmtCurrency(dtDlyValues.Rows(0)("DM_SURR_CHRG"))
            End If
            If Not (netSurrCharge Is Nothing) Then
                netSurrCharge.Text = "Contact se2" 'fmtCurrency(dtDlyValues.Rows(0)("DM_SURR_VAL_CALC"))
            End If
            If Not (cashValue Is Nothing) Then
                cashValue.Text = fmtCurrency(dtDlyValues.Rows(0)("DM_ACCT_TOTAL"))
            End If
            If Not (baseDividend Is Nothing) Then
                baseDividend.Text = "Contact se2"
            End If
            If Not (currentInterestRate Is Nothing) Then
                currentInterestRate.Text = "Contact se2"
            End If

            If Not (isProduction) Then
                If Not (guaranteeCashValue Is Nothing) Then
                    guaranteeCashValue.ToolTip = "DM_BASE_CV"
                End If
                If Not (totalCashVal Is Nothing) Then
                    totalCashVal.ToolTip = "DM_SURR_VAL"
                End If
                If Not (surrCharge Is Nothing) Then
                    surrCharge.ToolTip = "DM_SURR_CHRG"
                End If
                If Not (netSurrCharge Is Nothing) Then
                    netSurrCharge.ToolTip = "DM_SURR_VAL_CALC"
                End If
                If Not (cashValue Is Nothing) Then
                    cashValue.ToolTip = "DM_ACCT_TOTAL"
                End If
            End If

        Else
            If Not (guaranteeCashValue Is Nothing) Then
                guaranteeCashValue.Text = "Contact se2"
            End If
            If Not (totalCashVal Is Nothing) Then
                totalCashVal.Text = "Contact se2"
            End If
            If Not (surrCharge Is Nothing) Then
                surrCharge.Text = "Contact se2"
            End If
            If Not (divCashValue Is Nothing) Then
                divCashValue.Text = "Contact se2"
                divOption.Text = "Contact se2"
            End If
            If Not (netSurrCharge Is Nothing) Then
                netSurrCharge.Text = "Contact se2"
            End If
            If Not (cashValue Is Nothing) Then
                cashValue.Text = "Contact se2"
            End If
            If Not (baseDividend Is Nothing) Then
                baseDividend.Text = "Contact se2"
            End If
            If Not (currentInterestRate Is Nothing) Then
                currentInterestRate.Text = "Contact se2"
            End If
        End If
    End Sub

    Public Sub setDividendOptionCashVal(ByVal isProduction As Boolean, ByVal dtDlyValues As DataTable, ByRef divOption As Label, ByRef divCashVal As Label)
        Dim cashVal As Double = 0.0
        Dim opt As String = ""
        If Not (divOption Is Nothing) AndAlso Not (divCashVal Is Nothing) Then
            If (dtDlyValues.Rows(0)("DM_PAIDUP_CV") > 0 OrElse dtDlyValues.Rows(0)("DM_DAF_CV") > 0) Then
                If (dtDlyValues.Rows(0)("DM_PAIDUP_CV") > 0) Then
                    cashVal = cashVal + dtDlyValues.Rows(0)("DM_PAIDUP_CV")
                    opt = opt & "PUA"
                End If
                If (dtDlyValues.Rows(0)("DM_DAF_CV") > 0) Then
                    cashVal = cashVal + dtDlyValues.Rows(0)("DM_DAF_CV")
                    opt = opt & "Deposits"
                End If
            End If
            If (opt.Length = 0) Then
                opt = "N/A"
            End If

            If Not (isProduction) Then
                divOption.ToolTip = "Determined by which field has a value DM_DAF_CV = Deposits, DM_PAIDUP_CV = Paid Up"
                divCashVal.ToolTip = "DM_DAF_CV + DM_PAIDUP_CV"
            End If

            divOption.Text = opt
            divCashVal.Text = FormatCurrency(cashVal)
        End If
    End Sub

    'Public Sub setPremiumVals(ByVal isProduction As Boolean, ByVal dtPolicy As DataTable, ByVal dtDlyValues As DataTable, ByRef annPremium As Label, ByRef semiAnn As Label, _
    '                            ByRef quarterly As Label, ByRef bankDraft As Label, ByRef monthly As Label, _
    '                            ByRef trPremMonthly As HtmlTableRow)

    '  If (dtPolicy.Rows(0)("PLCY_CONT_STATUS").Trim = "!") Then  ''Paid up status
    '    annPremium.Text = "Paid Up"
    '    semiAnn.Text = "Paid Up"
    '    quarterly.Text = "Paid Up"
    '    bankDraft.Text = "Paid Up"
    '    monthly.Text = "Paid Up"
    '  Else
    '    If (dtDlyValues.Rows.Count > 0) Then
    '      annPremium.Text = fmtCurrency(dtDlyValues.Rows(0)("DM_ANN_PREM"))
    '      Dim modals() As String = calculateModal(dtDlyValues.Rows(0)("DM_ANN_PREM"), dtPolicy.Rows(0)("PLCY_PLANCODE"), dtPolicy.Rows(0)("PLCY_PREM_MODE"))
    '      semiAnn.Text = modals(0)
    '      quarterly.Text = modals(1)
    '      bankDraft.Text = modals(2)
    '      If (dtPolicy.Rows(0)("PLCY_PREM_MODE") = "M") Then
    '        monthly.Text = modals(3)
    '        trPremMonthly.Style.Add("display", "")
    '      Else
    '        trPremMonthly.Style.Add("display", "none")
    '      End If

    '      If Not (isProduction) Then
    '        annPremium.ToolTip = "DM_ANN_PREM"
    '        semiAnn.ToolTip = "DM_ANN_PREM * " & modals(4)
    '        quarterly.ToolTip = "DM_ANN_PREM * " & modals(5)
    '        bankDraft.ToolTip = "DM_ANN_PREM * " & modals(6)
    '        monthly.ToolTip = "DM_ANN_PREM * " & modals(7)
    '      End If

    '    Else
    '      annPremium.Text = "Contact se2"
    '      semiAnn.Text = "Contact se2"
    '      quarterly.Text = "Contact se2"
    '      bankDraft.Text = "Contact se2"
    '    End If
    '  End If
    'End Sub

    Public Sub setBilling(ByVal isProduction As Boolean, ByVal dtPolicy As DataTable, ByVal dtDlyValues As DataTable, ByRef mode As Label, ByRef lastPaidDate As Label,
                             Optional paidToDate As Label = Nothing, Optional ByRef expectedAnnual As Label = Nothing, Optional ByRef lastPaidAmount As Label = Nothing, Optional ByRef billedAmount As Label = Nothing)

        If (dtPolicy.Rows(0)("PLCY_CONT_STATUS").Trim = "!") Then

            mode.Text = "Paid Up"
            If (dtPolicy.Rows(0)("PLCY_PREM_PAID_DATE") = "#12/31/2999#") Then
                lastPaidDate.Text = "N/A"
            End If
            If Not (paidToDate Is Nothing) Then
                paidToDate.Text = "Contact se2"
            End If
            If (Not expectedAnnual Is Nothing) AndAlso (dtDlyValues.Rows.Count > 0) Then
                expectedAnnual.Text = fmtCurrency(dtDlyValues.Rows(0)("DM_PLANNED_MOD_PREM"))
            End If
            If Not (billedAmount Is Nothing) Then
                billedAmount.Text = "Paid Up"
            End If
        Else
            lastPaidDate.Text = dtPolicy.Rows(0)("PLCY_PREM_PAID_DATE")
            mode.Text = formatMode(dtPolicy.Rows(0)("PLCY_PREM_MODE"))
            If Not (paidToDate Is Nothing) Then
                paidToDate.Text = "Contact se2"
            End If
            If (Not expectedAnnual Is Nothing) AndAlso (dtDlyValues.Rows.Count > 0) Then
                expectedAnnual.Text = fmtCurrency(dtDlyValues.Rows(0)("DM_PLANNED_MOD_PREM"))
            End If
            If Not (billedAmount Is Nothing) Then
                If (dtDlyValues.Rows.Count > 0) Then
                    billedAmount.Text = dtDlyValues.Rows(0)("DM_ANN_PREM")
                Else
                    billedAmount.Text = "Contact se2"
                End If

            End If
        End If



        If Not (lastPaidAmount Is Nothing) Then
            lastPaidAmount.Text = "See Transactions"
        End If

        If Not (isProduction) Then
            If Not (expectedAnnual Is Nothing) Then
                expectedAnnual.ToolTip = "DM_PLANNED_MOD_PREM"
            End If
            If Not (billedAmount Is Nothing) Then
                billedAmount.ToolTip = "DM_ANN_PREM"
            End If
            lastPaidDate.ToolTip = "PLCY_PREM_PAID_DATE"
            mode.ToolTip = "PLCY_PREM_MODE"
        End If


    End Sub

    Public Sub setLoanInfo(ByVal isProduction As Boolean, ByVal dtPolicy As DataTable, ByRef maxLoan As Label, ByRef loanBal As Label,
                               ByRef loanRate As Label)

        If Not (dtPolicy.Rows(0).IsNull("LOAN_IMPORT_DATE")) Then
            loanBal.Text = fmtCurrency(dtPolicy.Rows(0)("LOAN_BAL"))
            If (dtPolicy.Rows(0)("LOAN_RATE") > 0) Then
                loanRate.Text = (Math.Round(dtPolicy.Rows(0)("LOAN_RATE") * 100, 2)) & "%"
            Else
                loanRate.Text = "Contact se2"
            End If
            maxLoan.Text = "Contact se2"

            If Not (isProduction) Then
                loanBal.ToolTip = "LOAN_BAL"
                loanRate.ToolTip = "LOAN_RATE"
            End If

        Else
            loanBal.Text = "$0.00"
            loanRate.Text = "0%"
            maxLoan.Text = "Contact se2"
        End If
    End Sub

    Public Sub setDeathBene(ByVal isProduction As Boolean, ByVal dtPolicy As DataTable, ByVal dtDlyValues As DataTable, ByRef insurAmount As Label, ByRef deathBen As Label, Optional ByRef puaFaceAmount As Label = Nothing)

        If (dtDlyValues.Rows.Count > 0) Then
            Dim loanBalance As Double = Nothing
            If (dtPolicy.Rows(0).IsNull("LOAN_IMPORT_DATE")) Then
                loanBalance = 0.0
            Else
                loanBalance = dtPolicy.Rows(0)("LOAN_BAL")
            End If
            ' Dim deathAmount As Double = (dtDlyValues.Rows(0)("DM_FACE_AMT") + dtDlyValues.Rows(0)("DM_DAF_CV") + dtDlyValues.Rows(0)("DM_FACE_PUA") + dtDlyValues.Rows(0)("DM_FACE_TERM")) - loanBalance
            insurAmount.Text = fmtCurrency(dtDlyValues.Rows(0)("DM_FACE_AMT"))

            deathBen.Text = fmtCurrency(dtDlyValues.Rows(0)("DM_DB"))


            If Not (puaFaceAmount Is Nothing) Then
                puaFaceAmount.Text = dtDlyValues.Rows(0)("DM_FACE_PUA")
            End If


            If Not (isProduction) Then

                deathBen.ToolTip = "DM_DB"
                insurAmount.ToolTip = "DM_FACE_AMT"

                If Not (puaFaceAmount Is Nothing) Then
                    puaFaceAmount.ToolTip = "DM_FACE_PUA"
                End If

            End If


        Else
            insurAmount.Text = "Contact se2"
            deathBen.Text = "Contact se2"

            If Not (puaFaceAmount Is Nothing) Then
                puaFaceAmount.Text = "Contact se2"
            End If
        End If

    End Sub



    Public Sub setWithdrawalInfo(ByVal isProduction As Boolean, ByVal dtDlyValues As DataTable, ByRef maxWithdrawal As Label, Optional ByRef currentWithdrawals As Label = Nothing,
                                 Optional ByRef ytdWithdrawals As Label = Nothing, Optional ByRef withdrawalCharge As Label = Nothing)

        Dim showCurrentValues As Date = DateSerial(2013, 11, 1)
        If (dtDlyValues.Rows.Count > 0) Then
            maxWithdrawal.Text = "Contact se2"
            If Not (currentWithdrawals Is Nothing) Then
                If dateCompare(showCurrentValues) = True Then
                    currentWithdrawals.Text = fmtCurrency(dtDlyValues.Rows(0)("DM_PYTD_WITH"))
                Else
                    currentWithdrawals.Text = "Contact se2"
                End If
            End If

            If Not (ytdWithdrawals Is Nothing) Then
                ytdWithdrawals.Text = fmtCurrency(dtDlyValues.Rows(0)("DM_CYTD_WITH"))
            End If


            If Not (withdrawalCharge Is Nothing) Then
                withdrawalCharge.Text = "Contact se2"
            End If

            If Not (isProduction) Then
                If Not (currentWithdrawals Is Nothing) Then
                    currentWithdrawals.ToolTip = "DM_PYTD_WITH"
                End If
                If Not (ytdWithdrawals Is Nothing) Then
                    ytdWithdrawals.ToolTip = "DM_CYTD_WITH"
                End If

            End If

        Else
            maxWithdrawal.Text = "Contact se2"
            If Not (currentWithdrawals Is Nothing) Then
                currentWithdrawals.Text = "Contact se2"
            End If
            If Not (ytdWithdrawals Is Nothing) Then
                ytdWithdrawals.Text = "Contact se2"
            End If
            If Not (withdrawalCharge Is Nothing) Then
                withdrawalCharge.Text = "Contact se2"
            End If

        End If
    End Sub

    Public Sub setGainInformation(ByVal isProduction As Boolean, ByVal dtDlyValues As DataTable, ByVal blnAnnuity As Boolean,
                                    Optional ByRef pwGain As Label = Nothing, Optional ByRef csGain As Label = Nothing, Optional ByRef pwGainNQ As Label = Nothing,
                                    Optional ByRef finalGainNQ As Label = Nothing, Optional ByRef csPwGainRoth As Label = Nothing, Optional ByRef deathGainRoth As Label = Nothing)
        If (blnAnnuity = True) Then
            pwGainNQ.Text = "Contact se2"
            finalGainNQ.Text = "Contact se2"
            csPwGainRoth.Text = "Contact se2"
            deathGainRoth.Text = "Contact se2"
        Else
            pwGain.Text = "Contact se2"
            csGain.Text = "Contact se2"
        End If

    End Sub

    Public Sub setRider(ByVal isProduction As Boolean, ByVal dtDlyValues As DataTable, ByRef adRider As Label, ByRef adAmount As Label, ByRef wpRider As Label,
                          ByRef payorBenefit As Label, Optional ByRef termRider As Label = Nothing, Optional ByRef termRiderAmount As Label = Nothing)

        If (dtDlyValues.Rows.Count > 0) Then
            If (dtDlyValues.Rows(0)("DM_FACE_ADB") > 0) Then
                adRider.Text = "Yes"
            Else
                adRider.Text = "No"
            End If
            adAmount.Text = fmtCurrency(dtDlyValues.Rows(0)("DM_FACE_ADB"))
            wpRider.Text = "Contact se2"
            payorBenefit.Text = "Contact se2"

            If Not (termRider Is Nothing) Then
                termRider.Text = "Contact se2"
                termRiderAmount.Text = fmtCurrency(dtDlyValues.Rows(0)("DM_FACE_TERM"))
            End If

            If Not (isProduction) Then
                adAmount.ToolTip = "DM_FACE_ADB"
                adRider.ToolTip = "if DM_FACE_ADB > 0 then Yes otherwise No"
                If Not (termRider Is Nothing) Then
                    termRiderAmount.ToolTip = "DM_FACE_TERM"
                End If
            End If
        Else
            adRider.Text = "Contact se2"
            adAmount.Text = "Contact se2"
            wpRider.Text = "Contact se2"
            payorBenefit.Text = "Contact se2"

            If Not (termRider Is Nothing) Then
                termRider.Text = "Contact se2"
                termRiderAmount.Text = "Contact se2"
            End If
        End If

    End Sub

    Public Sub setCertOptions(ByVal isProduction As Boolean, ByVal dtDlyValues As DataTable, ByRef nonforfeitureOpt As Label, ByRef rpuAmount As Label,
                                ByRef eiAmount As Label, ByRef eiDate As Label)

        If (dtDlyValues.Rows.Count > 0) Then
            nonforfeitureOpt.Text = "Contact se2"
            rpuAmount.Text = "Contact se2"
            eiAmount.Text = "Contact se2"
            eiDate.Text = "Contact se2"
        Else
            nonforfeitureOpt.Text = "Contact se2"
            rpuAmount.Text = "Contact se2"
            eiAmount.Text = "Contact se2"
            eiDate.Text = "Contact se2"
        End If

    End Sub

    Public Sub setContributions(ByVal isProduction As Boolean, ByRef dtPolicy As DataTable, ByRef currContributions As Label, ByRef ytdContributions As Label,
                                   ByRef currTaxYearContri As Label, ByRef prevTaxYearContri As Label)

        Dim showCurrentDate As Date = DateSerial(2013, 11, 1)
        Dim showPreTaxDate As Date = DateSerial(2013, 12, 31)
        Dim show2TaxDate As Date = DateSerial(2014, 12, 31)

        ''Show the values after Nov 1 2013 since this will be the first time valid values will be sent over
        If (dateCompare(showCurrentDate) = True) Then
            currContributions.Text = fmtCurrency(dtPolicy.Rows(0)("PLCY_CURR_YR_CONT"))
        Else
            currContributions.Text = "Contact se2"
        End If

        ''Show the values after Jan 1 2014 since this will be the first time valid values will be sent over
        If (dateCompare(showPreTaxDate) = True) Then
            prevTaxYearContri.Text = fmtCurrency(dtPolicy.Rows(0)("PLCY_PRIOR_YR_CONT"))
        Else
            prevTaxYearContri.Text = "Contact se2"
        End If

        ''Show the values after Jan 1 2015 since this will be the first time valid values will be sent over


        currTaxYearContri.Text = fmtCurrency(dtPolicy.Rows(0)("PLCY_CURR_YR_CONT"))
        ytdContributions.Text = fmtCurrency(dtPolicy.Rows(0)("PLCY_CURR_YR_CONT"))

        If Not (isProduction) Then
            currContributions.ToolTip = "PLCY_CURR_YR_CONT"
            prevTaxYearContri.ToolTip = "PLCY_PRIOR_YR_CONT"
            currTaxYearContri.ToolTip = "PLCY_CURR_YR_CONT"
            ytdContributions.ToolTip = "PLCY_CURR_YR_CONT"
        End If


    End Sub

    Public Sub setLegendLabels(ByVal dtPolicy As DataTable, ByVal dtDlyValues As DataTable, ByVal controlCollection As ControlCollection)
        For Each ctl As Control In controlCollection 'Loop through legends to add import date
            If (TypeOf (ctl) Is Label) Then
                If (CType(ctl, Label).CssClass = "legend") Then
                    If (CType(ctl, Label).ID = "lblLoanLegendLabel") Then
                        If Not (dtPolicy.Rows(0).IsNull("LOAN_IMPORT_DATE")) Then
                            CType(ctl, Label).Text = "as of " & dtPolicy.Rows(0)("LOAN_IMPORT_DATE")
                        End If
                    Else
                        If (dtDlyValues.Rows.Count > 0) Then
                            CType(ctl, Label).Text = "as of " & dtDlyValues.Rows(0)("DM_IMPORT_DATE")
                        End If
                    End If
                End If
            End If
        Next
    End Sub

    Public Sub commentValues(ByVal blnComment As Boolean, ByRef controlCollection As ControlCollection)
        If (blnComment = True) Then
            For Each ctl As Control In controlCollection
                If (TypeOf (ctl) Is Label) Then
                    If (CType(ctl, Label).CssClass <> "legend") Then
                        CType(ctl, Label).Text = "Contact se2"
                    End If
                End If
            Next
        End If
    End Sub

#End Region

End Class
