﻿Imports Microsoft.VisualBasic

Namespace Models


  Public Enum CertType
    Annuity
    Dividend
    Interest
    Term
    Other
  End Enum

  Public Class CertInformation
    Public Property CertNumber As BaseValue
    Public Property Status As BaseValue
    Public Property IssueAge As BaseValue
    Public Property IssueDate As BaseValue
    Public Property MaturityDate As BaseValue
    Public Property PlanCode As BaseValue
    Public Property PlanDescription As BaseValue
    Public ReadOnly Property Type As CertType
      Get
        If Not (Me.ERAPlanType Is Nothing) Then
          Return Helpers.GetPlanType(Me.ERAPlanType.value)
        Else
          Return CertType.Other
        End If

      End Get
    End Property
    Public Property ERAPlanType As BaseValue

    Public Property InsuredInformation As Person.Person
    Public Property AdditionalInsureds As List(Of Person.Person)

    Public Property OwnerInformation As Person.Person
    Public Property AdditionalOwners As List(Of Person.Person)

    Public Property Mode As BaseValue
    Public Property LastPaidAmount As BaseValue
    Public Property LastPaidDate As BaseValue
    Public Property RatingClass As BaseValue

    Public Property PolicyImportedDate As BaseValue
    Public Property DailyMonthlyImportedDate As BaseValue
    Public Property LoanImportedDate As BaseValue
    Public Property RolesImportedDate As BaseValue



    Public Property ErrorMessage As List(Of String)



  End Class




End Namespace

