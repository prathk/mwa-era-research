﻿Imports Microsoft.VisualBasic

Namespace Models

  Public Class Term : Inherits CertInformation

    Public ReadOnly Property AdRider As String
      Get
        If (AdAmount.value.Length > 0 AndAlso AdAmount.value <> "0.00" AndAlso AdAmount.value <> "Contact se2") Then
          Return "Yes"
        ElseIf AdAmount.value = "Contact se2" Then
          Return "Contact se2"
        Else
          Return "No"
        End If
      End Get
    End Property
    Public Property AdAmount As BaseValue
    Public Property WpRider As BaseValue
    Public Property PayorBenefitRider As BaseValue
    Public Property PaidToDate As BaseValue
    Public Property BilledAmount As BaseValue
    Public Property Payor As Models.Person.Person
    Public Property InsuranceAmount As BaseValue


  End Class

End Namespace

