﻿Imports Microsoft.VisualBasic

Namespace Models

  Public Class Dividend
    Public ReadOnly Property DividendCashValue As BaseValue
      Get
        Dim clsSE2DataHub As New clsSE2DataHub

        Try
          Return Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency((CDbl(Me.DividendDeposits.value) + CDbl(DividendPaidUpCV.value))), Me.DividendDeposits.dbField & " + " & DividendPaidUpCV.dbField)

        Catch ex As Exception
          Return Helpers.FormatBaseField("Contact se2", "DM_DAF_CV + DM_PAIDUP_CV")
        End Try

      End Get
    End Property
    Public Property BaseDividend As BaseValue
    Public ReadOnly Property DividendOption As BaseValue
      Get
        Dim divOption As String = ""
        If Not (DividendPaidUpCV.value = "Contact se2" Or DividendDeposits.value = "Contact se2") Then
          If (DividendPaidUpCV.value > 0) Then
            divOption = "PUA "
          End If
          If (DividendDeposits.value > 0) Then
            divOption = divOption & "Deposits"
          End If
          Return Helpers.FormatBaseField(divOption, "Has DM_DAF_CV or DM_PAIDUP_CV")
        Else
          Return Helpers.FormatBaseField("Contact se2", "Has DM_DAF_CV or DM_PAIDUP_CV")
        End If

      End Get
    End Property
    Public Property PuaFaceAmount As BaseValue
    Public Property DividendPaidUpCV As BaseValue
    Public Property DividendDeposits As BaseValue




  End Class

End Namespace

