﻿Imports Microsoft.VisualBasic


Namespace Models


  Public Class LifePlans : Inherits CertInformation

    Public Property GuaranteedCashValue As BaseValue
    Public Property TotalCashValue As BaseValue
    Public Property SurrenderCharge As BaseValue
    Public Property NetSurrenderValue As BaseValue
    Public Property InsuranceAmount As BaseValue
    Public Property DeathBenefit As BaseValue
    Public Property MaxWithdrawal As BaseValue
    Public Property MaxLoan As BaseValue
    Public Property LoanBalance As BaseValue
    Public Property LoanInterestRate As BaseValue
    Public Property NonForfeitureOption As BaseValue
    Public Property RpuAmount As BaseValue
    Public Property EiAmount As BaseValue
    Public Property EiDate As BaseValue
    Public Property PaidToDate As BaseValue

    Public Property BilledAmount As BaseValue
    Public Property Payor As Person.Person
    Public Property TermRider As BaseValue
    Public Property TermRiderAmount As BaseValue
    Public ReadOnly Property AdRider As String
      Get
        If (AdAmount.value.Length > 0 AndAlso AdAmount.value <> "0.00" AndAlso AdAmount.value <> "Contact se2") Then
          Return "Yes"
        ElseIf AdAmount.value = "Contact se2" Then
          Return "Contact se2"
        Else
          Return "No"
        End If
      End Get
    End Property
    Public Property AdAmount As BaseValue
    Public Property WpRider As BaseValue
    Public Property PayorBenefitRider As BaseValue
    Public Property PwGain As BaseValue
    Public Property CsGain As BaseValue

    Public Property Dividend As Models.Dividend
    Public Property Interest As Models.Interest

  End Class


End Namespace

