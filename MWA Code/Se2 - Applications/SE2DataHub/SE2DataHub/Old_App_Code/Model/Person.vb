﻿Imports Microsoft.VisualBasic
Imports System.Data

Namespace Models.Person

  Public Enum RoleType
    Insured
    PrimPart
    PrimOwner
    JtOwner
    ContOwner
    JtCovered
    Agent
    Assign
    Assignee
    Beneficiary
    CnsldPayor
    Conservator
    CollAssign
    CommRep
    ContigentBene
    Executor
    GroupMaster
    Owner
    OwnerBene
    Payee
    Payor
    PrimBene
    ServRep
    ServAgent
    Spouse
    TertBene
    Trustee
  End Enum

  Public Class Person

    Public Property Name As BaseValue
    Public Property Dob As BaseValue
    Public Property Gender As BaseValue
    Public Property Address As BaseValue
    Public Property SSN As BaseValue
    Public Property Phone As BaseValue
    Public ReadOnly Property Type As RoleType
      Get
        Return Helpers.GetPersonType(RoleDescription.value)
      End Get
    End Property
    Public Property RoleDescription As BaseValue
    Public Property Percent As BaseValue


    Public Sub New(ByVal drPerson As DataRow)
      Dim clsSe2DataHub As New clsSE2DataHub

      Me.Name = Helpers.FormatBaseField(clsSe2DataHub.formatName(drPerson("NAME_FIRST"), drPerson("NAME_MIDDLE"), drPerson("NAME_LAST"), drPerson("NAME_SUFFIX")), "NAME_FIRST NAME_MIDDLE NAME_LAST NAME_SUFFIX")
      Me.Dob = Helpers.FormatBaseField(CDate(drPerson("NAME_DOB")).ToShortDateString, "NAME_DOB")
      Me.Gender = Helpers.FormatBaseField(drPerson("NAME_SEX").ToString.Trim, "NAME_SEX")

      If Not drPerson.IsNull("ADDR_ADDRESS_1") Then
        Me.Address = Helpers.FormatBaseField(clsSe2DataHub.formatAddress(drPerson("ADDR_ADDRESS_1"), drPerson("ADDR_ADDRESS_2"), drPerson("ADDR_ADDRESS_3"), drPerson("ADDR_CITY"), drPerson("ADDR_STATE"), drPerson("ADDR_ZIP"), drPerson("ADDR_ZIP_EXT")), "ADDR_ADDRESS_1,2,3, <br /> ADDR_CITY ADDR_STATE ADDR_ZIP ADDR_ZIP_EXT")
      Else
        Me.Address = Helpers.FormatBaseField("Contact se2", "N/A")
      End If

      Me.SSN = Helpers.FormatBaseField(clsSe2DataHub.formatSSN(drPerson("NAME_TAX_ID").ToString.Trim), "NAME_TAX_ID")
      Me.Phone = Helpers.FormatBaseField(clsSe2DataHub.GetPhone(drPerson("ROLE_ROLE_NM_ID"), "HTELE"), "PHN_PHONE_NUMBER or PHN_UNF_PHONE")
      Me.RoleDescription = Helpers.FormatBaseField(drPerson("ROLE_ROLE_DESC"), "ROLE_ROLE_DESC")
      Me.Percent = Helpers.FormatBaseField(clsSe2DataHub.formatPercent(drPerson("ROLE_PERCENTAGE")), "ROLE_PERCENTAGE")
    End Sub


  End Class

End Namespace

