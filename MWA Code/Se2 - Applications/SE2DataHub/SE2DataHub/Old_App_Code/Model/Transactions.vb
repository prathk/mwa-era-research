﻿Imports Microsoft.VisualBasic

Namespace Models

  Public Class TransactionsItems

    Public Property TransactionId As String = String.Empty
    Public Property Type As String = String.Empty
    Public Property PostedDate As Date
    Public Property EffectiveDate As Date
    Public Property ReversalDate As String = String.Empty
    Public Property Amount As String = String.Empty

  End Class

  Public Class Transaction
    Public Property TypeList As List(Of String)
    Public Property TransactionItems As List(Of TransactionsItems)
    Public Property TransactionImportDate As BaseValue
  End Class
End Namespace

