﻿Imports Microsoft.VisualBasic

Namespace Models

  Public Class Annuity : Inherits CertInformation
    Public Property YearEndCashValue As BaseValue
    Public Property CashValue As BaseValue
    Public Property SurrenderValue As BaseValue
    Public Property CurrentContributions As BaseValue
    Public Property YtdContributions As BaseValue
    Public Property CurrentTaxContributions As BaseValue
    Public Property PrevTaxContributions As BaseValue
    Public Property WithdrawalCharge As BaseValue
    Public Property CurrentWithdrawals As BaseValue
    Public Property YtdWithdrawals As BaseValue
    Public Property GovernmentStatus As BaseValue
    Public Property CurrentInterestRates As BaseValue
    Public Property MaxWithdrawal As BaseValue
  End Class

End Namespace

