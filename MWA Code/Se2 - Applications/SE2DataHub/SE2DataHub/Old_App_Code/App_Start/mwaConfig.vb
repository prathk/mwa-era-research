﻿Imports Microsoft.VisualBasic
Imports System.Web.Optimization

<Assembly: WebActivatorEx.PostApplicationStartMethod(GetType(mwaConfig), "PostStart")> 

Public Class mwaConfig
  Public Shared Sub PostStart()
    BundleConfig.RegisterBundles(BundleTable.Bundles)
  End Sub
End Class
