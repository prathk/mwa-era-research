﻿Imports Microsoft.VisualBasic
Imports System.Web.Optimization

Public Class BundleConfig
  Public Shared Sub RegisterBundles(bundles As BundleCollection)
    bundles.IgnoreList.Clear()
    AddDefaultIgnorePatterns(bundles.IgnoreList)
    BundleTable.EnableOptimizations = False

        'Include all vendor supplied scripts
        bundles.Add(New ScriptBundle("~/scripts/vendor") _
    .Include("~/scripts/jquery-1.8.2.min.js") _
      .Include("~/scripts/modernizr-{version}.js") _
      .Include("~/scripts/respond.js") _
      .Include("~/scripts/bootstrap.js") _
      .Include("~/scripts/knockout-3.1.0.js") _
      .Include("~/scripts/moment.js") _
      .Include("~/scripts/toastr.js") _
      .Include("~/scripts/jquery.inputmask/jquery.inputmask-2.5.9.js") _
      .Include("~/scripts/jquery.inputmask/jquery.inputmask.date.extensions-2.5.9.js") _
      .Include("~/scripts/knockout-bootstrap.min.js")
    )
        'Include all css within the site
        bundles.Add(New ScriptBundle("~/Content/css") _
       .Include("~/Content/ie10mobile.css") _
      .Include("~/Content/bootstrap/bootstrap.css") _
      .Include("~/Content/bootstrap/bootstrap-theme.css") _
      .Include("~/Content/css/font-awesome.css") _
      .Include("~/Content/toastr.css") _
    .Include("~/Content/mwa.css") _
    .Include("~/Css/Se2DataHub.css")
    )
    End Sub

  Public Shared Sub AddDefaultIgnorePatterns(ignoreList As IgnoreList)
    If IsNothing(ignoreList) Then
      Throw New ArgumentNullException("ignoreList")
    End If

    ignoreList.Ignore("*.intellisense.js")
    ignoreList.Ignore("*-vsdoc.js")
  End Sub
End Class
