﻿Imports System.Runtime.CompilerServices
Imports System.Data

Namespace Helpers

  Public Module Helpers

    <Extension()>
    Public Function GetPlanType(ByVal eraPlanType As String) As Models.CertType
      Select Case eraPlanType
        Case "ERA Deferred Annuity"
          Return Models.CertType.Annuity
        Case "ERA Whole Life Insurance"
          Return Models.CertType.Dividend
        Case "ERA Interest Sensitive Whole Life I", "ERA Interest Sensitive Whole Life II"
          Return Models.CertType.Interest
        Case "ERA Endowment Insurance"
          Return Models.CertType.Interest
        Case "ERA Term Life"
          Return Models.CertType.Term
        Case Else
          Return Models.CertType.Other
      End Select
    End Function

    <Extension()>
    Public Function GetPersonType(ByVal roleDesc As String) As Models.Person.RoleType

      Select Case roleDesc.ToUpper
        Case "ANNUITANT / INSURED", "INSURED", "PRIM PART"
          Return Models.Person.RoleType.Insured
        Case "JT COVERED"
          Return Models.Person.RoleType.JtCovered
        Case "ASSIGNEE", "ASSIGN"
          Return Models.Person.RoleType.Assignee
        Case "COLL ASSIGN"
          Return Models.Person.RoleType.CollAssign
        Case "SERV REP", "SERVICING AGENT"
          Return Models.Person.RoleType.ServAgent
        Case "CNSLD PAYOR"
          Return Models.Person.RoleType.CnsldPayor
        Case "BENEFICIARY", "PRIM BENE"
          Return Models.Person.RoleType.Beneficiary
        Case "CONT BENE"
          Return Models.Person.RoleType.ContigentBene
        Case "PAYOR"
          Return Models.Person.RoleType.Payor
        Case "EXECUTOR"
          Return Models.Person.RoleType.Executor
        Case "OWNER BENEFICIARY"
          Return Models.Person.RoleType.OwnerBene
        Case "GROUP MASTER"
          Return Models.Person.RoleType.GroupMaster
        Case "PAYEE", "PAYEE"
          Return Models.Person.RoleType.Payee
        Case "CNSRVTR"
          Return Models.Person.RoleType.Conservator
        Case "TERT BENE"
          Return Models.Person.RoleType.TertBene
        Case "CONT OWNER"
          Return Models.Person.RoleType.ContOwner
        Case "TRUSTEE"
          Return Models.Person.RoleType.Trustee
        Case "OWNER", "PRIM OWNER"
          Return Models.Person.RoleType.Owner
        Case "JT OWNER"
          Return Models.Person.RoleType.JtOwner
        Case "SPOUSE"
          Return Models.Person.RoleType.Spouse
        Case "Agent"
          Return Models.Person.RoleType.Agent
        Case "COMM REP"
          Return Models.Person.RoleType.CommRep
        Case Else
          Return Nothing
      End Select

    End Function

    <Extension()>
    Public Function FormatBaseField(ByVal value As String, ByVal dbField As String) As Models.BaseValue
      Dim field As New Models.BaseValue
      field.dbField = dbField
      field.value = value
      Return field
    End Function


    Public Function LoadCertInformation(ByVal plcyLoan As DataTable, ByVal dlyMonth As DataTable, ByVal certRoles As DataTable) As Models.CertInformation
      Dim certType As New Models.CertInformation
      certType.ERAPlanType = Helpers.FormatBaseField(plcyLoan.Rows(0)("PLCY_PRODUCT"), "PLCY_PRODUCT")
      Dim personList = LoadCertPersonObj(certRoles)

      Dim certInfo As New Models.CertInformation

      Select Case (certType.Type)
        Case Models.CertType.Annuity
          certInfo = New Models.Annuity()
          certInfo.ERAPlanType = certType.ERAPlanType
        Case Models.CertType.Dividend
          certInfo = New Models.LifePlans()
          certInfo.ERAPlanType = certType.ERAPlanType
        Case Models.CertType.Interest
          certInfo = New Models.LifePlans()
          certInfo.ERAPlanType = certType.ERAPlanType
        Case Models.CertType.Term
          certInfo = New Models.Term()
          certInfo.ERAPlanType = certType.ERAPlanType
      End Select


      certInfo.AdditionalInsureds = New List(Of Models.Person.Person)

      LoadImportDates(plcyLoan, dlyMonth, certRoles, certInfo)
      LoadBaseCertInfo(plcyLoan, dlyMonth, certInfo)
      LoadInsuredInformation(personList, certInfo.InsuredInformation, certInfo.AdditionalInsureds)
      LoadOwnerInformation(personList, certInfo.OwnerInformation, certInfo.AdditionalOwners)

      Dim payor = GetPayor(personList)

      If (certInfo.Type = Models.CertType.Annuity) Then
        certInfo = LoadAnnuity(plcyLoan, dlyMonth, certInfo)
      ElseIf certInfo.Type = Models.CertType.Dividend Then
        certInfo = LoadDividend(plcyLoan, dlyMonth, payor, certInfo)
      ElseIf certInfo.Type = Models.CertType.Interest Then
        certInfo = LoadInterest(plcyLoan, dlyMonth, payor, certInfo)
      ElseIf certInfo.Type = Models.CertType.Term Then
        certInfo = LoadTerm(plcyLoan, dlyMonth, payor, certInfo)
      End If



      Return certInfo


    End Function

    Public Function LoadTransactions(ByVal transactions As DataTable) As Models.Transaction

      Dim clsSE2DataHub As New clsSE2DataHub
      Dim trans As New Models.Transaction
      trans.TypeList = New List(Of String)
      trans.TransactionItems = New List(Of Models.TransactionsItems)
      If Not (transactions Is Nothing) Then
        For Each row In transactions.Rows
          Dim t As New Models.TransactionsItems
          t.TransactionId = row("TRAN_TRANS_NUMBER")
          t.Type = clsSE2DataHub.formatLoanType(row("TRAN_TRANS_NAME"))
          t.PostedDate = row("TRAN_POSTED_DATE")
          t.EffectiveDate = row("TRAN_EFF_DATE")
          If (row("TRAN_REVERSAL_DATE") <> "01/01/1900") Then
            t.ReversalDate = row("TRAN_REVERSAL_DATE")
          End If

          t.Amount = clsSE2DataHub.fmtCurrency(row("TRAN_TRAN_AMT"))
          trans.TransactionItems.Add(t)
          trans.TransactionImportDate = Helpers.FormatBaseField(row("TRAN_IMPORT_DATE"), "TRAN_IMPORT_DATE")
          If Not (trans.TypeList.Contains(clsSE2DataHub.formatLoanType(row("TRAN_TRANS_NAME")))) Then
            trans.TypeList.Add(clsSE2DataHub.formatLoanType(row("TRAN_TRANS_NAME")))
          End If

        Next

                'trans.TransactionItems = trans.TransactionItems.OrderByDescending(Function(x) x.TransactionId).ToList
                'SR152998-Se2 datahub sorting transactions
                trans.TransactionItems = trans.TransactionItems.OrderByDescending(Function(x) x.EffectiveDate).ToList
        trans.TypeList.Sort()

      End If
      

      Return trans
    End Function

    Private Function LoadAnnuity(ByVal plcyLoan As DataTable, ByVal dlyMonth As DataTable, ByVal annuity As Models.Annuity) As Models.CertInformation
      Dim clsSE2DataHub As New clsSE2DataHub

      If Not (dlyMonth.Rows.Count = 0) Then


        annuity.YearEndCashValue = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_BASE_CV")), "DM_BASE_CV")
        annuity.CashValue = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_ACCT_TOTAL")), "DM_ACCT_TOTAL")
        annuity.CurrentWithdrawals = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_PYTD_WITH")), "DM_PYTD_WITH")
        annuity.YtdWithdrawals = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_CYTD_WITH")), "DM_CYTD_WITH")
      Else
        annuity.YearEndCashValue = Helpers.FormatBaseField("Contact se2", "DM_BASE_CV")
        annuity.CashValue = Helpers.FormatBaseField("Contact se2", "DM_ACCT_TOTAL")
        annuity.CurrentWithdrawals = Helpers.FormatBaseField("Contact se2", "DM_PYTD_WITH")
        annuity.YtdWithdrawals = Helpers.FormatBaseField("Contact se2", "DM_CYTD_WITH")
      End If

      If Not plcyLoan.Rows.Count = 0 Then
        annuity.CurrentContributions = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(plcyLoan.Rows(0)("PLCY_CURR_YR_CONT")), "PLCY_CURR_YR_CONT")
        annuity.YtdContributions = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(plcyLoan.Rows(0)("PLCY_CURR_YR_CONT")), "PLCY_CURR_YR_CONT")
        annuity.CurrentTaxContributions = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(plcyLoan.Rows(0)("PLCY_CURR_YR_CONT")), "PLCY_CURR_YR_CONT")
        annuity.PrevTaxContributions = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(plcyLoan.Rows(0)("PLCY_PRIOR_YR_CONT")), "PLCY_PRIOR_YR_CONT")
        annuity.GovernmentStatus = Helpers.FormatBaseField(plcyLoan.Rows(0)("PLCY_QUAL_PLAN_TYPE"), "PLCY_QUAL_PLAN_TYPE")
      Else
        annuity.CurrentContributions = Helpers.FormatBaseField("Contact se2", "PLCY_CURR_YR_CONT")
        annuity.YtdContributions = Helpers.FormatBaseField("Contact se2", "PLCY_CURR_YR_CONT")
        annuity.CurrentTaxContributions = Helpers.FormatBaseField("Contact se2", "PLCY_CURR_YR_CONT")
        annuity.PrevTaxContributions = Helpers.FormatBaseField("Contact se2", "PLCY_PRIOR_YR_CONT")
        annuity.GovernmentStatus = Helpers.FormatBaseField("Contact se2", "PLCY_QUAL_PLAN_TYPE")
      End If

      annuity.SurrenderValue = Helpers.FormatBaseField("Contact se2", "DM_SURR_VAL") 'clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_SURR_VAL")),"")
      annuity.WithdrawalCharge = Helpers.FormatBaseField("Contact se2", "N/A")
      annuity.CurrentInterestRates = Helpers.FormatBaseField("Contact se2", "N/A")
      annuity.MaxWithdrawal = Helpers.FormatBaseField("Contact se2", "N/A")
      Return annuity

    End Function

    Private Function LoadDividend(ByVal plcyLoan As DataTable, ByVal dlyMonth As DataTable, ByVal payor As Models.Person.Person, ByVal dividend As Models.LifePlans) As Models.CertInformation
      Dim clsSE2DataHub As New clsSE2DataHub
      LoadLifePlans(plcyLoan, dlyMonth, payor, dividend)
      dividend.Dividend = New Models.Dividend

      If Not (dlyMonth.Rows.Count = 0) Then
        dividend.Dividend.DividendPaidUpCV = Helpers.FormatBaseField(dlyMonth.Rows(0)("DM_PAIDUP_CV"), "DM_PAIDUP_CV")
        dividend.Dividend.DividendDeposits = Helpers.FormatBaseField(dlyMonth.Rows(0)("DM_DAF_CV"), "DM_DAF_CV")
        dividend.Dividend.PuaFaceAmount = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_FACE_PUA")), "DM_FACE_PUA")
      Else
        dividend.Dividend.DividendPaidUpCV = Helpers.FormatBaseField("Contact se2", "DM_PAIDUP_CV")
        dividend.Dividend.DividendDeposits = Helpers.FormatBaseField("Contact se2", "DM_DAF_CV")
        dividend.Dividend.PuaFaceAmount = Helpers.FormatBaseField("Contact se2", "DM_FACE_PUA")
      End If

   
      dividend.Dividend.BaseDividend = Helpers.FormatBaseField("Contact se2", "N/A")

      Return dividend

    End Function

    Private Function LoadInterest(ByVal plcyLoan As DataTable, ByVal dlyMonth As DataTable, ByVal payor As Models.Person.Person, ByVal interest As Models.LifePlans) As Models.CertInformation

      LoadLifePlans(plcyLoan, dlyMonth, payor, interest)
      interest.Interest = New Models.Interest
      interest.Interest.CurrentInterestRates = Helpers.FormatBaseField("Contact se2", "N/A")
      Return interest

    End Function

    Private Function LoadTerm(ByVal plcyLoan As DataTable, ByVal dlyMonth As DataTable, ByVal payor As Models.Person.Person, ByVal term As Models.Term) As Models.CertInformation
      Dim clsSE2DataHub As New clsSE2DataHub

      term.PaidToDate = Helpers.FormatBaseField("Contact se2", "N/A")
      term.Payor = payor

      term.WpRider = Helpers.FormatBaseField("Contact se2", "N/A")
      term.PayorBenefitRider = Helpers.FormatBaseField("Contact se2", "N/A")

      If Not (dlyMonth.Rows.Count = 0) Then
        term.InsuranceAmount = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_FACE_AMT")), "DM_FACE_AMT")
        term.BilledAmount = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_ANN_PREM")), "DM_ANN_PREM")
        term.AdAmount = Helpers.FormatBaseField(dlyMonth.Rows(0)("DM_FACE_ADB"), "DM_FACE_ADB")
      Else
        term.InsuranceAmount = Helpers.FormatBaseField("Contact se2", "DM_FACE_AMT")
        term.BilledAmount = Helpers.FormatBaseField("Contact se2", "DM_ANN_PREM")
        term.AdAmount = Helpers.FormatBaseField("Contact se2", "DM_FACE_ADB")
      End If

      Return term

    End Function

    Public Function LoadCertPersonObj(ByVal certRoles As DataTable) As IEnumerable(Of Models.Person.Person)
      Dim personList As New List(Of Models.Person.Person)
      If Not (certRoles Is Nothing) Then
        For Each role In certRoles.Rows
          Dim person = New Models.Person.Person(role)
          personList.Add(person)
        Next
      End If
      Return personList
    End Function

    Private Sub LoadInsuredInformation(ByVal personList As List(Of Models.Person.Person), ByRef insured As Models.Person.Person, ByRef additionalInsured As List(Of Models.Person.Person))
      additionalInsured = New List(Of Models.Person.Person)
      For Each person In personList

        If (person.Type = Models.Person.RoleType.Insured Or person.Type = Models.Person.RoleType.PrimPart) Then
          insured = person
        End If

        If (person.Type = Models.Person.RoleType.JtCovered) Then
          additionalInsured.Add(person)
        End If

      Next
        If (additionalInsured.Count > 0 AndAlso IsNothing(insured)) Then
          insured = additionalInsured(0)
          additionalInsured.RemoveAt(0)
        End If
    End Sub

    Private Sub LoadOwnerInformation(ByVal personList As List(Of Models.Person.Person), ByRef owner As Models.Person.Person, ByRef additionalOwners As List(Of Models.Person.Person))
      additionalOwners = New List(Of Models.Person.Person)
      For Each person In personList

        If (person.Type = Models.Person.RoleType.Owner) Then
          owner = person
        End If

        If (person.Type = Models.Person.RoleType.ContOwner Or person.Type = Models.Person.RoleType.JtOwner) Then
          additionalOwners.Add(person)
        End If

      Next

      If (additionalOwners.Count > 0 AndAlso IsNothing(owner)) Then
        owner = additionalOwners.Where(Function(x) x.Type = Models.Person.RoleType.JtOwner).FirstOrDefault
        additionalOwners.Remove(owner)
      End If

    End Sub

    Private Sub LoadBaseCertInfo(ByVal plcyLoan As DataTable, ByVal dlyMonth As DataTable, ByRef certInfo As Models.CertInformation)
      Dim clsSE2DataHub As New clsSE2DataHub

      If Not (plcyLoan.Rows.Count = 0) Then
        certInfo.CertNumber = Helpers.FormatBaseField(plcyLoan.Rows(0)("PLCY_POLICY_NUMBER"), "PLCY_POLICY_NUMBER")
        certInfo.IssueAge = Helpers.FormatBaseField(plcyLoan.Rows(0)("PLCY_PRIM_ISSUE_AGE"), "PLCY_PRIM_ISSUE_AGE")
        certInfo.IssueDate = Helpers.FormatBaseField(plcyLoan.Rows(0)("PLCY_ISSUE_DATE"), "PLCY_ISSUE_DATE")
        certInfo.LastPaidDate = Helpers.FormatBaseField(plcyLoan.Rows(0)("PLCY_PREM_PAID_DATE"), "PLCY_PREM_PAID_DATE")
        certInfo.MaturityDate = Helpers.FormatBaseField(plcyLoan.Rows(0)("PLCY_MAT_DATE"), "PLCY_MAT_DATE")

        certInfo.Mode = Helpers.FormatBaseField(clsSE2DataHub.formatMode(plcyLoan.Rows(0)("PLCY_PREM_MODE")), "PLCY_PREM_MODE")
        certInfo.Status = Helpers.FormatBaseField(clsSE2DataHub.formatStatus(plcyLoan.Rows(0)("PLCY_CONT_STATUS").Trim), "PLCY_CONT_STATUS")

        If (certInfo.Status.value.ToUpper = "PAID UP") Then
          certInfo.Mode.value = "Paid Up"
          certInfo.LastPaidDate.value = "N/A"
        End If

      Else
        certInfo.CertNumber = Helpers.FormatBaseField("Contact se2", "PLCY_POLICY_NUMBER")
        certInfo.IssueAge = Helpers.FormatBaseField("Contact se2", "PLCY_PRIM_ISSUE_AGE")
        certInfo.IssueDate = Helpers.FormatBaseField("Contact se2", "PLCY_ISSUE_DATE")
        certInfo.LastPaidDate = Helpers.FormatBaseField("Contact se2", "PLCY_PREM_PAID_DATE")
        certInfo.MaturityDate = Helpers.FormatBaseField("Contact se2", "PLCY_MAT_DATE")
        certInfo.Mode = Helpers.FormatBaseField("Contact se2", "PLCY_PREM_MODE")
        certInfo.Status = Helpers.FormatBaseField("Contact se2", "PLCY_CONT_STATUS")
      End If

      certInfo.LastPaidAmount = Helpers.FormatBaseField("See Transactions", "N/A")
      certInfo.PlanCode = Helpers.FormatBaseField(clsSE2DataHub.GetPlanCode(certInfo.CertNumber.value), "ERA Database Policy>>Plan Field")
      If Not (certInfo.PlanCode.value = "") Then
        certInfo.PlanDescription = Helpers.FormatBaseField(clsSE2DataHub.GetPlanDescription(certInfo.PlanCode.value, "ERA"), "Acquisitions DB>>Description")
      Else
        certInfo.PlanDescription = Helpers.FormatBaseField("Contact se2", "Acquisitions DB>>Description")
      End If

      certInfo.RatingClass = Helpers.FormatBaseField("Contact se2", "PLCY_JT_RT_CLASS") 'dtPolicy.Rows(0)("PLCY_JT_RT_CLASS"), "")

    End Sub

    Private Sub LoadLifePlans(ByVal plcyLoan As DataTable, ByVal dlyMonth As DataTable, ByVal payor As Models.Person.Person, ByRef certInfo As Models.LifePlans)

      Dim clsSE2DataHub As New clsSE2DataHub


      If Not (dlyMonth.Rows.Count = 0) Then
        certInfo.GuaranteedCashValue = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_BASE_CV")), "DM_BASE_CV")
        certInfo.TotalCashValue = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_SURR_VAL")), "DM_SURR_VAL")
        certInfo.InsuranceAmount = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_FACE_AMT")), "DM_FACE_AMT")
        certInfo.DeathBenefit = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_DB")), "DM_DB")
        certInfo.BilledAmount = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_ANN_PREM")), "DM_ANN_PREM")
        certInfo.TermRiderAmount = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_FACE_TERM")), "DM_FACE_TERM")
        certInfo.AdAmount = Helpers.FormatBaseField(dlyMonth.Rows(0)("DM_FACE_ADB"), "DM_FACE_ADB")

        If (certInfo.Status.value.ToUpper = "PAID UP") Then
          certInfo.BilledAmount.value = "Paid Up"
        End If

      Else
        certInfo.GuaranteedCashValue = Helpers.FormatBaseField("Contact se2", "DM_BASE_CV")
        certInfo.TotalCashValue = Helpers.FormatBaseField("Contact se2", "DM_SURR_VAL")
        certInfo.InsuranceAmount = Helpers.FormatBaseField("Contact se2", "DM_FACE_AMT")
        certInfo.DeathBenefit = Helpers.FormatBaseField("Contact se2", "DM_DB")
        certInfo.BilledAmount = Helpers.FormatBaseField("Contact se2", "DM_ANN_PREM")
        certInfo.TermRiderAmount = Helpers.FormatBaseField("Contact se2", "DM_FACE_TERM")
        certInfo.AdAmount = Helpers.FormatBaseField("Contact se2", "DM_FACE_ADB")
      End If

      If Not (plcyLoan.Rows.Count = 0) Then
        If Not (plcyLoan.Rows(0).IsNull("LOAN_IMPORT_DATE")) Then
          certInfo.LoanBalance = Helpers.FormatBaseField(clsSE2DataHub.fmtCurrency(plcyLoan.Rows(0)("LOAN_BAL")), "LOAN_BAL")
          certInfo.LoanInterestRate = Helpers.FormatBaseField((Math.Round(plcyLoan.Rows(0)("LOAN_RATE") * 100, 2)) & "%", "LOAN_RATE")
        End If
      Else
        certInfo.LoanBalance = Helpers.FormatBaseField("Contact se2", "LOAN_BAL")
        certInfo.LoanInterestRate = Helpers.FormatBaseField("Contact se2", "LOAN_RATE")
      End If

      certInfo.SurrenderCharge = Helpers.FormatBaseField("Contact se2", "DM_SURR_CHRG") 'clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_SURR_CHRG"))
      certInfo.NetSurrenderValue = Helpers.FormatBaseField("Contact se2", "DM_SURR_VAL_CALC") 'clsSE2DataHub.fmtCurrency(dlyMonth.Rows(0)("DM_SURR_VAL_CALC"))
      certInfo.MaxWithdrawal = Helpers.FormatBaseField("Contact se2", "N/A")
      certInfo.MaxLoan = Helpers.FormatBaseField("Contact se2", "N/A")
      certInfo.NonForfeitureOption = Helpers.FormatBaseField("Contact se2", "N/A")
      certInfo.RpuAmount = Helpers.FormatBaseField("Contact se2", "N/A")
      certInfo.EiAmount = Helpers.FormatBaseField("Contact se2", "N/A")
      certInfo.EiDate = Helpers.FormatBaseField("Contact se2", "N/A")
      certInfo.PaidToDate = Helpers.FormatBaseField("Contact se2", "N/A")
      certInfo.Payor = payor
      certInfo.TermRider = Helpers.FormatBaseField("Contact se2", "N/A")
      certInfo.WpRider = Helpers.FormatBaseField("Contact se2", "N/A")
      certInfo.PayorBenefitRider = Helpers.FormatBaseField("Contact se2", "N/A")
      certInfo.PwGain = Helpers.FormatBaseField("Contact se2", "N/A")
      certInfo.CsGain = Helpers.FormatBaseField("Contact se2", "N/A")


    End Sub

    Private Function GetPayor(ByVal personList As List(Of Models.Person.Person)) As Models.Person.Person

      Dim payor = personList.Where(Function(x) x.Type = Models.Person.RoleType.Payor).FirstOrDefault

      If (payor Is Nothing) Then
        payor = personList.Where(Function(x) x.Type = Models.Person.RoleType.Owner).FirstOrDefault
      End If

      Return payor
    End Function

    Public Function GetBeneficiaries(ByVal certRoles As DataTable) As List(Of Models.Person.Person)
      Dim beneList As New List(Of Models.Person.Person)
      Dim personList = LoadCertPersonObj(certRoles)

      If Not (personList Is Nothing) Then
        For Each person In personList
          If (person.Type = Models.Person.RoleType.Beneficiary Or person.Type = Models.Person.RoleType.PrimBene Or person.Type = Models.Person.RoleType.OwnerBene Or person.Type = Models.Person.RoleType.ContigentBene Or person.Type = Models.Person.RoleType.TertBene) Then
            beneList.Add(person)
          End If
        Next
      End If

      Return beneList
    End Function

    Private Sub LoadImportDates(ByVal plcyLoan As DataTable, ByVal dlyMonth As DataTable, ByVal certRoles As DataTable, ByRef certInfo As Models.CertInformation)

      If (plcyLoan.Rows.Count > 0) Then
        certInfo.PolicyImportedDate = Helpers.FormatBaseField(plcyLoan.Rows(0)("PLCY_IMPORT_DATE"), "PLCY_IMPORT_DATE")
        If Not (plcyLoan.Rows(0).IsNull("LOAN_IMPORT_DATE")) Then
          certInfo.LoanImportedDate = Helpers.FormatBaseField(plcyLoan.Rows(0)("LOAN_IMPORT_DATE"), "LOAN_IMPORT_DATE")
        Else
          certInfo.LoanImportedDate = Helpers.FormatBaseField("N/A", "LOAN_IMPORT_DATE")
        End If
      Else
        certInfo.PolicyImportedDate = Helpers.FormatBaseField("N/A", "PLCY_IMPORT_DATE")
        certInfo.LoanImportedDate = Helpers.FormatBaseField("N/A", "LOAN_IMPORT_DATE")
      End If

      If (dlyMonth.Rows.Count > 0) Then
        certInfo.DailyMonthlyImportedDate = Helpers.FormatBaseField(dlyMonth.Rows(0)("DM_IMPORT_DATE"), "DM_IMPORT_DATE")
      Else
        certInfo.DailyMonthlyImportedDate = Helpers.FormatBaseField("N/A", "DM_IMPORT_DATE")
      End If

      If (certRoles.Rows.Count > 0) Then
        certInfo.RolesImportedDate = Helpers.FormatBaseField(certRoles.Rows(0)("ROLE_IMPORT_DATE"), "ROLE_IMPORT_DATE")
      Else
        certInfo.RolesImportedDate = Helpers.FormatBaseField("N/A", "ROLE_IMPORT_DATE")
      End If


    End Sub


  End Module



End Namespace



