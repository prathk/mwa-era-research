﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Collections.Generic
Imports Newtonsoft.Json
Imports mwa.Logging.Core


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Se2DataHub
  Inherits System.Web.Services.WebService

  <WebMethod()> _
  Public Function GetCertInfo(ByVal aCertificateNumber As String) As Models.CertInformation
    Dim clsDataHub As New clsSE2DataHub

    Dim plcyLoan = clsDataHub.GetCertInfo(aCertificateNumber)
    Dim dlyMonth = clsDataHub.GetDailyValues(aCertificateNumber)
    Dim certRoles = clsDataHub.GetNameAddressInfo(aCertificateNumber)

    Dim response = New Models.CertInformation
    Dim errorList As New List(Of String)

    Try

      If (plcyLoan.Rows.Count = 0) Then
        errorList.Add("No current records in the Policy File")
        response.ErrorMessage = errorList
        Return response
      End If

      If (dlyMonth.Rows.Count = 0) Then
        errorList.Add("No current records in the Daily Monthly Values file")
      End If

      If (certRoles.Rows.Count = 0) Then
        errorList.Add("No current records in the Roles/Name files")
      End If


      response = Helpers.LoadCertInformation(plcyLoan, dlyMonth, certRoles)
      response.ErrorMessage = errorList
      Return response

    Catch ex As Exception
      Logger.Error(ex)
      Dim errorResponse = New Models.CertInformation
      errorResponse.ErrorMessage.Add(ex.Message)
      Return errorResponse
    End Try

  End Function

  <WebMethod()> _
  Public Function GetTransactions(ByVal aCertificateNumber As String) As Models.Transaction
    Try
      Dim clsDataHub As New clsSE2DataHub
      Dim trans = clsDataHub.getLoanTransInformation(aCertificateNumber)
      Return Helpers.LoadTransactions(trans)
    Catch ex As Exception
      Logger.Error(ex)
      Throw
    End Try
  End Function

  <WebMethod()> _
  Public Function GetBeneficiaries(ByVal aCertificateNumber As String) As List(Of Models.Person.Person)
    Try
      Dim clsDataHub As New clsSE2DataHub
      Dim certRoles = clsDataHub.GetNameAddressInfo(aCertificateNumber)
      Return Helpers.GetBeneficiaries(certRoles)
    Catch ex As Exception
      Logger.Error(ex)
      Throw
    End Try
   
  End Function

End Class