﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPages/mw.master" AutoEventWireup="false" Inherits="prjSE2DataHub.MasterPages_Default" Codebehind="Home.aspx.vb" %>
<%@ MasterType VirtualPath="~/MasterPages/mw.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" Runat="Server">
  <span>SE2 Data Hub</span>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">

  <div style="float:left; margin-left:25%; width: 50%" id="divSearch" class="bs-example" data-bind="visible: !showInformation()">
    <br />
    <fieldset style="text-align:center">
      <legend>Certificate Number</legend>

      <input type="text" maxlength="7" style="width:100px" data-bind="value: searchCertNumber, hasFocus: !showInformation()"  />
      <button class="btn btn-primary" data-bind="click: GetCertInformation">Retrieve <i class="fa fa-spinner fa-spin" data-bind="visible: showSearchSpinner"></i></button>
      
      <span style="color: red"></span>
    </fieldset>



  </div>

  


  <div id="divDetails" data-bind="visible: showInformation">

    <div class="visible-md visible-lg" style="float: left; width:100px; margin-left:5px">
      <div class="btn btn-group-vertical ">
        <button class="btn btn-sm btn-primary" data-bind="click: newCertClick">New Certificate</button>
        <button class="btn btn-sm btn-primary" data-bind="click: showMainClick, enable: !showMain()">Certificate Info</button>
        <button class="btn btn-sm btn-primary" data-bind="click: showBeneficiariesClick, enable: !showBeneficiaries()">Beneficiary</button>
        <button class="btn btn-sm btn-primary" data-bind="click: showAbcClick, enable: !showAbc()">ABC</button>
        <button class="btn btn-sm btn-primary" data-bind="click: showTransactionClick, enable: !showTransactions()">Transactions</button>
        <button class="btn btn-sm btn-primary" data-bind="click: ShowMappingClick, text: showMappingText()" ></button>
      </div>



    <!-- ko if: certInfo() -->
    <div style="float: right; width:100px; margin-right: 5px; margin-top: 5px">
      <ul class="list-group">
        <li class="list-group-item info">
          Policy File<br /> 
          <span data-bind="visible: !showMappings(), text: certInfo().FileImportDates().PolicyImportedDate().Value()"></span>
          <span data-bind="visible: showMappings(), text: certInfo().FileImportDates().PolicyImportedDate().DbField()"></span>
        </li>
        <li class="list-group-item info">
          Daily File<br />  
          <span data-bind="visible: !showMappings(), text: certInfo().FileImportDates().DailyMonthlyImportedDate().Value()"></span>
          <span data-bind="visible: showMappings(), text: certInfo().FileImportDates().DailyMonthlyImportedDate().DbField()"></span>
        </li>
        <li class="list-group-item info" data-bind="visible: !certInfo().BaseCert().IsTerm() && !certInfo().BaseCert().IsAnnuity()">
          Loan File<br />  
          <span data-bind="visible: !showMappings(), text: certInfo().FileImportDates().LoanImportedDate().Value()"></span>
          <span data-bind="visible: showMappings(), text: certInfo().FileImportDates().LoanImportedDate().DbField()"></span>
        </li>
        <li class="list-group-item info">
          Role File<br /> 
          <span data-bind="visible: !showMappings(), text: certInfo().FileImportDates().RolesImportedDate().Value()"></span>
          <span data-bind="visible: showMappings(), text: certInfo().FileImportDates().RolesImportedDate().DbField()"></span>
        </li>
        <li class="list-group-item info" data-bind="visible: hasLoadedTransactions()">
          Trans File<br /> 
          <span data-bind="visible: !showMappings(), text: certInfo().FileImportDates().TransImportedDate().Value()"></span>
          <span data-bind="visible: showMappings(), text: certInfo().FileImportDates().TransImportedDate().DbField()"></span>
        </li>
      </ul>
    </div>
    <!-- /ko -->

    </div>


    <div class="visible-sm visible-xs" style="margin-left:5px; padding-right:10px;">
      <div class="btn btn-group ">
        <button class="btn btn-primary" data-bind="click: newCertClick">New Certificate</button>
        <button class="btn btn-primary" data-bind="click: showMainClick, enable: !showMain()">Certificate Info</button>
        <button class="btn btn-primary" data-bind="click: showBeneficiariesClick, enable: !showBeneficiaries()">Beneficiary</button>
        <button class="btn btn-primary" data-bind="click: showAbcClick, enable: !showAbc()">ABC</button>
        <button class="btn btn-primary" data-bind="click: showTransactionClick, enable: !showTransactions()">Transactions</button>
        <button class="btn btn-primary" data-bind="click: ShowMappingClick, text: showMappingText()" ></button>
      </div>



    <!-- ko if: certInfo() -->
    <div style=" margin-right: 5px; margin-top: 5px">
      <ul class="list-group list-inline">
        <li class="list-group-item info">
          Policy File<br /> 
          <span data-bind="visible: !showMappings(), text: certInfo().FileImportDates().PolicyImportedDate().Value()"></span>
          <span data-bind="visible: showMappings(), text: certInfo().FileImportDates().PolicyImportedDate().DbField()"></span>
        </li>
        <li class="list-group-item info">
          Daily File<br />  
          <span data-bind="visible: !showMappings(), text: certInfo().FileImportDates().DailyMonthlyImportedDate().Value()"></span>
          <span data-bind="visible: showMappings(), text: certInfo().FileImportDates().DailyMonthlyImportedDate().DbField()"></span>
        </li>
        <li class="list-group-item info" data-bind="visible: !certInfo().BaseCert().IsTerm() && !certInfo().BaseCert().IsAnnuity()">
          Loan File<br />  
          <span data-bind="visible: !showMappings(), text: certInfo().FileImportDates().LoanImportedDate().Value()"></span>
          <span data-bind="visible: showMappings(), text: certInfo().FileImportDates().LoanImportedDate().DbField()"></span>
        </li>
        <li class="list-group-item info">
          Role File<br /> 
          <span data-bind="visible: !showMappings(), text: certInfo().FileImportDates().RolesImportedDate().Value()"></span>
          <span data-bind="visible: showMappings(), text: certInfo().FileImportDates().RolesImportedDate().DbField()"></span>
        </li>
        <li class="list-group-item info" data-bind="visible: hasLoadedTransactions()">
          Trans File<br /> 
          <span data-bind="visible: !showMappings(), text: certInfo().FileImportDates().TransImportedDate().Value()"></span>
          <span data-bind="visible: showMappings(), text: certInfo().FileImportDates().TransImportedDate().DbField()"></span>
        </li>
      </ul>
    </div>
    <!-- /ko -->

    </div>



    <!-- ko if: certInfo() -->

    <div id="divMain" style="width:80%; float:left; margin-left: 20px;">
      <div ><h1><span data-bind="text: certInfo().BaseCert().Type()"><br /></span></h1></div>
      <br />
      <fieldset>
        <legend><span data-bind="text: certInfo().BaseCert().InsuredAnnuitant()"></span>/Owner Information</legend>

        <table style="width: 100%">
          <tr>

            <td>
              <span data-bind="text: certInfo().BaseCert().InsuredAnnuitant()"></span>:
            </td>
            <td>
              <span class="bold" data-bind="visible: !showMappings(), text: certInfo().Insured().InsuredName().Value()" ></span>
              <span class="bold" data-bind="visible: showMappings(), text: certInfo().Insured().InsuredName().DbField()" ></span>
            </td>

            <td>
              <span data-bind="text: certInfo().BaseCert().InsuredAnnuitant()"></span> DOB:
            </td>
            <td >
              <span class="bold" data-bind="visible: !showMappings(), text: certInfo().Insured().InsuredDOB().Value()"></span>
              <span class="bold" data-bind="visible: showMappings(), text: certInfo().Insured().InsuredDOB().DbField()"></span>
            </td>

            <td>Gender:</td>
            <td>
              <span class="bold" data-bind="visible: !showMappings(), text: certInfo().Insured().InsuredGender().Value()"></span>
              <span class="bold" data-bind="visible: showMappings(), text: certInfo().Insured().InsuredGender().DbField()"></span>
            </td>

          </tr>

          <!-- ko if: certInfo().Insured().HasAdditional() -->
          <tr >
            <td colspan="7">
              <a href="#" style="color:red; font:bold" data-bind="click: AdditionalInsuredClick">
                <i class="fa fa-plus-square" data-bind="  visible: !showAdditionalInsured()"></i>
                <i class="fa fa-minus-square" data-bind="  visible: showAdditionalInsured()"></i>
                <span data-bind="  text: additionalInsuredText"></span>
              </a>
            </td>
          </tr>

          <tr data-bind="visible: showAdditionalInsured">
            <td colspan="7">
              <table style="width: 100%">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>DOB</th>
                    <th>Address</th>
                    <th>SSN</th>
                    <th>Gender</th>
                    <th>Phone</th>
                    <th>Rating Class</th>
                  </tr>
                  <tr>
                    <td colspan="7"><hr /></td>
                  </tr>
                </thead>
                <tbody data-bind="template: { foreach: certInfo().Insured().AdditionalInsured }">
                  <tr data-bind="css: { 'alt-row': ($index() % 2 == 0) }">
                    <td >
                      <span data-bind="visible: !$root.showMappings(), text: Name().Value()"></span>
                      <span data-bind="visible: $root.showMappings(), text: Name().DbField()"></span>
                    </td>
                    <td >
                      <span data-bind="visible: !$root.showMappings(), text: DOB().Value()"></span>
                      <span data-bind="visible: $root.showMappings(), text: DOB().DbField()"></span>
                    </td>
                    <td >
                      <span data-bind="visible: !$root.showMappings(), html: Address().Value()"></span>
                      <span data-bind="visible: $root.showMappings(), html: Address().DbField()"></span>
                    </td>
                    <td >
                      <span data-bind="visible: !$root.showMappings(), text: SSN().Value()"></span>
                      <span data-bind="visible: $root.showMappings(), text: SSN().DbField()"></span>
                    </td>
                    <td >
                      <span data-bind="visible: !$root.showMappings(), text: Gender().Value()"></span>
                      <span data-bind="visible: $root.showMappings(), text: Gender().DbField()"></span>
                    </td>
                    <td >
                      <span data-bind="visible: !$root.showMappings(), text: Phone().Value()"></span>
                      <span data-bind="visible: $root.showMappings(), text: Phone().DbField()"></span>
                    </td>
                    <td >
                      <span data-bind="visible: !$root.showMappings(), text: $root.certInfo().BaseCert().RatingClass().Value()"></span>
                      <span data-bind="visible: $root.showMappings(), text: $root.certInfo().BaseCert().RatingClass().DbField()"></span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
          <!-- /ko -->

          <tr>
            <td colspan="7"><hr /></td>
          </tr>

          <tr>
            <td style="vertical-align: top;"><span>Owner:</span></td>
            <td style="vertical-align: top;">
              <span id="lblOwnerName" class="bold" data-bind="visible: !showMappings(), text: certInfo().Owner().OwnerName().Value()"></span>
              <span id="Span1" class="bold" data-bind="visible: showMappings(), text: certInfo().Owner().OwnerName().DbField()"></span>
            </td>
            <td ><span>Owner SSN:</span></td>
            <td>
              <span id="lblOwnerSSN" class="bold" data-bind="visible: !showMappings(), text: certInfo().Owner().OwnerSSN().Value()" ></span>
              <span id="Span2" class="bold" data-bind="visible: showMappings(), text: certInfo().Owner().OwnerSSN().DbField()" ></span>
            </td>
            <td ><span>Owner Phone:</span></td>
            <td>
              <span id="lblOwnerPhone" class="bold" data-bind="visible: !showMappings(), text: certInfo().Owner().OwnerPhone().Value()"></span>
              <span id="Span3" class="bold" data-bind="visible: showMappings(), text: certInfo().Owner().OwnerPhone().DbField()"></span>
            </td>
          </tr>
          <tr>
            <td  rowspan="2" style="vertical-align: top"><span>Owner Address:</span></td>
            <td style="vertical-align: top" rowspan="2">
              <span id="lblOwnerAddress" class="bold" data-bind="visible: !showMappings(), html: certInfo().Owner().OwnerAddress().Value()" ></span>
              <span id="Span4" class="bold" data-bind="visible: showMappings(), html: certInfo().Owner().OwnerAddress().DbField()" ></span>
            </td>
            <td>
              <span id="lblRatingClass" data-bind="visible: !certInfo().BaseCert().IsAnnuity()" >Rating Class</span>
            </td>
            <td>
              <span class="bold" id="lblRatingClassValue" data-bind="visible: !certInfo().BaseCert().IsAnnuity() && !showMappings(), text: certInfo().BaseCert().RatingClass().Value()"></span>
              <span class="bold" id="Span14" data-bind="visible: !certInfo().BaseCert().IsAnnuity() && showMappings(), text: certInfo().BaseCert().RatingClass().DbField()"></span>
            </td>
            <td>Gender:</td>
            <td>
              <span id="lblOwnerGender" class="bold" data-bind="visible: !showMappings(), text: certInfo().Owner().Gender().Value()"></span>
              <span id="Span5" class="bold" data-bind="visible: showMappings(), text: certInfo().Owner().Gender().DbField()"></span>
            </td>
          </tr>
        </table>

        <!-- ko if: certInfo().Owner().HasAdditional() -->
        <a href="#" id="lbMultipleOwner" style="color:red" class="value" data-bind="click: AdditionalOwnersClick"><i class="fa fa-plus-square" data-bind="  visible: !showAdditionalOwners()"></i><i class="fa fa-minus-square" data-bind="  visible: showAdditionalOwners()"></i>   <span data-bind="  text: additionalOwnerText"></span></a>
        <div data-bind="visible: showAdditionalOwners">
          <table style="width: 100%;">
            <thead>
              <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Address</th>
                <th>SSN</th>
                <th>Gender</th>
                <th>Phone</th>
                <th>Rating Class</th>
              </tr>
              <tr><td colspan="7"><hr /></td></tr>
            </thead>
            <tbody data-bind="template: {foreach: certInfo().Owner().AdditionalOwners}"> <!-- DataBind -->
              <tr data-bind="css: { 'alt-row': ($index() % 2 == 0) }">
                <td >
                  <span data-bind="visible: !$root.showMappings(), text: Name().Value()"></span>
                  <span data-bind="visible: $root.showMappings(), text: Name().DbField()"></span>
                </td>
                <td >
                  <span data-bind="visible: !$root.showMappings(), text: Type().Value()"></span>
                  <span data-bind="visible: $root.showMappings(), text: Type().DbField()"></span>
                </td>
                <td >
                  <span data-bind="visible: !$root.showMappings(), html: Address().Value()"></span>
                  <span data-bind="visible: $root.showMappings(), html: Address().DbField()"></span>
                </td>
                <td >
                  <span data-bind="visible: !$root.showMappings(), text: SSN().Value()"></span>
                  <span data-bind="visible: $root.showMappings(), text: SSN().DbField()"></span>
                </td>
                <td >
                  <span data-bind="visible: !$root.showMappings(), text: Gender().Value()"></span>
                  <span data-bind="visible: $root.showMappings(), text: Gender().DbField()"></span>
                </td>
                <td >
                  <span data-bind="visible: !$root.showMappings(), text: Phone().Value()"></span>
                  <span data-bind="visible: $root.showMappings(), text: Phone().DbField()"></span>
                </td>
                <td >
                  <span data-bind="visible: !$root.showMappings(), text: RatingClass().Value()"></span>
                  <span data-bind="visible: $root.showMappings(), text: RatingClass().DbField()"></span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /ko -->

      </fieldset>
      
      <div><br /></div>

      <fieldset>
        <legend>Certificate Information</legend>
        <table style="width: 100%" data-bind="with: certInfo().BaseCert() ">
          
          <tr>
            <td>Cert No:</td>
            <td>
              <span class="bold" id="lblCertNo" data-bind="visible: !$root.showMappings(), text: CertNumber().Value()"></span>
              <span class="bold" id="Span6" data-bind="visible: $root.showMappings(), text: CertNumber().DbField()"></span>
            </td>
            <td>Status:</td>
            <td>
              <span class="bold" id="lblCertStatus" data-bind="visible: !$root.showMappings(), text: Status().Value()"></span>
              <span class="bold" id="Span7" data-bind="visible: $root.showMappings(), text: Status().DbField()"></span>
            </td>
            <td>
              <span id="lblInsAmount" data-bind="visible: !IsAnnuity()">Insurance Amount:</span>
              <span id="lblGovtStatus" data-bind="visible: IsAnnuity()">Government Status:</span>
            </td>
            <td>
              <span class="bold" id="lblInsAmountVal" data-bind="visible: !IsAnnuity() && !$root.showMappings(), text: InsuranceAmount().Value()"></span>
              <span class="bold" id="lblGovtStatusVal" data-bind="visible: IsAnnuity() && !$root.showMappings(), text: GovernmentStatus().Value()"></span>
              <span class="bold" id="Span8" data-bind="visible: !IsAnnuity() && $root.showMappings(), text: InsuranceAmount().DbField()"></span>
              <span class="bold" id="Span9" data-bind="visible: IsAnnuity() && $root.showMappings(), text: GovernmentStatus().DbField()"></span>
            </td>
          </tr>

          <tr>
            <td>Issue Age:</td>
            <td>
              <span class="bold" id="lblIssueAge" data-bind="visible: !$root.showMappings(), text: IssueAge().Value()"></span>
              <span class="bold" id="Span10" data-bind="visible: $root.showMappings(), text: IssueAge().DbField()"></span>
            </td>
            <td>Issue Date:</td>
            <td>
              <span class="bold" id="lblIssueDate" data-bind="visible: !$root.showMappings(), text: IssueDate().Value()"></span>
              <span class="bold" id="Span11" data-bind="visible: $root.showMappings(), text: IssueDate().DbField()"></span>
            </td>
            <td>
              <span data-bind="visible: !IsTerm()">Maturity Date:</span>
              <span data-bind="visible: IsTerm()">Expiry Date:</span>
            </td>
            <td>
              <span class="bold" id="lblSomethingMat" data-bind="visible: !$root.showMappings(), text: MaturityDate().Value()"></span>
               <span class="bold" id="Span12" data-bind="visible: $root.showMappings(), text: MaturityDate().DbField()"></span>
            </td>
          </tr>

          <tr>
            <td>Plan:</td>
            <td colspan="3">
              <span class="bold" id="lblPlan" data-bind="visible: !$root.showMappings(), text: PlanInformation()"></span>
              <span class="bold" id="Span13" data-bind="visible: $root.showMappings()"> MWA ERA Database: Policy>>Plan Field && Acquisitions DB>>Description</span>
            </td>
          </tr>

        </table>
      </fieldset>

      <div style="width:100%"><hr /></div>

      <div data-bind="visible: showMain()">
        
        <table style="width:100%">
          
          <tr>

            <td id="column1" style="width:50%; vertical-align: top;">
              <table style="width: 100%">
                
                <tr data-bind="visible: !certInfo().BaseCert().IsTerm()">
                  <td>
                    <fieldset>
                      <legend>Cash Value</legend>
                      <table style="width:85%" data-bind="with: certInfo().CashValue()">
                        
                        <tr data-bind="visible: !$root.certInfo().BaseCert().IsAnnuity()">
                          <td>Guaranteed Cash Value:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: GuaranteedCashValue().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: GuaranteedCashValue().DbField()"></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: $root.certInfo().BaseCert().IsAnnuity()">
                          <td>Cash Value:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: CashValue().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: CashValue().DbField()"></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: $root.certInfo().BaseCert().IsAnnuity()">
                          <td>Year End Cash Value:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: YearEndCashValue().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: YearEndCashValue().DbField()"></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: $root.certInfo().BaseCert().IsAnnuity()">
                          <td>Surrender Value:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: SurrenderValue().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: SurrenderValue().DbField()"></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: $root.certInfo().BaseCert().IsDividend()">
                          <td>Dividend Cash Value:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: DividendCashValue().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: DividendCashValue().DbField()"></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: $root.certInfo().BaseCert().IsDividend()">
                          <td>Base Dividend:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: BaseDividend().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: BaseDividend().DbField()"></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: $root.certInfo().BaseCert().IsDividend()">
                          <td>Dividend Option:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: DividendOption().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: DividendOption().DbField()"></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: !$root.certInfo().BaseCert().IsAnnuity()">
                          <td>Total Cash Value:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: TotalCashValue().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: TotalCashValue().DbField()"></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: !$root.certInfo().BaseCert().IsAnnuity()">
                          <td>Surrender Charge:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: SurrenderCharge().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: SurrenderCharge().DbField()"></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: !$root.certInfo().BaseCert().IsAnnuity()">
                          <td>Net Surrender Value:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: NetSurrenderValue().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: NetSurrenderValue().DbField()"></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: !$root.certInfo().BaseCert().IsDividend() && !$root.certInfo().BaseCert().IsTerm()">
                          <td>Current Interest Rate:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: CurrentInterestRate().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: CurrentInterestRate().DbField()"></span>
                          </td>
                        </tr>

                      </table>
                    </fieldset>
                  </td>
                </tr>

                <tr data-bind="visible: !certInfo().BaseCert().IsTerm() && !certInfo().BaseCert().IsAnnuity()">
                  <td>
                    <fieldset>
                      <legend>Death Benefit</legend>
                      <table style="width: 85%">
                        <tr>
                          <td>Insurance Amount:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !showMappings(), text: certInfo().BaseCert().InsuranceAmount().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: showMappings(), text: certInfo().BaseCert().InsuranceAmount().DbField()"></span>
                          </td>
                        </tr>
                        <tr data-bind="visible: certInfo().BaseCert().IsDividend()">
                          <td>PUA Face Amount:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !showMappings(), text: certInfo().Death().PuaFaceAmount().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: showMappings(), text: certInfo().Death().PuaFaceAmount().DbField()"></span>
                          </td>
                        </tr>
                        <tr>
                          <td>Death Benefit:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !showMappings(), text: certInfo().Death().DeathBenefit().Value()"></span>
                             <span class="bold pull-right" data-bind="visible: showMappings(), text: certInfo().Death().DeathBenefit().DbField()"></span>
                          </td>
                        </tr>
                      </table>
                    </fieldset>
                  </td>
                </tr>

                <tr data-bind="visible: !certInfo().BaseCert().IsTerm()">
                  <td>
                    <fieldset>
                      <legend>Withdrawal Information</legend>
                      <table style="width: 85%" data-bind="with: certInfo().Withdrawal()">
                        
                        <tr>
                          <td>Max Withdrawal:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: MaxWithdrawal().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: MaxWithdrawal().DbField()"></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: $root.certInfo().BaseCert().IsAnnuity()">
                          <td>Withdrawal Charge:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: WithdrawalCharge().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: WithdrawalCharge().DbField()"></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: $root.certInfo().BaseCert().IsAnnuity()">
                          <td>Current Withdrawals:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: CurrentWithdrawals().Value() "></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: CurrentWithdrawals().DbField() "></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: $root.certInfo().BaseCert().IsAnnuity()">
                          <td>YTD Withdrawals:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: YtdWithdrawals().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: YtdWithdrawals().DbField()"></span>
                          </td>
                        </tr>

                      </table>
                    </fieldset>
                  </td>
                </tr>

                <tr data-bind="visible: !certInfo().BaseCert().IsAnnuity()">
                  <td>
                    <fieldset>
                      <legend>Riders</legend>
                      <table style="width: 85%" data-bind="with: certInfo().Riders()">
                        
                        <tr data-bind="visible: !$root.certInfo().BaseCert().IsTerm()">
                          <td>Term Rider:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: TermRider().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: TermRider().DbField()"></span>
                          </td>
                        </tr>
                        
                        <tr data-bind="visible: !$root.certInfo().BaseCert().IsTerm()">
                          <td>Term Rider Amount:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: TermRiderAmount().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: TermRiderAmount().DbField()"></span>
                          </td>
                        </tr>  

                        <tr>
                          <td>AD Rider:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: AdRider()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings()">If AdAmount > 0 Show Yes</span>
                          </td>
                        </tr>

                        <tr>
                          <td>AD Amount:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: AdAmount().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: AdAmount().DbField()"></span>
                          </td>
                        </tr>

                        <tr>
                          <td>WP Rider:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: WpRider().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: WpRider().DbField()"></span>
                          </td>
                        </tr>

                        <tr>
                          <td>Payor Benefit Rider:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: PayorBenefit().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: PayorBenefit().DbField()"></span>
                          </td>
                        </tr>

                      </table>
                    </fieldset>
                  </td>
                </tr>

                <tr data-bind="visible: !certInfo().BaseCert().IsTerm() && !certInfo().BaseCert().IsAnnuity()" >
                  <td>
                    <fieldset>
                      <legend>Certificate Options</legend>
                      <table style="width: 85%" data-bind="with: certInfo().CertOptions()">
                        
                        <tr>
                          <td>Nonforfeiture Option:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: NonForfeitureOptions().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: NonForfeitureOptions().DbField()"></span>
                          </td>
                        </tr>

                        <tr>
                          <td>RPU Amount:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: RpuAmount().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: RpuAmount().DbField()"></span>
                          </td>
                        </tr>

                        <tr>
                          <td>EI Amount:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: EiAmount().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: EiAmount().DbField()"></span>
                          </td>
                        </tr>

                        <tr>
                          <td>EI Date:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: EiDate().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: EiDate().DbField()"></span>
                          </td>
                        </tr>

                      </table>
                    </fieldset>
                  </td>
                </tr>

                <tr data-bind="visible: certInfo().BaseCert().IsAnnuity()">
                  <td>
                    <fieldset>
                      <legend>Tax Year Contributions</legend>
                      <table style="width: 85%" data-bind="with: certInfo().Contributions()">
                        
                        <tr>
                          <td>Current Tax Year:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: CurrentTaxYearContributions().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: CurrentTaxYearContributions().DbField()"></span>
                          </td>
                        </tr>

                        <tr>
                          <td>Previous Tax Year:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: PreviousTaxYearContributions().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: PreviousTaxYearContributions().DbField()"></span>
                          </td>
                        </tr>

                      </table>
                    </fieldset>
                  </td>
                </tr>

              </table>
            </td>

            <td>&nbsp;</td>

            <td id="column2" style="width:50%; vertical-align: top;">
              <table style="width: 100%">

                <tr>
                  <td>
                    <fieldset>
                      <legend>Billing</legend>
                      <table style="width: 85%" data-bind="with: certInfo().Billing()">

                        <tr data-bind="visible: !$root.certInfo().BaseCert().IsAnnuity()">
                          <td>Paid To Date:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: PaidToDate().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: PaidToDate().DbField()"></span>
                          </td>
                        </tr>

                        <tr>
                          <td>Mode:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: Mode().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: Mode().DbField()"></span>
                          </td>
                        </tr>

                        <tr data-bind="visible: !$root.certInfo().BaseCert().IsAnnuity()">
                          <td>Billed Amount:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: BilledAmount().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: BilledAmount().DbField()"></span>
                          </td>
                        </tr>

                        <tr>
                          <td>Last Paid Date:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: LastPaidDate().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: LastPaidDate().DbField()"></span>
                          </td>
                        </tr>

                        <tr>
                          <td>Last Paid Amount:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: LastPaidAmount().Value()" ></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: LastPaidAmount().DbField()" ></span>
                          </td>
                        </tr>

                      </table>
                    </fieldset>
                  </td>
                </tr>

                <tr data-bind="visible: !certInfo().BaseCert().IsAnnuity()">
                  <td>
                    <fieldset>
                      <legend>Payor</legend>
                      <table style="width: 85%" data-bind="with: certInfo().Payor()" >
                        
                        <tr>
                          <td>Payor Name:</td>
                          <td>
                            <span class="bold pull-right" style="text-align:right" data-bind="visible: !$root.showMappings(), text: PayorName().Value()"></span>
                            <span class="bold pull-right" style="text-align:right" data-bind="visible: $root.showMappings(), text: PayorName().DbField()"></span>
                          </td>
                        </tr>

                        <tr>
                          <td>Payor Address:</td>
                          <td>
                            <span class="bold pull-right" style="text-align:right" data-bind="visible: !$root.showMappings(), html: PayorAddress().Value()"></span>
                            <span class="bold pull-right" style="text-align:right" data-bind="visible: $root.showMappings(), html: PayorAddress().DbField()"></span>
                          </td>
                        </tr>

                      </table>
                    </fieldset>
                  </td>
                </tr>

                <tr data-bind="visible: !certInfo().BaseCert().IsTerm() && !certInfo().BaseCert().IsAnnuity()">
                  <td>
                    <fieldset>
                      <legend>Loan Information</legend>
                      <table style="width: 85%" data-bind="with: certInfo().Loan()">
                        
                        <tr>
                          <td>Maximum Loan:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: MaxLoan().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: MaxLoan().DbField()"></span>
                          </td>
                        </tr>

                        <tr>
                          <td>Loan Balance:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: LoanBalance().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: LoanBalance().DbField()"></span>
                          </td>
                        </tr>

                        <tr>
                          <td>Loan Rate:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: LoanRate().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: LoanRate().DbField()"></span>
                          </td>
                        </tr>

                      </table>
                    </fieldset>
                  </td>
                </tr>

                <tr data-bind="visible: certInfo().BaseCert().IsAnnuity()">
                  <td>
                    <fieldset>
                      <legend>Contribution History</legend>
                      <table style="width: 85%" data-bind="with: certInfo().Contributions()">
                        
                        <tr>
                          <td>Current Contributions:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: CurrentContributions().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: CurrentContributions().DbField()"></span>
                          </td>
                        </tr>

                        <tr>
                          <td>YTD Contributions:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: YtdContributions().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: YtdContributions().DbField()"></span>
                          </td>
                        </tr>

                      </table>
                    </fieldset>
                  </td>
                </tr>

                <tr data-bind="visible: !certInfo().BaseCert().IsTerm()">
                  <td>
                    <fieldset>
                      <legend>Gain Information <span data-bind="visible: certInfo().BaseCert().IsAnnuity()"> Contact se2</span></legend>
                      <table style="width: 85%" data-bind="with: certInfo().Gain(),visible: !certInfo().BaseCert().IsAnnuity()">
                        
                        <tr>
                          <td>PW Gain:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: PwGain().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: PwGain().DbField()"></span>
                          </td>
                        </tr>  

                        <tr>
                          <td>CS Gain:</td>
                          <td>
                            <span class="bold pull-right" data-bind="visible: !$root.showMappings(), text: CsGain().Value()"></span>
                            <span class="bold pull-right" data-bind="visible: $root.showMappings(), text: CsGain().DbField()"></span>
                          </td>
                        </tr>

<%--                        <tr>
                          <td>PW Gain NQ:</td>
                          <td>
                            <span class="bold pull-right" data-bind="text: PwGainNQ"></span>
                          </td>
                        </tr>
                        
                        <tr>
                          <td>Final Gain NQ:</td>
                          <td>
                            <span class="bold pull-right" data-bind="text: FinalGainNQ"></span>
                          </td>
                        </tr>
                        
                        <tr>
                          <td>Cs PW Gain Roth:</td>
                          <td>
                            <span class="bold pull-right" data-bind="text: CsPwGainRoth"></span>
                          </td>
                        </tr>
                        
                        <tr>
                          <td>Death Gain Roth:</td>
                          <td>
                            <span class="bold pull-right" data-bind="text: DeathGainRoth"></span>
                          </td>
                        </tr>
                        
                        <tr>
                          <td>PW Gain NQ:</td>
                          <td>
                            <span class="bold pull-right" data-bind="text: PwGainNQ"></span>
                          </td>
                        </tr>--%>

                      </table>
                    </fieldset>
                  </td>
                </tr>

              </table>
            </td>
          </tr>
        </table>

      </div>

      <div id="divTransactions" style="width:80%; float:left; margin-left: 20px;" data-bind="visible: showTransactions()">
<%--        <fieldset style="margin-left: 25%; width:50%">
          <legend>Filter Criteria</legend>
          <table style="width: 100%">
           <tr>
              <td>Transaction Type:</td>
              <td>
                <select class="dropdown" id="ddlTransType" data-bind="options: certTypes, value: selectedTransType, optionsCaption: 'Choose...'" ></select>

              </td>
            </tr>
            <tr>
              <td>Posted Date:</td>
              <td>
                <table style="width:100%">
                  <tr>
                    <td style="width:93px">
                      <input class="date" id="txtStartPostedDate" style="width:90px" data-bind="value: startPostedDate" />
                    </td>
                    <td>
                      To
                    </td>
                    <td>
                      <input class="date" style="width:90px" data-bind="value: endPostedDate" />
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td>Effective Date:</td>
              <td>
                <table style="width:100%">
                  <tr>
                    <td style="width:93px">
                      <input class="date" style="width:90px" data-bind="value: startEffectiveDate" />
                    </td>
                    <td>
                      To
                    </td>
                    <td>
                      <input class="date" style="width:90px" data-bind="value: endEffectiveDate" />
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <div style="margin-left: 25%;" class="btn btn-group">
            <button class="btn btn-primary" data-bind="click: ClearFiltersClick">Clear</button>
            <button class="btn btn-primary" data-bind="click: showMainClick">Back</button>
          </div>
        </fieldset>--%>

        <fieldset>
          <legend>Transactions</legend>
          <table style="width:100%">
            <thead style="background-color:#006094; color:#fff">
              <tr>
                <th>Transaction No</th>
                <th>Type &nbsp;&nbsp;
                  <a href="#" id="aTypeFilter" style="color:#fff" data-bind="popover: { template: 'typePopover', title: 'Type Filter', placement: 'top' }">
                    <i class="fa fa-filter" data-bind="visible: !selectedTransType()"></i>
                    <i class="fa fa-edit" data-bind="visible: selectedTransType()"></i>
                  </a>
                  <a href="#" style="color:#fff" data-bind="click: ClearTypeFilterClick, visible: selectedTransType() ">
                    <i class="fa fa-times-circle"></i>
                  </a>
                </th>
                <th>Posted Date&nbsp;&nbsp;
                  <a href="#" id="aPostedFilter" style="color:#fff" data-bind="popover: { template: 'postedDatePopover', title: 'Posted Date Filter', placement: 'top' }">
                    <i class="fa fa-filter" data-bind="visible: !hasPostedFilter()"></i>
                    <i class="fa fa-edit" data-bind="visible: hasPostedFilter()"></i>
                  </a>
                  <a href="#" style="color:#fff" data-bind="click: ClearOnPostedDateClick, visible: hasPostedFilter() ">
                    <i class="fa fa-times-circle"></i>
                  </a>
                </th>
                <th>Effective Date&nbsp;&nbsp;
                  <a href="#" id="aEffectiveFilter" style="color:#fff" data-bind="popover: { template: 'effectiveDatePopover', title: 'Effective Date Filter', placement: 'top' }">
                    <i class="fa fa-filter" data-bind="visible: !hasEffectiveFilter()"></i>
                    <i class="fa fa-edit" data-bind="visible: hasEffectiveFilter()"></i>
                  </a>
                  <a href="#" style="color:#fff" data-bind="click: ClearOnEffectiveDateClick, visible: hasEffectiveFilter() ">
                    <i class="fa fa-times-circle"></i>
                  </a>
                </th>
                <th>Reversal Date</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody data-bind="template: { foreach: filterTable }">
              <tr  data-bind="css: { 'alt-row': ($index() % 2 == 0) }">
                <td data-bind="text: id"></td>
                <td data-bind="text: type"></td>
                <td data-bind="text: postedDate"></td>
                <td data-bind="text: effectiveDate"></td>
                <td data-bind="text: reversalDate"></td>
                <td data-bind="text: amount"></td>
              </tr>
            </tbody>
          </table>
        </fieldset>
     
        

      </div>

      <div id="divBeneficiaries" data-bind="visible: showBeneficiaries">
        
        <fieldset style="width:75%">
          <legend>Principal Beneficiaries</legend>
          <table style="width:75%"  data-bind="visible: certBene()">
            <thead>
              <tr>
                <th></th>
                <th>Name</th>
                <th>Address</th>
                <th>Percent</th>
              </tr>
            </thead>
            <tbody data-bind="template: { foreach: certBene}">
              <!-- ko if: Type() == 9 -->
              <tr>
                <td></td>
                <td data-bind="text: Name().Value()"></td>
                <td data-bind="html: Address().Value()"></td>
                <td data-bind="text: Percent().Value()"></td>
              </tr>
              <!-- /ko -->
            </tbody>
          </table>
        </fieldset>

        <fieldset  style="width:75%">
          <legend>Contingent Beneficiaries</legend>
          <table style="width:75%" data-bind="visible: certBene()">
            <thead>
              <tr>
                <th></th>
                <th>Name</th>
                <th>Address</th>
                <th>Percent</th>
              </tr>
            </thead>
            <tbody data-bind="template: { foreach: certBene }">
              <!-- ko if: Type() == 14 -->
              <tr>
                <td></td>
                <td data-bind="text: Name().Value()"></td>
                <td data-bind="html: Address().Value()"></td>
                <td data-bind="text: Percent().Value()"></td>
              </tr>
              <!-- /ko -->
            </tbody>
          </table>
        </fieldset>

      </div>

      <div id="divABC" data-bind="visible: showAbc">
        
        <fieldset  style="width:75%">
          <legend>ABC Information</legend>
          <table style="width: 100%">
            <tr>
              <td>Payor Name:</td>
              <td><span class="bold">Contact se2</span></td>
              <td style="vertical-align:top">Payor Address:</td>
              <td rowspan="2" style="vertical-align:top"><span class="bold">Contact se2</span></td>
            </tr>
            <tr>
              <td>Bank Name:</td>
              <td><span class="bold">Contact se2</span></td>
              <td></td>
            </tr>
            <tr>
              <td>Routing Number:</td>
              <td><span class="bold">Contact se2</span></td>
              <td>Account Number:</td>
              <td><span class="bold">Contact se2</span></td>
            </tr>
          </table>
        </fieldset>

        <fieldset  style="width:75%">
          <legend>Payments</legend>
          <table style="width:100%" ></table>
        </fieldset>

      </div>

    </div>



    

  <%--<div data-bind="text: ko.toJSON(selectedTransType)"></div>--%>

    <%--<div data-bind="text: ko.toJSON(certTypes)"></div>--%>

    <!-- /ko -->
  </div>
    


  <script src="App/Models/Models.js"></script>
  <script src="App/DataContext/DataContext.js"></script>
  <script src="App/ViewModels/ViewModel.js"></script>


</asp:Content>

<asp:Content ContentPlaceHolderID="Footer" runat="server">

  <script type="text/html" id="typePopover" >
    <form>
      <select class="dropdown" id="Select1" data-bind="options: certTypes, value: selectedTransType, optionsCaption: 'Choose...'" ></select>
      <div class="btn btn-group btn-group-sm">
        <button class="btn btn-primary" data-bind="click: FilterOnTypeClick"><i class="fa fa-check"></i></button>
        <button class="btn btn-danger" data-bind="click: ClearTypeFilterClick"><i class="fa fa-times"></i></button>
      </div>
    </form>
  </script>


  <script type="text/html" id="postedDatePopover" >
    <form>
      <div style="color: black">
        <input style="width:90px" data-bind="value: startPostedDate" class="date" />
        <label>To</label>
        <input style="width:90px" data-bind="value: endPostedDate" class="date" />
        <div class="btn btn-group btn-group-sm pull-right">
          <button class="btn btn-primary" data-bind="click: FilterOnPostedDateClick"><i class="fa fa-check"></i></button>
          <button class="btn btn-danger" data-bind="click: ClearOnPostedDateClick"><i class="fa fa-times"></i></button>
        </div>
      </div>
    </form>
  </script>

  <script type="text/html" id="effectiveDatePopover" >
    <form>
      <div style="color: black; width: 100%">
        <input class="date" id="Text2" style="width:90px" data-bind="value: startEffectiveDate"  />
        <span>To</span>
        <input class="date" style="width:90px" data-bind="value: endEffectiveDate"  />
        <div class="btn btn-group btn-group-sm pull-right" >
          <button class="btn btn-primary" data-bind="click: FilterOnEffectiveDateClick" ><i class="fa fa-check"></i></button>
          <button class="btn btn-danger" data-bind="click: ClearOnEffectiveDateClick" ><i class="fa fa-times"></i></button>
        </div>
      </div>
    </form>
  </script>


</asp:Content>