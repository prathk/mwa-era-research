﻿$(function () {
  $("#btnRetrieve").click(function () {//Validate Cert Number is numeric
    if ($("#txtCertNum").val().length > 0) {
      var intRegex = /^\d+$/;
      if (intRegex.test($("#txtCertNum").val())) { $("#lblCertRetrieveErr").html(""); }
      else { $("#lblCertRetrieveErr").html("Certificate Number must be numeric"); $("#txtCertNum").focus(); return false; }
    } else { $("#lblCertRetrieveErr").html("Enter Certificate Number"); $("#txtCertNum").focus(); return false; }
  });
  $("#lblMultipleOwner").click(function () {
    $("#pnlMultipleOwner").toggle(); return false;
  });
  $("#lbMultiJT").click(function () {
    $("#pnlMultiJT").toggle(); return false;
  });

  $("#ucTransactions_txtEffStartDate").datepicker({
    onSelect: function (selected) {
      $("#ucTransactions_txtEffEndDate").datepicker("option", "minDate", selected);
    }
  });
  $("#ucTransactions_txtEffEndDate").datepicker({
    onSelect: function (selected) {
      $("#ucTransactions_txtEffStartDate").datepicker("option", "maxDate", selected);
    }
  });
  $("#ucTransactions_txtPostedStartDate").datepicker({
    onSelect: function (selected) {
      $("#ucTransactions_txtPostedEndDate").datepicker("option", "minDate", selected);
    }
  });
  $("#ucTransactions_txtPostedEndDate").datepicker({
    onSelect: function (selected) {
      $("#ucTransactions_txtPostedStartDate").datepicker("option", "maxDate", selected);
    }
  });


  $("#ucTransactions_btnReset").click(function () {
    $("#ucTransactions_txtEffStartDate").val("");
    $("#ucTransactions_txtEffEndDate").val("");
    $("#ucTransactions_txtPostedStartDate").val("");
    $("#ucTransactions_txtPostedEndDate").val("");
    $("#ucTransactions_ddlTransactionType").val("");

    $(".date").each(function () {
      $.datepicker._clearDate(this);
    });

  });

  $(".date").change(function () {
    validateDate($(this), "")
  });

  $("#ucTransactions_btnSearch").click(function () {
    var error;
    $(".date").each(function () {
      if ($(this).val().length > 0) {
        error = validateDate($(this), error);
      }
    });
    if (error == false) {
      return error;
    }
  });
});
function validateDate(obj, error) {
  var regEx = /^(?:(0[1-9]|1[012])[\/.](0[1-9]|[12][0-9]|3[01])[\/.](19|20)[0-9]{2})$/;
  if (regEx.test(obj.val())) {
    obj.css('color', 'black');
    return error
  } else {
    obj.css('color', 'red');
    return false;
  }
}