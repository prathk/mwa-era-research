﻿Imports System
Imports System.Data
Imports System.Text

'**************  This page is created via a NuGet Package.  Please do not edit. *****************

Partial Class mw
  Inherits System.Web.UI.MasterPage
  Public Property HTTP_BASE As String
  Public Property APP_BASE As String
  Public Property ServerLabel As String = ""

  Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			If IsNothing(Me.lblServer) Then
				Me.lblServer = New Label() With {.Visible = True, .Text = ServerLabel}
			End If
			If ServerLabel.ToUpper() <> "PRODUCTION" Then
				Me.lblServer.Text = ServerLabel
			End If

			Me.hlMyWork.NavigateUrl = APP_BASE & "/prjMyWork/Default.aspx"
			Me.hlReports.NavigateUrl = APP_BASE & "/prjReports/ReportFrontPage.aspx"
			Me.hlAbout.NavigateUrl = HTTP_BASE & "/Inside/Pages/Inside.aspx"
			Me.hlBreakroom.NavigateUrl = HTTP_BASE & "/BreakRoom/Pages/Break-Room.aspx"
			Me.hlDepartments.NavigateUrl = HTTP_BASE & "/TeamWork/Pages/Default.aspx"
			Me.hlDirectory.NavigateUrl = HTTP_BASE & "/Directory/Pages/Home.aspx"
			Me.hlHelpDesk.NavigateUrl = "http://help01/scripts/texcel/servicewise/CLogin.dll?MainPage?16"
			Me.hlHome.NavigateUrl = HTTP_BASE
			Me.hlProductivityHub.NavigateUrl = "http://dept.modern-woodmen.org/Sites/ProductivityHUB/default.aspx"
			Me.hlServices.NavigateUrl = HTTP_BASE & "/EmployeeCenter/Pages/Employee Center.aspx"
			Me.hlTools.NavigateUrl = HTTP_BASE & "/KnowledgeCenter/Pages/Knowledge Center.aspx"
			Me.hlTRC.NavigateUrl = "https://mwarep.modern-woodmen.org"
		Catch ex As Exception
			Throw
		End Try
  End Sub
End Class

