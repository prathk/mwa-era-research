﻿<%@ Page Language="VB"  MasterPageFile="~/MasterPages/mw.master" Inherits="prjSE2DataHub.ErrorPage"    Codebehind="ErrorPage.aspx.vb" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="Main" Runat="Server">
  <style type="text/css">
    .centeredrow {
      display: table
    }

    .centered {
      float: none;
      display: table-cell;
      vertical-align: middle
    }
  </style>

  <div class="container">
    <div class="row centeredrow">
      <div class="col-md-2 centered">
        <i class="fa fa-warning fa-5x"></i>
      </div>
      <div class="col-md-10">
        <div class="row">
          <h1>Application Error!</h1>
        </div>
        <div class="row">&nbsp;</div>
        <div class="row">
          <asp:MultiView ID="mvErrorInfo" runat="server">
            <asp:View ID="vNonProduction" runat="server">
              <div class="row">
                <div class="col-md-12">
                  <asp:Label class="text-primary" ID="lblMessage" runat="server"></asp:Label>
                </div>
              </div>
            </asp:View>
            <asp:View ID="vProduction" runat="server">
              <div class="row">
                <div class="col-md-2">
                   <asp:Label class="text-primary" ID="Label1" runat="server" Text="Error Message"></asp:Label>
                </div>
                 <div class="col-md-10">
                   <asp:Label class="text-primary" ID="lblErrorMessage" runat="server"></asp:Label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                  <asp:Label class="text-primary" ID="Label2" runat="server" Text="Stack Trace"></asp:Label>
                </div>
                 <div class="col-md-10">
                   <asp:Label class="text-primary" ID="lblStackTrace" runat="server"></asp:Label>
                </div>
              </div>
            </asp:View>
          </asp:MultiView>
        </div>
      </div>
    </div>
  </div>
</asp:Content>