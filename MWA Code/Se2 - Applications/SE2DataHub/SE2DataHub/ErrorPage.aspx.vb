﻿
Partial Class ErrorPage
    Inherits System.Web.UI.Page

  Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
    'Local Variable Declarations
    Dim ex As Exception
    If IsNothing(Server.GetLastError.InnerException) Then
      ex = Server.GetLastError()
    Else
      ex = Server.GetLastError.InnerException
    End If

    'Check to see if the application is being executed from within Production and if it is give a generic message
    'If Application("Production") = False Then
    '  lblErrorMessage.Text = ex.Message
    '  lblStackTrace.Text = ex.StackTrace.Replace(vbCrLf, "<br />")
    '  mvErrorInfo.SetActiveView(vProduction)
    'Else
    '  lblMessage.Text = String.Format("An unexpected error has occurred within this application.<br />If you continue to receive this error please contact the IT Helpdesk.<br /><br />{0}<br />{1}", Request.Url.ToString, Now.ToString("MM/dd/yyyy hh:mm:ss tt"))
    '  mvErrorInfo.SetActiveView(vNonProduction)
    'End If

    If ConfigurationManager.AppSettings("ErrorReportingStyle") = "Formatted" Then
      lblErrorMessage.Text = ex.Message
      lblStackTrace.Text = ex.StackTrace.Replace(vbCrLf, "<br />")
      mvErrorInfo.SetActiveView(vProduction)
    Else
      lblMessage.Text = String.Format("An unexpected error has occurred within this application.<br />If you continue to receive this error please contact the IT Helpdesk.<br /><br />{0}<br />{1}", Request.Url.ToString, Now.ToString("MM/dd/yyyy hh:mm:ss tt"))
      mvErrorInfo.SetActiveView(vNonProduction)
    End If

  End Sub


End Class
