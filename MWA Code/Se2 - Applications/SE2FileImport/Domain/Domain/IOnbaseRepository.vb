﻿Imports mwa.CustomerService.CommunicationManagement.SE2FileImport.Backend2.Commands
Imports mwa.OnBase
Public Interface IOnbaseRepository
    Sub Save(doc As Document)

    Sub Save(doc As Document, filePath As String)

    Function GetDocumentFromOnbase(imageFileAndSentToOnbase As ImageConvertedSe2FileIntoOnBase) As Document

    Function CreateDocument(docTypeName As String) As Document

    Function CreateKeywordGroup(keywordGroupName As String) As KeywordGroup

    Function RetrieveOnBaseDocType(docType As String, process As String) As String

    Function RetrieveDocTypeNumber(docType As String) As Integer

    Function RetrieveProcessNumber(docProcess As String) As Integer

End Interface
