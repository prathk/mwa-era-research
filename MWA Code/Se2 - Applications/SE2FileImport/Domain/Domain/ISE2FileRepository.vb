﻿Public Interface ISE2FileRepository

    Function GetFilePath(se2ObjectId As String, fileName As String, rootFileName As String) As String

    Function GetFolderPath(statusFolder As String, Optional se2Folder As String = Nothing) As String

    Sub CopyFile(sourceFolderPath As String, destinationFolderPath As String)

    Sub DeleteFile(folderPath As String)

    Sub DeleteDirectory(directoryPath As String)


End Interface
