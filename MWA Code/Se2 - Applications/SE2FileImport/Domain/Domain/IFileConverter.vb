﻿Public Interface IFileConverter

    Function ConvertFileToTiffAndReturnPathToNewFile(filePath As String, mimeType As String, fileName As String, zipFileName As String) As String

End Interface
