﻿Imports mwa.CustomerService.CommunicationManagement.SE2FileImport.Commands

Public Interface ISE2XmlParser


    Function ParseSe2XmlToListOfImageFileAndSendToOnbase(xmlDocumentPath As String, rootFileName As String) As List(Of ImageFileAndSendToOnbase)


End Interface
