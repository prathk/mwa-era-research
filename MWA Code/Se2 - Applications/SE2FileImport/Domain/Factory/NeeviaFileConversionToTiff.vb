﻿Imports System.Configuration
Imports mwa.ITOps.DeveloperTools.ImageUtilities

Public Class NeeviaFileConversionToTiff : Implements IFileConverter

    Private _neeviaInFolder As String
    Private _neeviaOutFolder As String
    Public Sub New(neeviaInFolder As String, neeviaOutFolder As String)
        _neeviaInFolder = neeviaInFolder
        _neeviaOutFolder = neeviaOutFolder
    End Sub


    Public Function ConvertFileToTiffAndReturnPathToNewFile(filePath As String, mimeType As String, fileName As String, zipFileName As String) As String Implements IFileConverter.ConvertFileToTiffAndReturnPathToNewFile

        If Not (System.IO.Directory.Exists(System.IO.Path.Combine(_neeviaInFolder, zipFileName.Replace(".zip", "")))) Then
            System.IO.Directory.CreateDirectory(System.IO.Path.Combine(_neeviaInFolder, zipFileName.Replace(".zip", "")))
        End If
        If Not (System.IO.Directory.Exists(System.IO.Path.Combine(_neeviaOutFolder, zipFileName.Replace(".zip", "")))) Then
            System.IO.Directory.CreateDirectory(System.IO.Path.Combine(_neeviaOutFolder, zipFileName.Replace(".zip", "")))
        End If

        If Not (DocumentNeedsConverted(filePath, mimeType)) Then
            ''Doesn't need to be converted so move it over to the converted folder
            System.IO.File.Copy(filePath, System.IO.Path.Combine(_neeviaOutFolder, zipFileName.Replace(".zip", ""), fileName), True)
            Return System.IO.Path.Combine(_neeviaOutFolder, zipFileName.Replace(".zip", ""), fileName)
        End If

        ''Copy the file to the neevia input folder for conversion
        System.IO.File.Copy(filePath, System.IO.Path.Combine(_neeviaInFolder, zipFileName.Replace(".zip", ""), fileName), True)
        Dim extension = My.Computer.FileSystem.GetFileInfo(filePath).Extension
        Return System.IO.Path.Combine(_neeviaOutFolder, zipFileName.Replace(".zip", ""), fileName.Replace(extension, ".tif"))

    End Function



    Private Function DocumentNeedsConverted(filePath As String, mimeType As String) As Boolean

        If filePath.ToLower.EndsWith(".txt") OrElse filePath.ToLower.EndsWith(".xls") OrElse filePath.ToLower.EndsWith(".xlsx") OrElse filePath.ToLower.EndsWith(".xlsm") Then
            Return False
        End If

        Select Case mimeType.ToLower
            Case "text/plain", "image/tiff", "image/tif", "application/excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                Return False
            Case Else
                Return True
        End Select

    End Function



End Class
