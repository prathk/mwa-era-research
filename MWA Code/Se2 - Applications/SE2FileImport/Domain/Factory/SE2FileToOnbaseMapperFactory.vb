﻿
Imports mwa.OnBase

Public Class SE2FileToOnbaseMapperFactory



    Private OnbaseRepository As IOnbaseRepository
    Public Sub New(onbaseRepository As IOnbaseRepository)

        Me.OnbaseRepository = onbaseRepository

    End Sub


    Public Function MapSE2FileToNewOnbaseDocument(onbaseDocument As Document, se2FileInformation As SE2ToOnbase) As Document

        Dim docTypeNumber = OnbaseRepository.RetrieveDocTypeNumber(se2FileInformation.DocType)
        Dim processNumber = OnbaseRepository.RetrieveProcessNumber(se2FileInformation.DocProcess)

        If onbaseDocument.Keywords.ContainsKey("Doc Type") Then
            onbaseDocument.Keywords("Doc Type").Add(New Keyword("Doc Type", se2FileInformation.DocType))
            onbaseDocument.Keywords("Doc Type No").Add(New Keyword("Doc Type No", docTypeNumber.ToString))
        End If

        Dim processKwName As String = (From k As KeyValuePair(Of String, List(Of Keyword)) In onbaseDocument.Keywords Where k.Key.ToLower.EndsWith(" process") Select k.Key).FirstOrDefault
        If onbaseDocument.Keywords.ContainsKey("Process") Then
            onbaseDocument.Keywords("Process").Add(New Keyword("Process", se2FileInformation.DocProcess))
            onbaseDocument.Keywords("Process No").Add(New Keyword("Process No", processNumber.ToString))
        ElseIf Not IsNothing(processKwName) Then
            onbaseDocument.Keywords(processKwName).Add(New Keyword(processKwName, se2FileInformation.DocProcess))
        End If

        If DocumentContainsSE2Keywords(onbaseDocument) = False Then
            Throw New Exception(onbaseDocument.DocType & " document type does not contain SE2 specific keywords")
        End If

        onbaseDocument.DocumentDate = se2FileInformation.FileDate

        Return MapSE2KeywordsToOnbaseDocument(onbaseDocument, se2FileInformation)

    End Function

    Public Function MapSE2KeywordsToOnbaseDocument(onbaseDocument As Document, se2FileInformation As SE2ToOnbase) As Document
        'Loop through each keyword within the keyword list and map it to the OnBase document object
        For Each keyword In se2FileInformation.KeywordList
            'If the document contains the keyword directly on the document, add the keyword value
            If onbaseDocument.Keywords.ContainsKey(keyword.Key) Then
                If onbaseDocument.Keywords(keyword.Key).Count > 0 Then
                    'There is already a keyword present on the document so change the value
                    onbaseDocument.Keywords(keyword.Key)(0).Value = keyword.Value
                Else
                    'There is not a keyword present on the document so add one with the value specified
                    onbaseDocument.Keywords(keyword.Key).Add(New Keyword(keyword.Key, keyword.Value))
                End If

                Continue For
            End If

            'Grab any keyword groups containing the keyword name passed in
            Dim keyGroupName As String = (From kwg As KeyValuePair(Of String, List(Of KeywordGroup)) _
                                          In onbaseDocument.KeywordGroups _
                                          Where OnbaseRepository.CreateKeywordGroup(kwg.Key).Keywords.ContainsKey(keyword.Key) Select kwg.Key).FirstOrDefault

            Dim keyGroup As KeywordGroup = Nothing

            'Check to see if any keyword groups contain the keyword we are looking for and if none are returned continue on
            If IsNothing(keyGroupName) Then
                Continue For
            End If

            'If there are no occurrances of the keyword group on the document then create one and add it to the document
            If onbaseDocument.KeywordGroups(keyGroupName).Count = 0 Then
                keyGroup = OnbaseRepository.CreateKeywordGroup(keyGroupName)
                onbaseDocument.KeywordGroups(keyGroupName).Add(keyGroup)
            Else
                keyGroup = onbaseDocument.KeywordGroups(keyGroupName)(0)
            End If

            'Change the value of the keyword on the OnBase document so it matches the one on the keyword list
            keyGroup.Keywords(keyword.Key).Value = keyword.Value
        Next



        Return onbaseDocument

    End Function



    Private Function DocumentContainsSE2Keywords(ByVal doc As Document) As Boolean
        Return doc.Keywords.ContainsKey("Se2 Object ID") AndAlso doc.Keywords.ContainsKey("Se2 File Name")
    End Function


End Class
