﻿Imports System.IO
Imports System.Xml
Imports mwa.CustomerService.CommunicationManagement.SE2FileImport

Public Class Se2XmlFileParser
    Implements ISE2XmlParser

    Private AcquisitionRepository As IAcquisitionsRepository

    Public Sub New(acquistion As IAcquisitionsRepository)
        AcquisitionRepository = acquistion
    End Sub

    Public Function ParseSe2XmlToListOfImageFileAndSendToOnbase(xmlDocumentPath As String, ByVal rootFileName As String) As List(Of Commands.ImageFileAndSendToOnbase) Implements ISE2XmlParser.ParseSe2XmlToListOfImageFileAndSendToOnbase


        Dim lstSE2FileInformationResponse As New List(Of Commands.ImageFileAndSendToOnbase)
        Dim xDoc = XDocument.Load(xmlDocumentPath)
        Dim timestamp = CDate(xDoc.Descendants("ImageTransfer").Attributes("TimeStamp").FirstOrDefault.Value)
        Dim documentList = xDoc.Descendants("Document")

        Dim acquisitions = AcquisitionRepository.DocumentMapping_Incoming("ERA")

        For Each documentTag In documentList

            Dim se2DocProcess As String = CStr(documentTag.Descendants("Keyword").Where(Function(r) r.Attributes("Name").FirstOrDefault.Value = "Process_Type").Select(Function(v) v.Attributes("Value").FirstOrDefault).FirstOrDefault)
            Dim se2DocType As String = CStr(documentTag.Descendants("Keyword").Where(Function(r) r.Attributes("Name").FirstOrDefault.Value = "Transaction_Type").Select(Function(v) v.Attributes("Value").FirstOrDefault).FirstOrDefault)
            Dim CertNumber = CStr(documentTag.Descendants("Keyword").Where(Function(r) r.Attributes("Name").FirstOrDefault.Value = "Cert_No").Select(Function(v) v.Attributes("Value").FirstOrDefault).FirstOrDefault)
            'Call Acquisition Table to get correct doc type and process

            If (CertNumber = 8544390) Then
                Dim r = "debug"

                For Each item In acquisitions
                    If (item.Se2Process.ToUpper = se2DocProcess.ToUpper) Then

                        If (String.IsNullOrEmpty(item.Se2DocType)) Then
                            Dim r2 = item.Se2DocType
                        End If


                    End If
                Next
            End If

            Dim acquisition = (From acq In acquisitions
                                          Where acq.Se2DocType.ToUpper = se2DocType.ToUpper AndAlso
                                                acq.Se2Process.ToUpper = se2DocProcess.ToUpper).FirstOrDefault()

            If acquisition Is Nothing Then
                Throw New System.Exception(String.Format("Failed to parse XML file because document mapping doesn’t exist certno{0},se2Doctype{1},se2docprocess{2}", CertNumber, se2DocType, se2DocProcess))
            End If

            Dim DocProcess = acquisition.MwaProcessType
            Dim DocType = acquisition.MwaDocType


            Dim EmployerId = CStr(documentTag.Descendants("Keyword").Where(Function(r) r.Attributes("Name").FirstOrDefault.Value = "Employer_ID").Select(Function(v) v.Attributes("Value").FirstOrDefault).FirstOrDefault)
            Dim ScanDate = CStr(documentTag.Descendants("Keyword").Where(Function(r) r.Attributes("Name").FirstOrDefault.Value = "Scan_Date").Select(Function(v) v.Attributes("Value").FirstOrDefault).FirstOrDefault)
            Dim SE2DocId = CStr(documentTag.Descendants("Keyword").Where(Function(r) r.Attributes("Name").FirstOrDefault.Value = "Se2_Doc_ID").Select(Function(v) v.Attributes("Value").FirstOrDefault).FirstOrDefault)
            Dim SE2ObjectId = CStr(documentTag.Descendants("Keyword").Where(Function(r) r.Attributes("Name").FirstOrDefault.Value = "Se2_Object_ID").Select(Function(v) v.Attributes("Value").FirstOrDefault).FirstOrDefault)
            Dim TPAPolicyNo = CStr(documentTag.Descendants("Keyword").Where(Function(r) r.Attributes("Name").FirstOrDefault.Value = "TPA_Policy_No").Select(Function(v) v.Attributes("Value").FirstOrDefault).FirstOrDefault)


            For Each File In documentTag.Descendants("File")


                Dim command = New Commands.ImageFileAndSendToOnbase With
                              {.CertNumber = CertNumber, _
                               .DocProcess = DocProcess, _
                               .DocType = DocType, _
                               .EmployerId = EmployerId, _
                               .FileName = File.Attributes("FileName").FirstOrDefault.Value, _
                               .FileDate = timestamp, _
                               .MimeType = File.Attributes("MimeType").FirstOrDefault.Value, _
                               .OnbaseDocId = If(File.Attributes("OB_Doc_ID").FirstOrDefault.Value <> "", CInt(File.Attributes("OB_Doc_ID").FirstOrDefault.Value), 0), _
                               .ZipFileName = rootFileName, _
                               .ScanDate = ScanDate, _
                               .SE2DocId = SE2DocId, _
                               .SE2ObjectId = SE2ObjectId, _
                               .TPAPolicyNo = TPAPolicyNo}



                lstSE2FileInformationResponse.Add(command)
            Next
        Next


        Return lstSE2FileInformationResponse


    End Function

End Class
