﻿
Imports System.IO
Imports System.IO.Compression
Public Class ZipFolderFactory
    Implements IZipService
    Private Const BUFFER_SIZE As Integer = 2048

    Public Sub UnZip(sourceFilePath As String, destFolderPath As String) Implements IZipService.UnZip


        ZipFile.ExtractToDirectory(sourceFilePath, destFolderPath, Nothing)

    End Sub

End Class
