﻿Imports mwa

Public Class SE2ToOnbaseKeywordComparer


    Private OnbaseRepository As IOnbaseRepository

    Public Sub New(ByVal onbaseRepository As IOnbaseRepository)

        Me.OnbaseRepository = onbaseRepository

    End Sub

    Public Function DocumentHasChanged(onbaseDocument As OnBase.Document, se2FileInformation As SE2ToOnbase) As Boolean

        'Loop through each keyword within the se2 keyword list and compare them with keywords on the OnBase document
        For Each keyword In se2FileInformation.KeywordList
            'Check to see if the keyword is a standalone keyword that is directly on the document
            If (onbaseDocument.Keywords.ContainsKey(keyword.Key)) Then
                'Check to see if the keyword values are blank and if they are, continue on
                If KeywordsAreBlank(keyword, onbaseDocument) Then
                    Continue For
                End If

                'Check to see if there is an instance of the keyword containing the value passed in
                If IsNothing(onbaseDocument.FindKeyword(keyword.Key, keyword.Value)) Then
                    Return False
                End If

                Continue For
            End If

            'Check to see if the keyword on the document has an instance of the keyword within a multi instance keyword group
            Dim keyGroup = From kwg As KeyValuePair(Of String, List(Of OnBase.KeywordGroup)) In onbaseDocument.KeywordGroups Where OnbaseRepository.CreateKeywordGroup(kwg.Key).Keywords.ContainsKey(keyword.Key) Select kwg
            If keyGroup.Any Then
                'Check to see if there is an instance of the keyword within the group containing the value passed in
                If IsNothing(onbaseDocument.FindKeywordGroup(keyGroup.First.Key, keyword.Key, keyword.Value)) Then
                    Return False
                End If
            End If
        Next

        Return True

    End Function



    Private Function KeywordsAreBlank(ByVal keyword As KeyValuePair(Of String, String), ByVal onbaseDocument As OnBase.Document) As Boolean

        Return String.IsNullOrWhiteSpace(keyword.Value) = True AndAlso onbaseDocument.Keywords(keyword.Key).Count = 0

    End Function

End Class
