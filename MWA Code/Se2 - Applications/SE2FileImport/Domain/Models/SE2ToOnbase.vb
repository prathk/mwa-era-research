﻿Imports mwa.CustomerService.CommunicationManagement.SE2FileImport
Imports mwa.CustomerService.CommunicationManagement.SE2FileImport.Backend2.Commands

Public Class SE2ToOnbase

    Public Sub New(message As ImageConvertedSe2FileIntoOnBase)

        Me.DocType = message.DocType
        Me.DocProcess = message.DocProcess
        Me.FileName = message.FileName
        Me.MimeType = message.MimeType
        Me.OnbaseDocId = message.OnbaseDocId
        Me.ZipFileName = message.ZipFileName
        Me.FileDate = message.FileDate

        Me.KeywordList = New Dictionary(Of String, String)

        Me.KeywordList.Add("Cert No", message.CertNumber)
        Me.KeywordList.Add("Se2 Object ID", message.SE2ObjectId)
        Me.KeywordList.Add("Se2 Doc ID", message.SE2DocId)
        Me.KeywordList.Add("TPA Policy No", message.TPAPolicyNo)
        Me.KeywordList.Add("SS NO", message.SSN)
        Me.KeywordList.Add("Se2 File Name", message.FileName)
        ' Me.KeywordList.Add("Entry Date", message.ScanDate)

    End Sub

    Public Property FileDate As DateTime
    Public Property DocType As String
    Public Property DocProcess As String

    Public Property KeywordList As Dictionary(Of String, String)
    Public Property FileName As String
    Public Property OnbaseDocId As Integer
    Public Property MimeType As String
    Public Property ZipFileName As String

End Class



