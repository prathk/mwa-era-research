﻿Imports mwa.OnBase
Imports mwa.DataAccess.Sql
Imports System.Data.Common
Imports mwa.DataAccess.Common
Imports System.Configuration
Imports mwa.CustomerService.CommunicationManagement.SE2FileImport.Backend2.Commands

Public Class OnbaseRepository
    Implements IOnbaseRepository


    Private _DocumentCache As New Dictionary(Of String, Document)
    Private _DocumentRepository As DocumentRepository
    Private _ImagingWS As ImagingWS.ImagingWS
    Private _ConnectionString As IConnectionStringFactory
    Private _ImagingWSUrl As String
    Private _newDocumentWorkflowName As String
    Private _ImagingUser As String = ConfigurationManager.AppSettings("ImagingUser")
    Private _se2IndexingSql As String = "Select From Document Where Se2 Object ID = '{0}' And Se2 File Name = '{1}'"

    Public Sub New(imagingWSUrl As String, newDocumentWorkflowName As String, aDocumentRepository As DocumentRepository, connectionString As IConnectionStringFactory)

        Me._DocumentRepository = aDocumentRepository
        Me._ImagingWSUrl = imagingWSUrl
        Me._ImagingWS = New ImagingWS.ImagingWS
        Me._newDocumentWorkflowName = newDocumentWorkflowName
        Me._ConnectionString = connectionString

    End Sub

    Public Function GetDocumentFromOnbase(imageFileAndSentToOnbase As ImageConvertedSe2FileIntoOnBase) As Document Implements IOnbaseRepository.GetDocumentFromOnbase

        Dim doc As Document = Nothing

        If (imageFileAndSentToOnbase.OnbaseDocId > 0) Then
            doc = FindOnbaseDocumentByDocumentId(imageFileAndSentToOnbase.OnbaseDocId)
        End If

        If (doc IsNot Nothing) Then
            Return doc
        End If

        doc = FindOnbaseDocumentByObjectIdAndFileName(imageFileAndSentToOnbase.SE2ObjectId, imageFileAndSentToOnbase.FileName)

        If (doc IsNot Nothing) Then
            Return doc
        End If

        Return Nothing

    End Function


    Public Function CreateDocument(docTypeName As String) As Document Implements IOnbaseRepository.CreateDocument

        DocumentFactory.UserName = _ImagingUser
        Return DocumentFactory.Create(docTypeName)

    End Function


    Public Sub Save(doc As Document) Implements IOnbaseRepository.Save

        _DocumentRepository.Save(doc)

    End Sub

    Public Sub Save(doc As Document, filePath As String) Implements IOnbaseRepository.Save

        Dim fileType As OnBase.FileTypes
        Select Case My.Computer.FileSystem.GetFileInfo(filePath).Extension.ToLower
            Case ".txt"
                fileType = FileTypes.Text
            Case ".tif", ".tiff"
                fileType = FileTypes.Tif
            Case ".xls", ".xlsx", ".xlsm"
                fileType = FileTypes.Excel
        End Select

        _DocumentRepository.Save(doc, My.Computer.FileSystem.ReadAllBytes(filePath), fileType)
        AddDocumentToWorkflow(CInt(doc.Id))

    End Sub


    Public Function CreateKeywordGroup(keywordGroupName As String) As KeywordGroup Implements IOnbaseRepository.CreateKeywordGroup

        KeywordGroupFactory.UserName = _ImagingUser
        Return KeywordGroupFactory.Create(keywordGroupName)

    End Function

    Public Function RetrieveOnBaseDocType(docType As String, process As String) As String Implements IOnbaseRepository.RetrieveOnBaseDocType

        Dim docTypeNumber = RetrieveDocTypeNumber(docType)
        Dim processNumber = RetrieveProcessNumber(process)

        Using _ImagingWS
            _ImagingWS.Url = _ImagingWSUrl
            _ImagingWS.Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim correctedDocType = _ImagingWS.RetrieveOnBaseDocTypeFromMwaDocTypeAndProcess(_ImagingUser, docTypeNumber, processNumber)
            If (correctedDocType <> "") Then
                Return correctedDocType
            End If
        End Using
        Return "CRMasterFile"

    End Function



    Public Function RetrieveDocTypeNumber(docType As String) As Integer Implements IOnbaseRepository.RetrieveDocTypeNumber

        Dim dt As New DataTable
        Using dac As New SqlDataAccess(Me._ConnectionString)
            Dim params As New List(Of DbParameter)
            dt = dac.ExecuteDataTable("Imaging", "mwawr", "SelectDocType", params)
        End Using
        Return CInt((From row As DataRow In dt.AsEnumerable Where CStr(row("DocType_Name")) = docType Select row("DocType_Number")).FirstOrDefault)

    End Function

    Public Function RetrieveProcessNumber(docProcess As String) As Integer Implements IOnbaseRepository.RetrieveProcessNumber

        Dim dt As New DataTable
        Using dac As New SqlDataAccess(Me._ConnectionString)
            Dim params As New List(Of DbParameter)
            params.Add(dac.ParameterFactory.Create("@stDocProcessName", docProcess, DbType.String, ParameterDirection.Input))
            dt = dac.ExecuteDataTable("Imaging", "mwawr", "SelectDocProcess", params)
        End Using
        If (dt.Rows.Count > 0) Then
            Return CInt(dt.Rows(0).Item("DocProc_Number"))
        End If
        Return -1

    End Function



#Region "Private Methods"
    Private Function FindOnbaseDocumentByDocumentId(id As Integer) As Document

        Dim doc = _DocumentRepository.Find(id)
        Return doc

    End Function

    Private Function FindOnbaseDocumentByObjectIdAndFileName(se2ObjectId As String, se2FileName As String) As Document

        Dim searchString = String.Format(_se2IndexingSql, se2ObjectId, se2FileName)
        Dim doc = _DocumentRepository.FindAll(searchString)
        If (doc IsNot Nothing) Then
            If (doc.Count > 0) Then
                Return doc(0)
            End If
        End If
        Return Nothing

    End Function

    Private Sub AddDocumentToWorkflow(ByVal docId As Integer)

        Using _ImagingWS
            _ImagingWS.Credentials = System.Net.CredentialCache.DefaultCredentials
            _ImagingWS.Url = _ImagingWSUrl
            _ImagingWS.AddDocumentToWorkflow(_ImagingUser, docId, _newDocumentWorkflowName)
        End Using

    End Sub


#End Region





End Class
