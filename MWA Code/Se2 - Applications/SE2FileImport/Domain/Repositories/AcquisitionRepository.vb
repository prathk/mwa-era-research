﻿Imports mwa.DataAccess.Sql
Imports mwa.DataAccess.Common
Imports System.Data.Common

Public Class AcquisitionRepository
    Implements IAcquisitionsRepository

    Private _ConnectionString As IConnectionStringFactory

    Public Sub New(connectionString As IConnectionStringFactory)
        _ConnectionString = connectionString

    End Sub

    Public Function DocumentMapping_Incoming(companyCode As String) As List(Of Acquisition) Implements IAcquisitionsRepository.DocumentMapping_Incoming
        Dim dt As New DataTable
        Using dac As New SqlDataAccess(Me._ConnectionString)
            Dim params As New List(Of DbParameter)
            'params.Add(dac.ParameterFactory.Create("@NonMwaProcess", nonMwaDocProcess, DbType.String, ParameterDirection.Input))
            'params.Add(dac.ParameterFactory.Create("@NonMwaDocType", nonMwaDocType, DbType.String, ParameterDirection.Input))
            params.Add(dac.ParameterFactory.Create("@CompanyCode", companyCode, DbType.String, ParameterDirection.Input))
            dt = dac.ExecuteDataTable("Acquisitions", "dbo", "DocumentMapping_Incoming_sel", params)
        End Using
        If (dt.Rows.Count > 0) Then
            Return (From row As DataRow In dt.AsEnumerable Select New Acquisition With
                                                                  {.MwaDocType = If(IsDBNull(row("MwaDocType")), "", row("MwaDocType")),
                                                                   .MwaProcessType = If(IsDBNull(row("MwaProcess")), "", row("MwaProcess")),
                                                                   .Se2DocType = If(IsDBNull(row("NonMwaDocType")), "", row("NonMwaDocType")),
                                                                   .Se2Process = If(IsDBNull(row("NonMwaProcess")), "", row("NonMwaProcess"))}).ToList()
        End If
        Return Nothing
    End Function

End Class
