﻿Imports System.IO
Imports System.Configuration

Public Class SE2FileRepository : Implements ISE2FileRepository


    Public Function GetFilePath(se2ObjectId As String, fileName As String, rootFileName As String) As String Implements ISE2FileRepository.GetFilePath

        Return ConfigurationManager.AppSettings("InProcessFolderPath").ToString & "\" & rootFileName & "\" & se2ObjectId & "\" & fileName

    End Function

    Public Function GetFolderPath(destinationFolder As String, Optional se2Folder As String = Nothing) As String Implements ISE2FileRepository.GetFolderPath
        Return ConfigurationManager.AppSettings("FolderPath").ToString & "\" & destinationFolder & If(se2Folder Is Nothing, "", "\" & se2Folder)
    End Function

    Public Sub CopyFile(sourceFolderPath As String, destinationFolderPath As String) Implements ISE2FileRepository.CopyFile
        If (My.Computer.FileSystem.FileExists(destinationFolderPath)) Then
            My.Computer.FileSystem.DeleteFile(destinationFolderPath)
        End If
        My.Computer.FileSystem.CopyFile(sourceFolderPath, destinationFolderPath)
        ' My.Computer.FileSystem.CopyDirectory(sourceFolderPath, destinationFolderPath)
    End Sub

    Public Sub DeleteFile(folderPath As String) Implements ISE2FileRepository.DeleteFile
        My.Computer.FileSystem.DeleteFile(folderPath)
    End Sub


    Public Sub DeleteDirectory1(directoryPath As String) Implements ISE2FileRepository.DeleteDirectory

        Dim dInfo As New DirectoryInfo(directoryPath)
        dInfo.Delete(True)
    End Sub
End Class
