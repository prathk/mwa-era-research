﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("mwa.CustomerService.CommunicationManagement.SE2FileImport.Events")>
<Assembly: AssemblyDescription("Domain events")>
<Assembly: AssemblyCompany("Modern Woodmen of America")>
<Assembly: AssemblyProduct("mwa.CustomerService.CommunicationManagement.SE2FileImport.Events")>
<Assembly: AssemblyCopyright("Copyright © Modern Woodmen of America 2015")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("9a01445e-5de2-4aa8-99b3-73847209a121")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
