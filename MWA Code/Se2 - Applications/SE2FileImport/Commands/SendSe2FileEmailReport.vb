﻿Imports System.Collections.Generic

Public Class SendSe2FileEmailReport

    Public Property ZipFileName As String
    Public Property FilesCompletedCount As Integer
    Public Property Errors As Dictionary(Of String, String)
    Public Property DateCompleted As DateTime

End Class
