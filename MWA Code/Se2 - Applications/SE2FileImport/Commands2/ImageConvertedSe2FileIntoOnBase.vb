﻿Public Class ImageConvertedSe2FileIntoOnBase
    Public Property DocType As String
    Public Property DocProcess As String
    Public Property CertNumber As String
    Public Property SE2ObjectId As String
    Public Property SE2DocId As String
    Public Property TPAPolicyNo As String
    Public Property SSN As String
    Public Property ScanDate As String
    Public Property EmployerId As String
    Public Property FileName As String
    Public Property OnbaseDocId As Integer
    Public Property MimeType As String
    Public Property ZipFileName As String
    Public Property FileDate As DateTime
    Public Property FilePath As String
    Public Property FileConversionAttempts As Integer = 0
End Class
