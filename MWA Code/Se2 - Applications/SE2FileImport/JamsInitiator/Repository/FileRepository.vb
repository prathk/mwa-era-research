﻿Imports System.IO

Public Class FileRepository : Implements IFileRepository

    Public Sub DeleteDirectory(path As String) Implements IFileRepository.DeleteDirectory
        My.Computer.FileSystem.DeleteDirectory(path, FileIO.DeleteDirectoryOption.DeleteAllContents)
    End Sub
End Class
