Imports NServiceBus
Imports System.Reflection

Public Class BusConfig

    Public Shared Function GetBus() As IEndpointInstance

        Dim busConfiguration = New EndpointConfiguration(Reflection.Assembly.GetExecutingAssembly.GetName.Name)
        busConfiguration.UsePersistence(Of InMemoryPersistence)()

        busConfiguration.Conventions.DefiningCommandsAs(Function(r) r.Namespace <> Nothing AndAlso r.Namespace.EndsWith("Commands"))
        'busConfiguration.Conventions.DefiningEventsAs(Function(r) r.Namespace <> Nothing AndAlso r.Namespace.EndsWith("Events"))

        busConfiguration.SendOnly()
        Return Endpoint.Start(busConfiguration).GetAwaiter.GetResult

    End Function

End Class