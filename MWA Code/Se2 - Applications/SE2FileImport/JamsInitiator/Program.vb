﻿Imports System.IO
Imports mwa.MemberServices.ERA.SE2FileImport.Commands
Imports NServiceBus

Module Program


    Sub Main()
        Dim fileRepository As IFileRepository = New FileRepository
        Dim bus = BusConfig.GetBus()

        Dim filePath As String = System.Configuration.ConfigurationManager.AppSettings("PendingDirectoryPath")

        Dim dInfo As New DirectoryInfo(filePath)
        Dim files = dInfo.GetFiles()

        For Each fi In files

            bus.Send(New ProcessZippedFileReceivedFromSE2() With {.ZipFileName = fi.Name, .Timestamp = DateTime.Now})

        Next

    End Sub

End Module