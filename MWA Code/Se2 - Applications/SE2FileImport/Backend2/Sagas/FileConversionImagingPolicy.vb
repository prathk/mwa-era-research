﻿Imports NServiceBus
Imports NServiceBus.Persistence.Sql
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Events
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Commands
Imports System.Configuration

Public Class FileConversionImagingPolicy : Inherits SqlSaga(Of FileConversionImagingPolicyData)
    Implements IAmStartedByMessages(Of ImageFileAndSendToOnbase),
        IHandleMessages(Of Se2FileSentToNeeviaFolderForConversion),
        IHandleTimeouts(Of NeeviaFileConversionTimeout)

    Protected Overrides ReadOnly Property CorrelationPropertyName As String
        Get
            Return NameOf(FileConversionImagingPolicyData.FileNameZipFileName)
        End Get
    End Property

    Protected Overrides Sub ConfigureMapping(mapper As IMessagePropertyMapper)
        mapper.ConfigureMapping(Of ImageFileAndSendToOnbase)(Function(r) r.FileName & r.ZipFileName)
        mapper.ConfigureMapping(Of Se2FileSentToNeeviaFolderForConversion)(Function(r) r.FileName & r.ZipFileName)
        mapper.ConfigureMapping(Of NeeviaFileConversionTimeout)(Function(r) r.FileName & r.ZipFileName)
    End Sub

    Public Async Function Handle(message As ImageFileAndSendToOnbase, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of ImageFileAndSendToOnbase).Handle

        ''Load up the Saga Data
        Me.Data = New FileConversionImagingPolicyData With {.CertNumber = message.CertNumber, .DocProcess = message.DocProcess, .DocType = message.DocType, .EmployerId = message.EmployerId, .FileDate = message.FileDate, .FileName = message.FileName,
            .MimeType = message.MimeType, .OnbaseDocId = message.OnbaseDocId, .ScanDate = message.ScanDate, .SE2DocId = message.SE2DocId, .SE2ObjectId = message.SE2ObjectId, .SSN = message.SSN, .TPAPolicyNo = message.TPAPolicyNo, .ZipFileName = message.ZipFileName, .OriginalMessageId = Me.Data.OriginalMessageId, .Id = Me.Data.Id, .Originator = Me.Data.Originator, .FileConversionAttempts = 0}

        ''validate the CertNumber is valid before continuing
        Try
            Dim cert = CInt(message.CertNumber)
        Catch ex As Exception
            Me.MarkAsComplete()
            context.Publish(Of Se2FileConversionToTiffFailed)(Function(r)
                                                                  r.ErrorDateTime = DateTime.Now
                                                                  r.FileDate = message.FileDate
                                                                  r.FileName = message.FileName
                                                                  r.ZipFileName = message.ZipFileName
                                                                  r.Ex = "Certificate Number was not an integer"
                                                                  Return r
                                                              End Function).GetAwaiter.GetResult()

            Exit Function
        End Try


        ''Send file to be converted
        Await context.Send(New ConvertSe2FileToTiff With {.FileName = Me.Data.FileName, .MimeType = Me.Data.MimeType, .Se2ObjectId = Me.Data.SE2ObjectId, .ZipFileName = Me.Data.ZipFileName}).ConfigureAwait(False)


    End Function

    Public Async Function Handle(message As Se2FileSentToNeeviaFolderForConversion, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of Se2FileSentToNeeviaFolderForConversion).Handle

        Me.Data.FileOutputPath = message.OutputFileLocation
        If (DoesFileExistsAfterNeeviaConversion(message.OutputFileLocation)) Then
            ''send command Image the file
            Await context.Send(New ImageConvertedSe2FileIntoOnBase With {.CertNumber = Me.Data.CertNumber, .DocProcess = Me.Data.DocProcess, .DocType = Me.Data.DocType, .EmployerId = Me.Data.EmployerId, .FileDate = Me.Data.FileDate, .FileName = Me.Data.FileName,
                   .FilePath = Me.Data.FileOutputPath, .MimeType = Me.Data.MimeType, .OnbaseDocId = Me.Data.OnbaseDocId, .ScanDate = Me.Data.ScanDate, .SE2DocId = Me.Data.SE2DocId, .SE2ObjectId = Me.Data.SE2ObjectId, .SSN = Me.Data.SSN,
                   .TPAPolicyNo = Me.Data.TPAPolicyNo, .ZipFileName = Me.Data.ZipFileName}).ConfigureAwait(False)
            Me.MarkAsComplete()
        Else
            Await RequestTimeout(Of NeeviaFileConversionTimeout)(context, TimeSpan.FromMinutes(5), New NeeviaFileConversionTimeout With {.FileName = Me.Data.FileName, .TimeoutAttempts = 1, .ZipFileName = Me.Data.ZipFileName}).ConfigureAwait(False)
        End If



    End Function

    Public Async Function Timeout(state As NeeviaFileConversionTimeout, context As IMessageHandlerContext) As Task Implements IHandleTimeouts(Of NeeviaFileConversionTimeout).Timeout

        If (DoesFileExistsAfterNeeviaConversion(Me.Data.FileOutputPath)) Then
            ''Send command to image file
            Await context.Send(New ImageConvertedSe2FileIntoOnBase With {.CertNumber = Me.Data.CertNumber, .DocProcess = Me.Data.DocProcess, .DocType = Me.Data.DocType, .EmployerId = Me.Data.EmployerId, .FileDate = Me.Data.FileDate, .FileName = Me.Data.FileName,
                               .FilePath = Me.Data.FileOutputPath, .MimeType = Me.Data.MimeType, .OnbaseDocId = Me.Data.OnbaseDocId, .ScanDate = Me.Data.ScanDate, .SE2DocId = Me.Data.SE2DocId, .SE2ObjectId = Me.Data.SE2ObjectId, .SSN = Me.Data.SSN,
                               .TPAPolicyNo = Me.Data.TPAPolicyNo, .ZipFileName = Me.Data.ZipFileName}).ConfigureAwait(False)
            Me.MarkAsComplete()
        ElseIf (DoesFileExistsInNeeviaConversionErrorFolder(Me.Data.FileOutputPath)) Then
            ''Check the number of times the file has been attempted to be converted.  
            ''When neevia has a large number of files (msg usual culprit) it tries to process it sometimes errors out when it shouldn't, retrying it fixes the issue usually
            If (Me.Data.FileConversionAttempts > 2) Then
                Me.MarkAsComplete()
                Await context.Publish(Of Se2FileConversionToTiffFailed)(Function(r)
                                                                            r.ErrorDateTime = DateTime.Now
                                                                            r.FileDate = Me.Data.FileDate
                                                                            r.FileName = Me.Data.FileName
                                                                            r.ZipFileName = Me.Data.ZipFileName
                                                                            r.Ex = "Neevia failed to convert the document"
                                                                            Return r
                                                                        End Function).ConfigureAwait(False)

                ''This should delete from the ErrorFolder and InFolder?

                Await context.Send(New DeleteConvertedFileFromNeeviaFolder With {.FileName = Me.Data.FileName, .FilePath = Me.Data.FileOutputPath}).ConfigureAwait(False)
            Else
                Me.Data.FileConversionAttempts += 1
                Await context.Send(New CleanupFoldersForFileConversionRetry With {.FileName = Me.Data.FileName, .MimeType = Me.Data.MimeType, .Se2ObjectId = Me.Data.SE2ObjectId, .ZipFileName = Me.Data.ZipFileName}).ConfigureAwait(False)
            End If

        Else

            If (state.TimeoutAttempts > 10) Then
                ''Check the number of times the file has been attempted to be converted.  
                ''When neevia has a large number of files (msg usual culprit) it tries to process it sometimes errors out when it shouldn't, retrying it fixes the issue usually
                If (Me.Data.FileConversionAttempts > 2) Then
                    Me.MarkAsComplete()
                    Await context.Publish(Of Se2FileConversionToTiffFailed)(Function(r)
                                                                                r.ErrorDateTime = DateTime.Now
                                                                                r.FileDate = Me.Data.FileDate
                                                                                r.FileName = Me.Data.FileName
                                                                                r.ZipFileName = Me.Data.ZipFileName
                                                                                r.Ex = "File failed to conversion after at least 7 minutes"
                                                                                Return r
                                                                            End Function).ConfigureAwait(False)
                    Await context.Send(New DeleteConvertedFileFromNeeviaFolder With {.FileName = Me.Data.FileName, .FilePath = IO.Path.Combine(ConfigurationManager.AppSettings("NeeviaInFolder"), Me.Data.ZipFileName.Replace(".zip", ""), Me.Data.FileName)}).ConfigureAwait(False)
                Else
                    Me.Data.FileConversionAttempts += 1
                    Await context.Send(New CleanupFoldersForFileConversionRetry With {.FileName = Me.Data.FileName, .MimeType = Me.Data.MimeType, .Se2ObjectId = Me.Data.SE2ObjectId, .ZipFileName = Me.Data.ZipFileName}).ConfigureAwait(False)
                End If

            Else
                ''Increase the timeout counter and then reset the timeout
                state.TimeoutAttempts += 1
                Await RequestTimeout(Of NeeviaFileConversionTimeout)(context, TimeSpan.FromMinutes(5), state).ConfigureAwait(False)
            End If


        End If


    End Function


    Private Function DoesFileExistsAfterNeeviaConversion(fileOutputPath As String) As Boolean

        ''This will handle files that were already .tiff / txt / xls / xlsx
        If (System.IO.File.Exists(fileOutputPath)) Then
            Return True
        End If


        ''Need to figure out the whole .msg -> New folder part
        Dim outputDirectory As String = ConfigurationManager.AppSettings("NeeviaOutFolder")

        If (IO.File.Exists(IO.Path.Combine(outputDirectory, Me.Data.FileName & ".tif"))) Then
            Me.Data.FileOutputPath = IO.File.Exists(IO.Path.Combine(outputDirectory, Me.Data.ZipFileName.Replace(".zip", ""), Me.Data.FileName & ".tif"))
            Return True
        End If
        If (IO.File.Exists(IO.Path.Combine(outputDirectory, Me.Data.ZipFileName.Replace(".zip", ""), Me.Data.FileName & ".TIFF"))) Then
            Me.Data.FileOutputPath = IO.File.Exists(IO.Path.Combine(outputDirectory, Me.Data.ZipFileName.Replace(".zip", ""), Me.Data.FileName & ".TIFF"))
            Return True
        End If
        Return False

    End Function

    Private Function DoesFileExistsInNeeviaConversionErrorFolder(fileOutputPath As String) As Boolean



        Dim outputDirectory As String = ConfigurationManager.AppSettings("NeeviaErrorFolder")
        If (System.IO.File.Exists(IO.Path.Combine(outputDirectory, Me.Data.ZipFileName.Replace(".zip", ""), Me.Data.FileName))) Then
            Return True
        End If

        If (Me.Data.FileName.EndsWith(".msg")) Then
            If (System.IO.Directory.Exists(IO.Path.Combine(outputDirectory, Me.Data.ZipFileName.Replace(".zip", ""), Me.Data.FileName & ".files"))) Then
                Return True
            End If
        End If

        If (IO.File.Exists(IO.Path.Combine(outputDirectory, Me.Data.FileName & ".tif"))) Then
            Me.Data.FileOutputPath = IO.File.Exists(IO.Path.Combine(outputDirectory, Me.Data.ZipFileName.Replace(".zip", ""), Me.Data.FileName & ".tif"))
            Return True
        End If
        If (IO.File.Exists(IO.Path.Combine(outputDirectory, Me.Data.FileName & ".TIFF"))) Then
            Me.Data.FileOutputPath = IO.File.Exists(IO.Path.Combine(outputDirectory, Me.Data.ZipFileName.Replace(".zip", ""), Me.Data.FileName & ".TIFF"))
            Return True
        End If
        Return False

    End Function


End Class



Public Class FileConversionImagingPolicyData : Inherits ContainSagaData

    Public Property DocType As String
    Public Property DocProcess As String
    Public Property CertNumber As String
    Public Property SE2ObjectId As String
    Public Property SE2DocId As String
    Public Property TPAPolicyNo As String
    Public Property SSN As String
    Public Property ScanDate As String
    Public Property EmployerId As String
    Public Property FileName As String
    Public Property OnbaseDocId As Integer
    Public Property MimeType As String
    Public Property ZipFileName As String
    Public Property FileDate As DateTime
    Public Property FileOutputPath As String
    Public Property FileConversionAttempts As Integer = 0
    Public Property FileNameZipFileName As String

End Class
