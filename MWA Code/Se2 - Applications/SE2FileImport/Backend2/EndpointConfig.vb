﻿
Imports NServiceBus
Imports mwa.OnBase
Imports mwa.DataAccess.Common
Imports mwa.ITOps.SoftwareDevelopment.NServiceBusExtensions
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain
Imports System.Configuration

Public Class EndpointConfig : Implements IConfigureThisEndpoint


    Public Sub New()

        LoggingConfiguration.ConfigureLogDirectory(System.Configuration.ConfigurationManager.AppSettings("LogFilePathFolder"))

    End Sub


    Public Sub Customize(configuration As EndpointConfiguration) Implements IConfigureThisEndpoint.Customize


        configuration.SendHeartbeatTo(System.Configuration.ConfigurationManager.AppSettings("ServiceControl/Queue"), TimeSpan.FromSeconds(30))
        SqlPersistence.ConfigureSqlPersistence(configuration, System.Configuration.ConfigurationManager.ConnectionStrings("SqlPersistenceConnectionString").ConnectionString)
        ConfigureUnobtrusiveMessageConventions(configuration)



        configuration.RegisterComponents(Sub(r) r.ConfigureComponent(Of ISE2FileRepository)(
                                               Function()
                                                   Return New SE2FileRepository()
                                               End Function, DependencyLifecycle.InstancePerCall)
                                        )
        configuration.RegisterComponents(Sub(r) r.ConfigureComponent(Of IFileConverter)(
                                              Function()
                                                  Return New NeeviaFileConversionToTiff(ConfigurationManager.AppSettings("NeeviaInFolder"), ConfigurationManager.AppSettings("NeeviaOutFolder"))
                                              End Function, DependencyLifecycle.InstancePerCall)
                                       )


        configuration.RegisterComponents(Sub(r) r.ConfigureComponent(Of IOnbaseRepository)(
                                              Function()
                                                  Return New OnbaseRepository(
                                                        ConfigurationManager.AppSettings("ImagingWSUrl"),
                                                        ConfigurationManager.AppSettings("NewDocumentWorkflowName"),
                                                        New DocumentRepository(ConfigurationManager.AppSettings("ImagingUser")),
                                                        New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString)
                                                        )
                                              End Function, DependencyLifecycle.InstancePerCall)
                                        )

        configuration.RegisterComponents(Sub(r) r.ConfigureComponent(Of IAcquisitionsRepository)(
                                      Function()
                                          Return New AcquisitionRepository(New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString))
                                      End Function, DependencyLifecycle.InstancePerCall)
                               )

        configuration.RegisterComponents(Sub(r) r.ConfigureComponent(Of IZipService)(
                                      Function()
                                          Return New ZipFolderFactory()
                                      End Function, DependencyLifecycle.InstancePerCall)
                               )


        configuration.RegisterComponents(Sub(r) r.ConfigureComponent(Of ISE2XmlParser)(
                                      Function()
                                          Return New Se2XmlFileParser(New AcquisitionRepository(New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString)))
                                      End Function, DependencyLifecycle.InstancePerCall)
                               )



    End Sub
End Class
