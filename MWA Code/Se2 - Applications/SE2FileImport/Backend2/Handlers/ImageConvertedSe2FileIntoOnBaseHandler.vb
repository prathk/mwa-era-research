﻿Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Events
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain
Imports NServiceBus
Imports NServiceBus.Logging

Public Class ImageConvertedSe2FileIntoOnBaseHandler : Implements IHandleMessages(Of ImageConvertedSe2FileIntoOnBase)


    Public Property OnbaseRepository As IOnbaseRepository
    Public Property SE2ToOnbaseKeywordComparer As SE2ToOnbaseKeywordComparer
    Public Property SE2FileToOnbaseMapper As SE2FileToOnbaseMapperFactory
    Private log As ILog = LogManager.GetLogger("ImageConvertedSe2FileIntoOnBaseHandler")

    Public Sub New(onbaseRepository As IOnbaseRepository)

        Me.OnbaseRepository = onbaseRepository
        Me.SE2FileToOnbaseMapper = New SE2FileToOnbaseMapperFactory(Me.OnbaseRepository)
        Me.SE2ToOnbaseKeywordComparer = New SE2ToOnbaseKeywordComparer(Me.OnbaseRepository)
    End Sub

    Public Async Function Handle(message As ImageConvertedSe2FileIntoOnBase, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of ImageConvertedSe2FileIntoOnBase).Handle
        Dim noErrorsWhileProcessing = True
        Try
            Dim docTypeName = OnbaseRepository.RetrieveOnBaseDocType(message.DocType, message.DocProcess)

            Dim documentAlreadyInOnbase = OnbaseRepository.GetDocumentFromOnbase(message)

            Dim se2ToOnbaseModel = New SE2ToOnbase(message)


            If (documentAlreadyInOnbase IsNot Nothing) Then
                If (SE2ToOnbaseKeywordComparer.DocumentHasChanged(documentAlreadyInOnbase, se2ToOnbaseModel) = False) Then
                    Dim mappedDocument As OnBase.Document = SE2FileToOnbaseMapper.MapSE2KeywordsToOnbaseDocument(documentAlreadyInOnbase, se2ToOnbaseModel)
                    OnbaseRepository.Save(documentAlreadyInOnbase)
                End If

            Else
                Dim doc = OnbaseRepository.CreateDocument(docTypeName)
                Dim mappedDocument As OnBase.Document = SE2FileToOnbaseMapper.MapSE2FileToNewOnbaseDocument(doc, se2ToOnbaseModel)
                OnbaseRepository.Save(mappedDocument, message.FilePath)
            End If
        Catch ex As Exception
            context.Publish(Of Se2FileConversionToTiffFailed)(Function(m)
                                                                  m.ErrorDateTime = DateTime.Now
                                                                  m.FileDate = message.FileDate
                                                                  m.FileName = message.FileName
                                                                  m.ZipFileName = message.ZipFileName
                                                                  m.Ex = "Invaild cast exception when loading to onbase"
                                                                  Return m
                                                              End Function).GetAwaiter.GetResult()
            context.Send(New DeleteConvertedFileFromNeeviaFolder With {.FileName = message.FileName, .FilePath = message.FilePath}).GetAwaiter().GetResult()
            log.Error("Error loading into onbase", ex)
            noErrorsWhileProcessing = False
        End Try



        If noErrorsWhileProcessing Then
            Await context.Publish(Of SE2FileImaged)(Function(r)
                                                        r.ZipFileName = message.ZipFileName
                                                        r.Timestamp = DateTime.Now
                                                        r.ImagedFileName = message.FileName
                                                        Return r
                                                    End Function).ConfigureAwait(False)
            Await context.Send(New DeleteConvertedFileFromNeeviaFolder With {.FileName = message.FileName, .FilePath = message.FilePath}).ConfigureAwait(False)
        End If
    End Function
End Class
