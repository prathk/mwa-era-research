﻿Imports NServiceBus
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Commands
Imports System.Configuration

Public Class CleanupFoldersForFileConversionRetryHandler : Implements IHandleMessages(Of CleanupFoldersForFileConversionRetry)

    Private _neeviaInFolder As String = ConfigurationManager.AppSettings("NeeviaInFolder")
    Private _neeviaOutFolder As String = ConfigurationManager.AppSettings("NeeviaOutFolder")
    Private _neeviaErrorFolder As String = ConfigurationManager.AppSettings("NeeviaErrorFolder")
    Public Async Function Handle(message As CleanupFoldersForFileConversionRetry, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of CleanupFoldersForFileConversionRetry).Handle

        ''Grab and cleanup the input folder
        Dim inputFile = System.IO.Path.Combine(_neeviaInFolder, message.ZipFileName.Replace(".zip", ""), message.FileName)
        If (System.IO.File.Exists(inputFile)) Then
            IO.File.Delete(inputFile)
        End If


        ''Grab and cleanup the output folder if .msg and well as directory in input for .msg
        If (message.FileName.EndsWith(".msg")) Then
            Dim inputFileMsgDirectory = System.IO.Path.Combine(_neeviaInFolder, message.ZipFileName.Replace(".zip", ""), message.FileName & ".files")
            If (IO.Directory.Exists(inputFileMsgDirectory)) Then
                IO.Directory.Delete(inputFileMsgDirectory, True)
            End If

            Dim outFileMsgFolder = System.IO.Path.Combine(_neeviaOutFolder, message.ZipFileName.Replace(".zip", ""), message.FileName)
            If (IO.Directory.Exists(outFileMsgFolder)) Then
                IO.Directory.Delete(outFileMsgFolder, True)
            End If

            If (System.IO.Directory.Exists(IO.Path.Combine(_neeviaErrorFolder, message.ZipFileName.Replace(".zip", ""), message.FileName & ".files"))) Then
                IO.Directory.Delete(IO.Path.Combine(_neeviaErrorFolder, message.ZipFileName.Replace(".zip", ""), message.FileName & ".files"), True)
            End If

        End If

        Await context.Send(New ConvertSe2FileToTiff With {.FileName = message.FileName, .MimeType = message.MimeType, .Se2ObjectId = message.Se2ObjectId, .ZipFileName = message.ZipFileName}).ConfigureAwait(False)

    End Function
End Class
