﻿'Imports NServiceBus
'Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
'Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Events
'Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain



'Public Class ImageFileAndSendToOnbaseHandler
'    Implements IHandleMessages(Of ImageFileAndSendToOnbase)

'    'Public Property SE2FileRepository As ISE2FileRepository
'    'Public Property FileConverter As IFileConverter
'    Public Property OnbaseRepository As IOnbaseRepository
'    Public Property SE2ToOnbaseKeywordComparer As SE2ToOnbaseKeywordComparer
'    Public Property SE2FileToOnbaseMapper As SE2FileToOnbaseMapperFactory

'    Public Sub New(
'                   onbaseRepository As IOnbaseRepository)

'        'Me.SE2FileRepository = SE2FileRepository
'        'Me.FileConverter = fileConverter
'        Me.OnbaseRepository = onbaseRepository
'        Me.SE2FileToOnbaseMapper = New SE2FileToOnbaseMapperFactory(Me.OnbaseRepository)
'        Me.SE2ToOnbaseKeywordComparer = New SE2ToOnbaseKeywordComparer(Me.OnbaseRepository)
'    End Sub



'    Public Async Function Handle(message As ImageFileAndSendToOnbase, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of ImageFileAndSendToOnbase).Handle
'        '   Dim filePath = SE2FileRepository.GetFilePath(message.SE2ObjectId, message.FileName, message.ZipFileName.Replace(".zip", ""))

'        Dim noErrorsWhileProcessing = True
'        Dim pathToConvertedFile As String = ""

'        Try
'            Dim cert = CInt(message.CertNumber)
'        Catch ex As Exception
'            context.Publish(Of Se2FileConversionToTiffFailed)(Function(r)
'                                                                  r.ErrorDateTime = DateTime.Now
'                                                                  r.FileDate = message.FileDate
'                                                                  r.FileName = message.FileName
'                                                                  r.ZipFileName = message.ZipFileName
'                                                                  r.Ex = "Certificate Number was not an integer"
'                                                                  Return r
'                                                              End Function).GetAwaiter.GetResult()
'            noErrorsWhileProcessing = False
'        End Try

'        'If (noErrorsWhileProcessing) Then
'        '    Try
'        '        pathToConvertedFile = FileConverter.ConvertFileToTiffAndReturnPathToNewFile(filePath, message.MimeType)
'        '    Catch ex As Exception

'        '        context.Publish(Of Se2FileConversionToTiffFailed)(Function(r)
'        '                                                              r.ErrorDateTime = DateTime.Now
'        '                                                              r.FileDate = message.FileDate
'        '                                                              r.FileName = message.FileName
'        '                                                              r.ZipFileName = message.ZipFileName
'        '                                                              r.Ex = "Failed to convert file to a tiff"
'        '                                                              Return r
'        '                                                          End Function).GetAwaiter.GetResult()
'        '        noErrorsWhileProcessing = False
'        '    End Try
'        'End If



'        If noErrorsWhileProcessing Then
'            Try
'                Dim docTypeName = OnbaseRepository.RetrieveOnBaseDocType(message.DocType, message.DocProcess)

'                Dim documentAlreadyInOnbase = OnbaseRepository.GetDocumentFromOnbase(message)

'                Dim se2ToOnbaseModel = New SE2ToOnbase(message)


'                If (documentAlreadyInOnbase IsNot Nothing) Then
'                    If (SE2ToOnbaseKeywordComparer.DocumentHasChanged(documentAlreadyInOnbase, se2ToOnbaseModel) = False) Then
'                        Dim mappedDocument As OnBase.Document = SE2FileToOnbaseMapper.MapSE2KeywordsToOnbaseDocument(documentAlreadyInOnbase, se2ToOnbaseModel)
'                        OnbaseRepository.Save(documentAlreadyInOnbase)
'                    End If

'                Else
'                    Dim doc = OnbaseRepository.CreateDocument(docTypeName)
'                    Dim mappedDocument As OnBase.Document = SE2FileToOnbaseMapper.MapSE2FileToNewOnbaseDocument(doc, se2ToOnbaseModel)
'                    OnbaseRepository.Save(mappedDocument, pathToConvertedFile)
'                End If
'            Catch ex As Exception
'                context.Publish(Of Se2FileConversionToTiffFailed)(Function(m)
'                                                                      m.ErrorDateTime = DateTime.Now
'                                                                      m.FileDate = message.FileDate
'                                                                      m.FileName = message.FileName
'                                                                      m.ZipFileName = message.ZipFileName
'                                                                      m.Ex = "Invaild cast exception when loading to onbase"
'                                                                      Return m
'                                                                  End Function).GetAwaiter.GetResult()

'                noErrorsWhileProcessing = False
'            End Try

'        End If

'        If noErrorsWhileProcessing Then
'            Await context.Publish(Of SE2FileImaged)(Function(r)
'                                                        r.ZipFileName = message.ZipFileName
'                                                        r.Timestamp = DateTime.Now
'                                                        r.ImagedFileName = message.FileName
'                                                        Return r
'                                                    End Function).ConfigureAwait(False)
'        End If
'    End Function
'End Class
