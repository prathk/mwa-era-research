﻿Imports NServiceBus
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain

Public Class DeleteConvertedFileFromNeeviaFolderHandler : Implements IHandleMessages(Of DeleteConvertedFileFromNeeviaFolder)

    Private _Se2FileRepo As ISE2FileRepository
    Public Sub New(se2FileRepo As ISE2FileRepository)
        _Se2FileRepo = se2FileRepo
    End Sub
    Public Function Handle(message As DeleteConvertedFileFromNeeviaFolder, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of DeleteConvertedFileFromNeeviaFolder).Handle

        If (System.IO.File.Exists(message.FilePath)) Then

            _Se2FileRepo.DeleteFile(message.FilePath)

        End If
        Return Task.CompletedTask()

    End Function
End Class
