﻿Imports NServiceBus
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Events

Public Class ConvertSe2FileToTiffHandler : Implements IHandleMessages(Of ConvertSe2FileToTiff)

    Public Property _SE2FileRepository As ISE2FileRepository
    Public Property _FileConverter As IFileConverter

    Public Sub New(fileRepository As ISE2FileRepository, fileConvert As IFileConverter)
        Me._SE2FileRepository = fileRepository
        Me._FileConverter = fileConvert
    End Sub


    Public Async Function Handle(message As ConvertSe2FileToTiff, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of ConvertSe2FileToTiff).Handle

        Dim filePath = _SE2FileRepository.GetFilePath(message.Se2ObjectId, message.FileName, message.ZipFileName.Replace(".zip", ""))

        ''Drops the files in the Neevia In folder for conversion and returns the file location for the output of the file
        Dim pathToConvertedFile = _FileConverter.ConvertFileToTiffAndReturnPathToNewFile(filePath, message.MimeType, message.FileName, message.ZipFileName)

        Await context.Publish(Of Se2FileSentToNeeviaFolderForConversion)(Function(r)
                                                                             r.FileName = message.FileName
                                                                             r.OutputFileLocation = pathToConvertedFile
                                                                             r.ZipFileName = message.ZipFileName
                                                                             Return r
                                                                         End Function).ConfigureAwait(False)



    End Function
End Class
