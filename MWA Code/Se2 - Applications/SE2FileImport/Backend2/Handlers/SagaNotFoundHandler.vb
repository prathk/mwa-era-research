﻿Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Events
Imports NServiceBus
Imports NServiceBus.Sagas

Public Class SagaNotFoundHandler : Implements IHandleSagaNotFound

    Public Async Function Handle(message As Object, context As IMessageProcessingContext) As Task Implements IHandleSagaNotFound.Handle

        If (TypeOf message Is Se2FileSentToNeeviaFolderForConversion) Then
            Dim convertedMessage = CType(message, Se2FileSentToNeeviaFolderForConversion)
            Await context.Publish(Of Se2FileConversionToTiffFailed)(Function(r)
                                                                        r.ErrorDateTime = DateTime.Now
                                                                        r.FileDate = DateTime.Now
                                                                        r.FileName = convertedMessage.FileName
                                                                        r.ZipFileName = convertedMessage.ZipFileName
                                                                        r.Ex = "Couldn't find correspondending saga"
                                                                        Return r
                                                                    End Function).ConfigureAwait(False)
        End If



    End Function



End Class
