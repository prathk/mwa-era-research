﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Commands

<TestClass()> Public Class SE2ToOnbaseTests

    <TestMethod()> Public Sub SE2CommandPropertiesMappedCorrectly()

        Dim se2CommandProperties As New ImageConvertedSe2FileIntoOnBase With
            {.CertNumber = "8537813",
            .DocProcess = "Death",
            .DocType = "Death Proofs",
            .EmployerId = "",
            .FileName = "12355SE2P846771-3.msg",
            .MimeType = "application/vnd.ms-outlook",
            .OnbaseDocId = Integer.MinValue,
            .ZipFileName = "P-SE2-04-06-2015_10-00-11",
            .ScanDate = "2012-12-20",
            .SE2DocId = "20121220-MW-448922",
            .SE2ObjectId = "12355SE2P846771",
            .SSN = "222222222",
            .TPAPolicyNo = "E20302",
             .FileDate = DateTime.Now
            }


        '  Dim se2CommandProperties As New ImageFileAndSendToOnbase With {.SagaId = Guid.NewGuid, .Se2FileInformation = se2Information}

        Dim model = New SE2ToOnbase(se2CommandProperties)


        Assert.IsTrue(model.KeywordList.ContainsKey("Cert No"))
        Assert.IsTrue(model.KeywordList.ContainsKey("Se2 Object ID"))
        Assert.IsTrue(model.KeywordList.ContainsKey("Se2 Doc ID"))
        Assert.IsTrue(model.KeywordList.ContainsKey("TPA Policy No"))
        Assert.IsTrue(model.KeywordList.ContainsKey("Se2 File Name"))

        Assert.AreEqual(model.KeywordList("Cert No"), se2CommandProperties.CertNumber)
        Assert.AreEqual(model.KeywordList("Se2 Object ID"), se2CommandProperties.SE2ObjectId)
        Assert.AreEqual(model.KeywordList("Se2 Doc ID"), se2CommandProperties.SE2DocId)
        Assert.AreEqual(model.KeywordList("TPA Policy No"), se2CommandProperties.TPAPolicyNo)
        Assert.AreEqual(model.KeywordList("Se2 File Name"), se2CommandProperties.FileName)


        Assert.AreEqual(model.DocProcess, se2CommandProperties.DocProcess)
        Assert.AreEqual(model.DocType, se2CommandProperties.DocType)
        Assert.AreEqual(model.FileName, se2CommandProperties.FileName)
        Assert.AreEqual(model.MimeType, se2CommandProperties.MimeType)
        Assert.AreEqual(model.OnbaseDocId, se2CommandProperties.OnbaseDocId)
        Assert.AreEqual(model.ZipFileName, se2CommandProperties.ZipFileName)




    End Sub

End Class