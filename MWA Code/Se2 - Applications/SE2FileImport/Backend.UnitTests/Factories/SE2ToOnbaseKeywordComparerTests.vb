﻿Imports System.Configuration
Imports mwa.OnBase
Imports mwa.DataAccess.Common
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Commands
<Ignore>
<TestClass()> Public Class SE2ToOnbaseKeywordComparerTests

    <TestMethod()> Public Sub DocumentHasChangedTest()
        Dim onbaseRepository As New OnbaseRepository(ConfigurationManager.AppSettings("ImagingWSURL"), _
                                                   ConfigurationManager.AppSettings("NewDocumentWorkflowName"), _
                                                    New DocumentRepository(ConfigurationManager.AppSettings("ImagingUser")), _
                                                     New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString))



        Dim onbaseKeywordComparer As New SE2ToOnbaseKeywordComparer(onbaseRepository)

        Dim doc = onbaseRepository.GetDocumentFromOnbase(ImageFileAndSendToOnbaseModel)

        Dim se2ToOnbase = New SE2ToOnbase(ImageFileAndSendToOnbaseModel)

        Dim hasDocumentChanged = onbaseKeywordComparer.DocumentHasChanged(doc, se2ToOnbase)

        Assert.IsFalse(hasDocumentChanged)


    End Sub

    Private Function ImageFileAndSendToOnbaseModel() As ImageConvertedSe2FileIntoOnBase

        Return New ImageConvertedSe2FileIntoOnBase With
    {.CertNumber = "8537813",
    .DocProcess = "Death",
    .DocType = "Death Proofs",
    .EmployerId = "",
    .FileName = "12355SE2P846771-3.msg",
    .MimeType = "application/vnd.ms-outlook",
    .OnbaseDocId = Integer.MinValue,
    .ZipFileName = "P-SE2-04-06-2015_10-00-11",
    .ScanDate = "2012-12-20",
    .SE2DocId = "20121220-MW-448922",
    .SE2ObjectId = "12355SE2P846771",
    .SSN = "222222222",
    .TPAPolicyNo = "E20302",
     .FileDate = DateTime.Now
    }


        '  Return New ImageFileAndSendToOnbase With {.SagaId = Guid.NewGuid, .Se2FileInformation = se2Information}

    End Function

End Class