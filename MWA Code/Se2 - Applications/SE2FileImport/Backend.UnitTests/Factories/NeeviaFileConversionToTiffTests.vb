﻿Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain
Imports System.Configuration


<TestClass()> Public Class NeeviaFileConversionToTiffTests

    Private neeviaOutFolder As String = System.IO.Directory.GetCurrentDirectory().Replace("\bin\Debug", "") & "\TestFile\NeeviaOutFolder\" 'ConfigurationManager.AppSettings("NeeviaOutFolder")
    Private neeviaInFolder As String = System.IO.Directory.GetCurrentDirectory().Replace("\bin\Debug", "") & "\TestFile\NeeviaInFolder\" 'ConfigurationManager.AppSettings("NeeviaInFolder")
    <TestMethod()> Public Sub ConvertFileToTiffAndReturnPathToNewTiffFileTest()

        Dim neeviaInFolderFile = System.IO.Path.Combine(neeviaInFolder, "12355SE2P846771", "12355SE2P846771-11.msg")

        If (System.IO.File.Exists(neeviaInFolderFile)) Then
            IO.File.Delete(neeviaInFolderFile)
        End If


        Dim neeviaFactory As New NeeviaFileConversionToTiff(neeviaInFolder, neeviaOutFolder)

        Dim filePath = System.IO.Directory.GetCurrentDirectory().Replace("\bin\Debug", "") & "\TestFile\12355SE2P846771\12355SE2P846771-11.msg"

        Dim convertedFilePath = neeviaFactory.ConvertFileToTiffAndReturnPathToNewFile(filePath, "application/vnd.ms-outlook", "12355SE2P846771-11.msg", "12355SE2P846771")

        Dim neeviaOutFolderFile = System.IO.Path.Combine(neeviaOutFolder, "12355SE2P846771", "12355SE2P846771-11.tif")

        Assert.IsTrue(System.IO.File.Exists(neeviaInFolderFile))
        Assert.AreEqual(neeviaOutFolderFile, convertedFilePath)

    End Sub

    <TestMethod()> Public Sub ConvertFileToTiffAndReturnPathToExistingFileInNeeviaOutFolderTest()
        Dim neeviaOutFolderFile = System.IO.Path.Combine(neeviaOutFolder, "12355SE2P846771", "12355SE2P846771-1.txt")
        If (System.IO.File.Exists(neeviaOutFolderFile)) Then
            IO.File.Delete(neeviaOutFolderFile)
        End If


        Dim neeviaFactory As New NeeviaFileConversionToTiff(neeviaInFolder, neeviaOutFolder)

        Dim filePath = System.IO.Directory.GetCurrentDirectory().Replace("\bin\Debug", "") & "\TestFile\12355SE2P846771\12355SE2P846771-1.txt"
        Dim converted = neeviaFactory.ConvertFileToTiffAndReturnPathToNewFile(filePath, "text/plain", "12355SE2P846771-1.txt", "12355SE2P846771")

        Dim neeviaInFolderFile = System.IO.Path.Combine(neeviaInFolder, "12355SE2P846771", "12355SE2P846771-1.txt")

        Assert.AreEqual(neeviaOutFolderFile, converted)
        Assert.IsFalse(System.IO.File.Exists(neeviaInFolderFile))


    End Sub



End Class