﻿Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain

<TestClass()> Public Class ZipFolderFactoryTest

    <TestMethod()> Public Sub UnZipTest()

        Dim xmlFilePath = System.IO.Directory.GetCurrentDirectory().Replace("\bin\Debug", "") & "\TestFile\P-SE2-04-05-2015_10-00-10.zip"
        If (System.IO.File.Exists(xmlFilePath.Replace(".zip", "") & "\P-SE2-04-05-2015_10-00-10.xml")) Then
            System.IO.File.Delete(xmlFilePath.Replace(".zip", "") & "\P-SE2-04-05-2015_10-00-10.xml")
        End If

        Dim unzipFactory As New ZipFolderFactory
        unzipFactory.UnZip(xmlFilePath, xmlFilePath.Replace(".zip", ""))

        Assert.IsTrue(System.IO.File.Exists(xmlFilePath.Replace(".zip", "") & "\P-SE2-04-05-2015_10-00-10.xml"))

    End Sub

End Class