﻿Imports System.Configuration
Imports mwa.DataAccess.Common
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain

<TestClass()> Public Class Se2XmlFileParserTests

    <TestMethod()> Public Sub ParseSe2XmlToListOfImageFileAndSendToOnbaseTest()

        Dim xmlFilePath = System.IO.Directory.GetCurrentDirectory().Replace("\bin\Debug", "") & "\TestFile\P-SE2-08-08-2016_10-00-18.xml"
        Dim acquisitionRepository As New AcquisitionRepository(New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString))


        Dim se2FileParser = New Se2XmlFileParser(acquisitionRepository)
        Dim lstOfCommands = se2FileParser.ParseSe2XmlToListOfImageFileAndSendToOnbase(xmlFilePath, "P-SE2-08-08-2016_10-00-18")


        Assert.IsNotNull(lstOfCommands)
        Assert.IsTrue(lstOfCommands.Count > 0)

    End Sub

End Class