﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports NServiceBus.Testing
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Events
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain
Imports NServiceBus
Imports mwa.DataAccess.Common
Imports System.Configuration
<Ignore>
<TestClass()> Public Class SendSe2FileEmailReportHandlerTest

    <TestMethod()> Public Sub Test_PublishEventBatchHandlerTest()
        Dim acquisitionRepository As New AcquisitionRepository(New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString))
        Dim errors As New Dictionary(Of String, String)
        errors.Add("14129SE2P364892-4.msg", "Error converting file")

        ' Test.Initialize()
        Test.Handler(New SendSe2FileEmailReportHandler(New SE2FileRepository, New Se2XmlFileParser(acquisitionRepository))).
        ExpectPublish(Of Se2FileEmailReportSent)(Function(s) s.ZipFileName = "P-SE2-10-16-2015_10-00-04.zip").
        OnMessage(Of SendSe2FileEmailReport)(Function(c)
                                                 c.ZipFileName = "P-SE2-10-16-2015_10-00-04.zip"
                                                 c.DateCompleted = DateTime.Now
                                                 c.FilesCompletedCount = 700
                                                 c.Errors = errors
                                                 Return c
                                             End Function)
    End Sub



End Class