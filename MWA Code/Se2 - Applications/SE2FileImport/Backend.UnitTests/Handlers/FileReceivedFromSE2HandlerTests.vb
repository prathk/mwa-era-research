﻿
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport
Imports mwa.DataAccess.Common
Imports System.Configuration
Imports NServiceBus
Imports NServiceBus.Testing
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain

<Ignore>
<TestClass()> Public Class FileReceivedFromSE2HandlerTests

    <TestMethod()> Public Sub FileReceivedFromSE2HandlerTests()
        Dim acquisitionRepository As New AcquisitionRepository(New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString))

        Dim se2FileRepository As New SE2FileRepository
        Dim zipFactory As New ZipFolderFactory()
        Dim se2XmlFileParser As New Se2XmlFileParser(acquisitionRepository)

        '  Test.Initialize()

        'Getting error with concrete type when using the Interface only on the onmessage
        Test.Handler(New ProcessZippedFileReceivedFromSE2Handler(se2FileRepository, zipFactory, se2XmlFileParser)).
        ExpectSend(Of Commands.ImageFileAndSendToOnbase)(Function(r) r.ZipFileName = "P-SE2-03-30-2015_10-00-08").
        OnMessage(Of ProcessZippedFileReceivedFromSE2)(Function(r)
                                                           r.ZipFileName = "P-SE2-03-30-2015_10-00-08.zip"
                                                           r.Timestamp = DateTime.Now
                                                           Return r
                                                       End Function)




    End Sub

End Class