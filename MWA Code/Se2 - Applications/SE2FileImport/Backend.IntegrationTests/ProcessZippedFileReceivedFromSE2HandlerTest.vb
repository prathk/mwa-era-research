﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports NServiceBus
Imports NServiceBus.Testing

<TestClass()> Public Class ProcessZippedFileReceivedFromSE2HandlerTest

    <TestMethod()> Public Async Function TestMethod1() As Task

        Dim busConfiguration = New EndpointConfiguration(Reflection.Assembly.GetExecutingAssembly.GetName.Name)
        busConfiguration.UsePersistence(Of InMemoryPersistence)()

        busConfiguration.Conventions.DefiningCommandsAs(Function(r) r.Namespace <> Nothing AndAlso r.Namespace.EndsWith("Commands"))
        'busConfiguration.Conventions.DefiningEventsAs(Function(r) r.Namespace <> Nothing AndAlso r.Namespace.EndsWith("Events"))



        busConfiguration.SendOnly()
        Dim anEndpoint = Await Endpoint.Start(busConfiguration)

        Await anEndpoint.Send("mwa.CustomerService.CommunicationManagement.Se2FileImport@ESBOMNIdev-H1", New ProcessZippedFileReceivedFromSE2 With {.Timestamp = DateTime.Now, .ZipFileName = "P-SE2-05-05-2018_10-00-17.zip"})

    End Function

End Class