﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain
Imports mwa.DataAccess.Common
Imports mwa.OnBase
Imports NServiceBus.Testing

<TestClass()> Public Class ImageConvertedSe2FileIntoOnbaseHandlerTest

    <TestMethod()> Public Async Function TestMethod1() As Task


        Dim testContext As New TestableMessageHandlerContext

        Dim message As New ImageConvertedSe2FileIntoOnBase With {.CertNumber = 8541890, .DocProcess = "Disability", .DocType = "Correspondence", .EmployerId = "", .FileConversionAttempts = 0, .FileDate = New Date(2018, 4, 13), .FileName = "18101SE2P339284.tif", .FilePath = "G:\MWA\Uat\CustomerService\CommunicationManagement\SE2FileImport\FileError\P-SE2-04-13-2018_10-00-03\18101SE2P339284.tif", .MimeType = "text/plain", .OnbaseDocId = 0, .ScanDate = New Date(2018, 4, 11), .SE2DocId = "20180411-F-53609-01", .SE2ObjectId = "18101SE2P339285", .TPAPolicyNo = "", .ZipFileName = "P-SE2-04-13-2018_10-00-03.zip"}

        Dim handler As New ImageConvertedSe2FileIntoOnBaseHandler(New OnbaseRepository("http://swebservices.modern-woodmen.org/prjImagingWS/ImagingWS.asmx", "TPA-Indexing", New DocumentRepository(New Uri("http://swebservices.modern-woodmen.org/prjImagingWS/ImagingWS.asmx"), "TPAUser"), New ConnectionStringFactory("Application Name=SE2FileImport;Server=sqlstg-h1.modern-woodmen.org;Trusted_Connection=True;Encrypt=True;TrustServerCertificate=True;")))


        Await handler.Handle(message, testContext)


    End Function

End Class