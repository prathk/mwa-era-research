﻿Imports System.Configuration
Imports mwa.OnBase
Imports mwa.DataAccess.Common
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Commands

<Ignore>
<TestClass()> Public Class OnbaseRepositoryTests

    <TestMethod()> Public Sub GetDocumentFromOnbaseTest()

        Dim onbaseRepository As New OnbaseRepository(ConfigurationManager.AppSettings("ImagingWSURL"), _
                                                      ConfigurationManager.AppSettings("NewDocumentWorkflowName"), _
                                                       New DocumentRepository(ConfigurationManager.AppSettings("ImagingUser")), _
                                                        New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString))


        Dim model = ImageFileAndSendToOnbaseModel()

        Dim doc = onbaseRepository.GetDocumentFromOnbase(model)

        Assert.IsNotNull(doc)
        Assert.AreEqual(doc.Keywords("Cert No")(0).Value, model.CertNumber)
        Assert.AreEqual(doc.Keywords("Se2 Doc ID")(0).Value, model.SE2DocId)
        Assert.AreEqual(doc.Keywords("Se2 Object ID")(0).Value, model.SE2ObjectId)

    End Sub

    <TestMethod()> Public Sub CreateDocumentTest()

        Dim onbaseRepository As New OnbaseRepository(ConfigurationManager.AppSettings("ImagingWSURL"), _
                                                      ConfigurationManager.AppSettings("NewDocumentWorkflowName"), _
                                                       New DocumentRepository(ConfigurationManager.AppSettings("ImagingUser")), _
                                                        New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString))



        Dim doc = onbaseRepository.CreateDocument("CRMasterFile")

        Assert.IsNotNull(doc)


    End Sub



    <TestMethod()> Public Sub CreateKeywordGroupTest()

        Dim onbaseRepository As New OnbaseRepository(ConfigurationManager.AppSettings("ImagingWSURL"), _
                                                      ConfigurationManager.AppSettings("NewDocumentWorkflowName"), _
                                                       New DocumentRepository(ConfigurationManager.AppSettings("ImagingUser")), _
                                                        New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString))


        'I have no idea what a valid keyword group is? And it errors out when you pass a bad valid in
        Try
            onbaseRepository.CreateKeywordGroup("TEst")
        Catch ex As Exception
            Assert.IsTrue(True)
        End Try

    End Sub

    <TestMethod()> Public Sub RetrieveOnBaseDocTypeTest()

        Dim onbaseRepository As New OnbaseRepository(ConfigurationManager.AppSettings("ImagingWSURL"), _
                                                      ConfigurationManager.AppSettings("NewDocumentWorkflowName"), _
                                                       New DocumentRepository(ConfigurationManager.AppSettings("ImagingUser")), _
                                                        New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString))


        Dim docType = onbaseRepository.RetrieveOnBaseDocType("Death Proofs", "Death")

        Assert.AreEqual(docType, "CRMasterFile")


        ''Need to find a doctype and process that actually gets a new doctype name

    End Sub


    <TestMethod()> Public Sub RetrieveDocTypeNumber()

        Dim onbaseRepository As New OnbaseRepository(ConfigurationManager.AppSettings("ImagingWSURL"), _
                                                      ConfigurationManager.AppSettings("NewDocumentWorkflowName"), _
                                                       New DocumentRepository(ConfigurationManager.AppSettings("ImagingUser")), _
                                                        New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString))


        Dim docTypeNumber = onbaseRepository.RetrieveDocTypeNumber("Death Proofs")

        Assert.AreEqual(docTypeNumber, 27)



    End Sub

    <TestMethod()> Public Sub RetrieveProcessNumber()

        Dim onbaseRepository As New OnbaseRepository(ConfigurationManager.AppSettings("ImagingWSURL"), _
                                                      ConfigurationManager.AppSettings("NewDocumentWorkflowName"), _
                                                       New DocumentRepository(ConfigurationManager.AppSettings("ImagingUser")), _
                                                        New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString))


        Dim processNumber = onbaseRepository.RetrieveProcessNumber("Death")

        Assert.AreEqual(processNumber, 12)



    End Sub



    Private Function ImageFileAndSendToOnbaseModel() As Backend2.Commands.ImageConvertedSe2FileIntoOnBase
        Return New Backend2.Commands.ImageConvertedSe2FileIntoOnBase With
                   {.CertNumber = "8537813",
                   .DocProcess = "Death",
                   .DocType = "Death Proofs",
                   .EmployerId = "",
                   .FileName = "12355SE2P846771-3.msg",
                   .MimeType = "application/vnd.ms-outlook",
                   .OnbaseDocId = Integer.MinValue,
                   .ZipFileName = "P-SE2-04-06-2015_10-00-11",
                   .ScanDate = "2012-12-20",
                   .SE2DocId = "20121220-MW-448922",
                   .SE2ObjectId = "12355SE2P846771",
                   .SSN = "222222222",
                   .TPAPolicyNo = "E20302",
                    .FileDate = Date.Now
                   }


        'Return New ImageFileAndSendToOnbase With {.SagaId = Guid.NewGuid, .Se2FileInformation = se2Information}


    End Function

End Class