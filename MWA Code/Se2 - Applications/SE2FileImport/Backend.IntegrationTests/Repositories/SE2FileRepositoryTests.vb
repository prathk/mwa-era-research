﻿Imports System.Configuration
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain

<Ignore>
<TestClass()> Public Class SE2FileRepositoryTests

    <TestMethod()> Public Sub GetFilePathTest()

        Dim se2FileRepository As New SE2FileRepository

        Dim file = se2FileRepository.GetFilePath("Se2ObjectId", "FileName.txt", "RootFileName")

        Dim comparer = ConfigurationManager.AppSettings("InProcessFolderPath").ToString & "\RootFileName\Se2ObjectId\FileName.txt"

        Assert.AreEqual(comparer, file)

    End Sub

    <TestMethod()> Public Sub GetFolderPathTest()

        Dim se2FileRepository As New SE2FileRepository

        Dim file = se2FileRepository.GetFolderPath("Pending", "Test.zip")

        Dim comparer = ConfigurationManager.AppSettings("FolderPath").ToString & "\Pending\Test.zip"

        Assert.AreEqual(comparer, file)

    End Sub

    <TestMethod()> Public Sub CopyFileTest()

        Dim testFilePath = System.IO.Directory.GetCurrentDirectory().Replace("\bin\Debug", "") & "\Test\"

        Dim se2FileRepository As New SE2FileRepository

        se2FileRepository.CopyFile(testFilePath & "TextFile1.txt", testFilePath & "\Copy\TextFile1.txt")

        Assert.IsTrue(System.IO.File.Exists(testFilePath & "\Copy\TextFile1.txt"))

    End Sub

    <TestMethod()> Public Sub DeleteFile()

        'Dim testFilePath = "\\modern-woodmen\gdrive\MWA\Dev\MemberServices\ERA\SE2FileImport\Pending\P-SE2-03-30-2015_10-00-08.zip" 'System.IO.Directory.GetCurrentDirectory().Replace("\bin\Debug", "") & "\Test\"

        'Dim se2FileRepository As New SE2FileRepository

        'se2FileRepository.DeleteFile(testFilePath)

        'Assert.IsFalse(System.IO.File.Exists(testFilePath))

    End Sub

    <TestMethod()> Public Sub DeleteAllFoldersInDirectory()

        'Dim testFilePath = "\\modern-woodmen\gdrive\MWA\Dev\MemberServices\ERA\SE2FileImport\InProcess" 'System.IO.Directory.GetCurrentDirectory().Replace("\bin\Debug", "") & "\Test\"

        'Dim se2FileRepository As New SE2FileRepository

        'se2FileRepository.DeleteAllFoldersInDirectory(testFilePath)

        'Assert.IsTrue(System.IO.Directory.GetFiles(testFilePath).Count = 0)

    End Sub

End Class
