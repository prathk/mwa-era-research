﻿Imports mwa.DataAccess.Common
Imports System.Configuration
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain

<TestClass()> Public Class AcquisitionRepositoryTests
    <Ignore>
    <TestMethod()> Public Sub DocumentMapping_IncomingTest()

        Dim acquistionRepo As New AcquisitionRepository(New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString))

        Dim model = acquistionRepo.DocumentMapping_Incoming("ERA")

        Assert.AreEqual(model(0).MwaDocType, "Death Proofs")
        Assert.AreEqual(model(0).MwaProcessType, "Death")


    End Sub

End Class