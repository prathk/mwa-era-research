﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Events
Imports NServiceBus.Testing

<TestClass()> Public Class ConvertSe2FileToTiffHandlerTests

    Private _Context As New TestableMessageHandlerContext

    <Ignore>
    <TestMethod()> Public Async Function ConvertSe2FileToTiffHandlerValidTest() As Task


        Dim message As New ConvertSe2FileToTiff With {.FileName = "18093SE2P279418-1.txt", .MimeType = "text/plain", .Se2ObjectId = "20180403-F-41666-00", .ZipFileName = "P-SE2-04-05-2018_10-00-12.zip"}

        Dim se2RepoFake As New Se2FileRepositoryFakes()
        Dim fileConverterFake As New FileConverterFake()

        Dim handler As New ConvertSe2FileToTiffHandler(se2RepoFake, fileConverterFake)


        Await handler.Handle(message, _Context).ConfigureAwait(False)

        Dim sentMessage As Se2FileSentToNeeviaFolderForConversion = _Context.PublishedMessages(0).Message

        Assert.AreEqual(message.FileName, sentMessage.FileName)
        Assert.AreEqual(System.IO.Directory.GetCurrentDirectory().Replace("\bin\Debug", "") & "\TestFile\P-SE2-04-05-2018_10-00-12\" & message.FileName.Replace(".txt", ".tif"), sentMessage.OutputFileLocation)




    End Function

End Class