﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Commands
Imports NServiceBus.Testing
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Events

<Ignore>
<TestClass()> Public Class FileConversionImagingPolicyTests

    Private _Context As New TestableMessageHandlerContext


    <TestMethod()> Public Async Function ImageFileAndSendToOnbaseValidMessageTest() As Task

        Dim message As New ImageFileAndSendToOnbase With {.CertNumber = "8552011", .DocProcess = "Certificate Services*", .DocType = "Correspondence", .EmployerId = "",
                                                            .FileDate = New Date(2017, 1, 1), .FileName = "18093SE2P279418-1.txt", .SE2ObjectId = "18093SE2P279418",
                                                            .MimeType = "text/plain", .OnbaseDocId = 0, .ScanDate = New Date(2018, 4, 3), .SE2DocId = "20180403-F-41666-00",
                                                            .SSN = "", .TPAPolicyNo = "", .ZipFileName = "P-SE2-04-05-2018_10-00-12.zip"}

        Dim sagaData As New FileConversionImagingPolicyData()
        Dim saga As New FileConversionImagingPolicy With {.Data = sagaData}

        Await saga.Handle(message, _Context).ConfigureAwait(False)


        Assert.AreEqual(saga.Data.DocProcess, message.DocProcess)
        Assert.AreEqual(saga.Data.CertNumber, message.CertNumber)
        Assert.AreEqual(saga.Data.DocType, message.DocType)
        Assert.AreEqual(saga.Data.EmployerId, message.EmployerId)
        Assert.AreEqual(saga.Data.FileDate, message.FileDate)
        Assert.AreEqual(saga.Data.FileName, message.FileName)
        Assert.AreEqual(saga.Data.FileOutputPath, Nothing)
        Assert.AreEqual(saga.Data.MimeType, message.MimeType)
        Assert.AreEqual(saga.Data.OnbaseDocId, message.OnbaseDocId)
        Assert.AreEqual(saga.Data.ScanDate, message.ScanDate)
        Assert.AreEqual(saga.Data.SE2DocId, message.SE2DocId)
        Assert.AreEqual(saga.Data.SE2ObjectId, message.SE2ObjectId)
        Assert.AreEqual(saga.Data.SSN, message.SSN)
        Assert.AreEqual(saga.Data.TPAPolicyNo, message.TPAPolicyNo)
        Assert.AreEqual(saga.Data.ZipFileName, message.ZipFileName)


        Dim processedMessage As ConvertSe2FileToTiff = _Context.SentMessages(0).Message

        Assert.IsTrue(_Context.PublishedMessages.Count = 0)


        Assert.AreEqual(message.FileName, processedMessage.FileName)
        Assert.AreEqual(message.MimeType, processedMessage.MimeType)
        Assert.AreEqual(message.SE2ObjectId, processedMessage.Se2ObjectId)
        Assert.AreEqual(message.ZipFileName, processedMessage.ZipFileName)



    End Function
    <TestMethod()> Public Async Function ImageFileAndSendToOnbaseInValidMessageTest() As Task

        Dim message As New ImageFileAndSendToOnbase With {.CertNumber = "r8552011", .DocProcess = "Certificate Services*", .DocType = "Correspondence", .EmployerId = "",
                                                            .FileDate = New Date(2017, 1, 1), .FileName = "18093SE2P279418-1.txt", .SE2ObjectId = "18093SE2P279418",
                                                            .MimeType = "text/plain", .OnbaseDocId = 0, .ScanDate = New Date(2018, 4, 3), .SE2DocId = "20180403-F-41666-00",
                                                            .SSN = "", .TPAPolicyNo = "", .ZipFileName = "P-SE2-04-05-2018_10-00-12.zip"}

        Dim sagaData As New FileConversionImagingPolicyData()
        Dim saga As New FileConversionImagingPolicy With {.Data = sagaData}

        Await saga.Handle(message, _Context).ConfigureAwait(False)


        Assert.AreEqual(saga.Data.DocProcess, message.DocProcess)
        Assert.AreEqual(saga.Data.CertNumber, message.CertNumber)
        Assert.AreEqual(saga.Data.DocType, message.DocType)
        Assert.AreEqual(saga.Data.EmployerId, message.EmployerId)
        Assert.AreEqual(saga.Data.FileDate, message.FileDate)
        Assert.AreEqual(saga.Data.FileName, message.FileName)
        Assert.AreEqual(saga.Data.FileOutputPath, Nothing)
        Assert.AreEqual(saga.Data.MimeType, message.MimeType)
        Assert.AreEqual(saga.Data.OnbaseDocId, message.OnbaseDocId)
        Assert.AreEqual(saga.Data.ScanDate, message.ScanDate)
        Assert.AreEqual(saga.Data.SE2DocId, message.SE2DocId)
        Assert.AreEqual(saga.Data.SE2ObjectId, message.SE2ObjectId)
        Assert.AreEqual(saga.Data.SSN, message.SSN)
        Assert.AreEqual(saga.Data.TPAPolicyNo, message.TPAPolicyNo)
        Assert.AreEqual(saga.Data.ZipFileName, message.ZipFileName)


        Dim processedMessage As Se2FileConversionToTiffFailed = _Context.PublishedMessages(0).Message

        Assert.IsTrue(_Context.SentMessages.Count = 0)


        Assert.AreEqual(message.FileName, processedMessage.FileName)
        Assert.AreEqual(message.ZipFileName, processedMessage.ZipFileName)
        Assert.AreEqual("Certificate Number was not an integer", processedMessage.Ex)

        Assert.IsTrue(saga.Completed)



    End Function

    <TestMethod()> Public Async Function Se2FileSentToNeeviaFolderForConversionMessageValidFileExistsTest() As Task

        Dim aEvent = New TestSe2FileSentToNeeviaFolderForConversion With {.FileName = "18093SE2P279418-1.txt", .OutputFileLocation = "\\modern-woodmen\gdrive\MWA\Dev\CustomerService\CommunicationManagement\SE2FileImport\NeeviaOutFolder\18093SE2P279418-1.txt"}

        Dim sagaData As New FileConversionImagingPolicyData() With {.CertNumber = "8552011", .DocProcess = "Certificate Services*", .DocType = "Correspondence", .EmployerId = "",
                                                            .FileDate = New Date(2017, 1, 1), .FileName = "18093SE2P279418-1.txt", .SE2ObjectId = "18093SE2P279418",
                                                            .MimeType = "text/plain", .OnbaseDocId = 0, .ScanDate = New Date(2018, 4, 3), .SE2DocId = "20180403-F-41666-00",
                                                            .SSN = "", .TPAPolicyNo = "", .ZipFileName = "P-SE2-04-05-2018_10-00-12.zip"}
        Dim saga As New FileConversionImagingPolicy With {.Data = sagaData}

        Await saga.Handle(aEvent, _Context).ConfigureAwait(False)


        Assert.AreEqual(saga.Data.FileName, aEvent.FileName)
        Assert.AreEqual(saga.Data.FileOutputPath, aEvent.OutputFileLocation)


        Dim processedMessage As ImageConvertedSe2FileIntoOnBase = _Context.SentMessages(0).Message



        Assert.AreEqual(saga.Data.DocProcess, processedMessage.DocProcess)
        Assert.AreEqual(saga.Data.CertNumber, processedMessage.CertNumber)
        Assert.AreEqual(saga.Data.DocType, processedMessage.DocType)
        Assert.AreEqual(saga.Data.EmployerId, processedMessage.EmployerId)
        Assert.AreEqual(saga.Data.FileDate, processedMessage.FileDate)
        Assert.AreEqual(saga.Data.FileName, processedMessage.FileName)
        Assert.AreEqual(saga.Data.FileOutputPath, processedMessage.FilePath)
        Assert.AreEqual(saga.Data.MimeType, processedMessage.MimeType)
        Assert.AreEqual(saga.Data.OnbaseDocId, processedMessage.OnbaseDocId)
        Assert.AreEqual(saga.Data.ScanDate, processedMessage.ScanDate)
        Assert.AreEqual(saga.Data.SE2DocId, processedMessage.SE2DocId)
        Assert.AreEqual(saga.Data.SE2ObjectId, processedMessage.SE2ObjectId)
        Assert.AreEqual(saga.Data.SSN, processedMessage.SSN)
        Assert.AreEqual(saga.Data.TPAPolicyNo, processedMessage.TPAPolicyNo)
        Assert.AreEqual(saga.Data.ZipFileName, processedMessage.ZipFileName)

        Assert.IsTrue(saga.Completed)


    End Function

    <TestMethod()> Public Async Function Se2FileSentToNeeviaFolderForConversionMessageValidFileDoesntExistsTest() As Task

        Dim aEvent = New TestSe2FileSentToNeeviaFolderForConversion With {.FileName = "18093SE2P279418-2.txt", .OutputFileLocation = "\\modern-woodmen\gdrive\MWA\Dev\CustomerService\CommunicationManagement\SE2FileImport\NeeviaOutFolder\18093SE2P279418-2.txt"}

        Dim sagaData As New FileConversionImagingPolicyData() With {.CertNumber = "8552011", .DocProcess = "Certificate Services*", .DocType = "Correspondence", .EmployerId = "",
                                                            .FileDate = New Date(2017, 1, 1), .FileName = "18093SE2P279418-2.txt", .SE2ObjectId = "18093SE2P279418",
                                                            .MimeType = "text/plain", .OnbaseDocId = 0, .ScanDate = New Date(2018, 4, 3), .SE2DocId = "20180403-F-41666-00",
                                                            .SSN = "", .TPAPolicyNo = "", .ZipFileName = "P-SE2-04-05-2018_10-00-12.zip"}
        Dim saga As New FileConversionImagingPolicy With {.Data = sagaData}

        Await saga.Handle(aEvent, _Context).ConfigureAwait(False)


        Assert.AreEqual(saga.Data.FileName, aEvent.FileName)
        Assert.AreEqual(saga.Data.FileOutputPath, aEvent.OutputFileLocation)


        Dim processedMessage As NeeviaFileConversionTimeout = _Context.SentMessages(0).Message



        Assert.AreEqual(saga.Data.FileName, processedMessage.FileName)
        Assert.AreEqual(1, processedMessage.TimeoutAttempts)


    End Function

    <TestMethod()> Public Async Function Se2FileSentToNeeviaFolderForConversionMessageValidFileExistsTiffTest() As Task

        Dim aEvent = New TestSe2FileSentToNeeviaFolderForConversion With {.FileName = "18093SE2P279418-2.pdf", .OutputFileLocation = "\\modern-woodmen\gdrive\MWA\Dev\CustomerService\CommunicationManagement\SE2FileImport\NeeviaOutFolder\18093SE2P279418-2.tif"}

        Dim sagaData As New FileConversionImagingPolicyData() With {.CertNumber = "8552011", .DocProcess = "Certificate Services*", .DocType = "Correspondence", .EmployerId = "",
                                                            .FileDate = New Date(2017, 1, 1), .FileName = "18093SE2P279418-2.pdf", .SE2ObjectId = "18093SE2P279418",
                                                            .MimeType = "text/plain", .OnbaseDocId = 0, .ScanDate = New Date(2018, 4, 3), .SE2DocId = "20180403-F-41666-00",
                                                            .SSN = "", .TPAPolicyNo = "", .ZipFileName = "P-SE2-04-05-2018_10-00-12.zip"}
        Dim saga As New FileConversionImagingPolicy With {.Data = sagaData}

        Await saga.Handle(aEvent, _Context).ConfigureAwait(False)


        Assert.AreEqual(saga.Data.FileName, aEvent.FileName)
        Assert.AreEqual(saga.Data.FileOutputPath, aEvent.OutputFileLocation)


        Dim processedMessage As ImageConvertedSe2FileIntoOnBase = _Context.SentMessages(0).Message



        Assert.AreEqual(saga.Data.DocProcess, processedMessage.DocProcess)
        Assert.AreEqual(saga.Data.CertNumber, processedMessage.CertNumber)
        Assert.AreEqual(saga.Data.DocType, processedMessage.DocType)
        Assert.AreEqual(saga.Data.EmployerId, processedMessage.EmployerId)
        Assert.AreEqual(saga.Data.FileDate, processedMessage.FileDate)
        Assert.AreEqual(saga.Data.FileName, processedMessage.FileName)
        Assert.AreEqual(saga.Data.FileOutputPath, processedMessage.FilePath)
        Assert.AreEqual(saga.Data.MimeType, processedMessage.MimeType)
        Assert.AreEqual(saga.Data.OnbaseDocId, processedMessage.OnbaseDocId)
        Assert.AreEqual(saga.Data.ScanDate, processedMessage.ScanDate)
        Assert.AreEqual(saga.Data.SE2DocId, processedMessage.SE2DocId)
        Assert.AreEqual(saga.Data.SE2ObjectId, processedMessage.SE2ObjectId)
        Assert.AreEqual(saga.Data.SSN, processedMessage.SSN)
        Assert.AreEqual(saga.Data.TPAPolicyNo, processedMessage.TPAPolicyNo)
        Assert.AreEqual(saga.Data.ZipFileName, processedMessage.ZipFileName)

        Assert.IsTrue(saga.Completed)


    End Function



    <TestMethod()> Public Async Function NeeviaFileConversionTimeoutFileExistsTest() As Task

        Dim message = New NeeviaFileConversionTimeout With {.FileName = "18093SE2P279418-2.pdf", .TimeoutAttempts = 1}

        Dim sagaData As New FileConversionImagingPolicyData() With {.CertNumber = "8552011", .DocProcess = "Certificate Services*", .DocType = "Correspondence", .EmployerId = "",
                                                            .FileDate = New Date(2017, 1, 1), .FileName = "18093SE2P279418-2.pdf", .SE2ObjectId = "18093SE2P279418",
                                                            .MimeType = "text/plain", .OnbaseDocId = 0, .ScanDate = New Date(2018, 4, 3), .SE2DocId = "20180403-F-41666-00",
                                                            .SSN = "", .TPAPolicyNo = "", .ZipFileName = "P-SE2-04-05-2018_10-00-12.zip", .FileOutputPath = "\\modern-woodmen\gdrive\MWA\Dev\CustomerService\CommunicationManagement\SE2FileImport\NeeviaOutFolder\18093SE2P279418-2.tif"}
        Dim saga As New FileConversionImagingPolicy With {.Data = sagaData}

        Await saga.Timeout(message, _Context).ConfigureAwait(False)


        Assert.AreEqual(saga.Data.FileName, message.FileName)


        Dim processedMessage As ImageConvertedSe2FileIntoOnBase = _Context.SentMessages(0).Message



        Assert.AreEqual(saga.Data.DocProcess, processedMessage.DocProcess)
        Assert.AreEqual(saga.Data.CertNumber, processedMessage.CertNumber)
        Assert.AreEqual(saga.Data.DocType, processedMessage.DocType)
        Assert.AreEqual(saga.Data.EmployerId, processedMessage.EmployerId)
        Assert.AreEqual(saga.Data.FileDate, processedMessage.FileDate)
        Assert.AreEqual(saga.Data.FileName, processedMessage.FileName)
        Assert.AreEqual(saga.Data.FileOutputPath, processedMessage.FilePath)
        Assert.AreEqual(saga.Data.MimeType, processedMessage.MimeType)
        Assert.AreEqual(saga.Data.OnbaseDocId, processedMessage.OnbaseDocId)
        Assert.AreEqual(saga.Data.ScanDate, processedMessage.ScanDate)
        Assert.AreEqual(saga.Data.SE2DocId, processedMessage.SE2DocId)
        Assert.AreEqual(saga.Data.SE2ObjectId, processedMessage.SE2ObjectId)
        Assert.AreEqual(saga.Data.SSN, processedMessage.SSN)
        Assert.AreEqual(saga.Data.TPAPolicyNo, processedMessage.TPAPolicyNo)
        Assert.AreEqual(saga.Data.ZipFileName, processedMessage.ZipFileName)

        Assert.IsTrue(saga.Completed)


    End Function

    <TestMethod()> Public Async Function NeeviaFileConversionTimeoutNoFileExistsTest() As Task

        Dim message = New NeeviaFileConversionTimeout With {.FileName = "18093SE2P279418-2.pdf", .TimeoutAttempts = 1}

        Dim sagaData As New FileConversionImagingPolicyData() With {.CertNumber = "8552011", .DocProcess = "Certificate Services*", .DocType = "Correspondence", .EmployerId = "",
                                                            .FileDate = New Date(2017, 1, 1), .FileName = "18093SE2P279418-2.pdf", .SE2ObjectId = "18093SE2P279418",
                                                            .MimeType = "text/plain", .OnbaseDocId = 0, .ScanDate = New Date(2018, 4, 3), .SE2DocId = "20180403-F-41666-00",
                                                            .SSN = "", .TPAPolicyNo = "", .ZipFileName = "P-SE2-04-05-2018_10-00-12.zip", .FileOutputPath = "\\modern-woodmen\gdrive\MWA\Dev\CustomerService\CommunicationManagement\SE2FileImport\NeeviaOutFolder\18093SE2P279418-3.tif"}
        Dim saga As New FileConversionImagingPolicy With {.Data = sagaData}

        Await saga.Timeout(message, _Context).ConfigureAwait(False)


        Assert.AreEqual(saga.Data.FileName, message.FileName)


        Dim processedMessage As NeeviaFileConversionTimeout = _Context.SentMessages(0).Message


        Assert.AreEqual(2, processedMessage.TimeoutAttempts)
        Assert.AreEqual(saga.Data.FileName, processedMessage.FileName)


    End Function

    <TestMethod()> Public Async Function NeeviaFileConversionTimeoutErrorFolderTest() As Task

        Dim message = New NeeviaFileConversionTimeout With {.FileName = "18093SE2P279418-4.pdf", .TimeoutAttempts = 25}

        Dim sagaData As New FileConversionImagingPolicyData() With {.CertNumber = "8552011", .DocProcess = "Certificate Services*", .DocType = "Correspondence", .EmployerId = "",
                                                            .FileDate = New Date(2017, 1, 1), .FileName = "18093SE2P279418-4.pdf", .SE2ObjectId = "18093SE2P279418",
                                                            .MimeType = "text/plain", .OnbaseDocId = 0, .ScanDate = New Date(2018, 4, 3), .SE2DocId = "20180403-F-41666-00",
                                                            .SSN = "", .TPAPolicyNo = "", .ZipFileName = "P-SE2-04-05-2018_10-00-12.zip", .FileOutputPath = "\\modern-woodmen\gdrive\MWA\Dev\CustomerService\CommunicationManagement\SE2FileImport\NeeviaOutFolder\P-SE2-04-05-2018_10-00-12\18093SE2P279418-4.tif"}
        Dim saga As New FileConversionImagingPolicy With {.Data = sagaData}

        Await saga.Timeout(message, _Context).ConfigureAwait(False)


        Assert.AreEqual(saga.Data.FileName, message.FileName)


        Dim processedMessage As Se2FileConversionToTiffFailed = _Context.PublishedMessages(0).Message


        Assert.AreEqual("Neevia failed to convert the document", processedMessage.Ex)
        Assert.AreEqual(saga.Data.FileDate, processedMessage.FileDate)
        Assert.AreEqual(saga.Data.FileName, processedMessage.FileName)
        Assert.AreEqual(saga.Data.ZipFileName, processedMessage.ZipFileName)
        Assert.IsTrue(saga.Completed)

    End Function

    <TestMethod()> Public Async Function NeeviaFileConversionTimeoutNoFileExistsHittingTimeoutLimitTest() As Task

        Dim message = New NeeviaFileConversionTimeout With {.FileName = "18093SE2P279418-2.pdf", .TimeoutAttempts = 32}

        Dim sagaData As New FileConversionImagingPolicyData() With {.CertNumber = "8552011", .DocProcess = "Certificate Services*", .DocType = "Correspondence", .EmployerId = "",
                                                            .FileDate = New Date(2017, 1, 1), .FileName = "18093SE2P279418-2.pdf", .SE2ObjectId = "18093SE2P279418",
                                                            .MimeType = "text/plain", .OnbaseDocId = 0, .ScanDate = New Date(2018, 4, 3), .SE2DocId = "20180403-F-41666-00",
                                                            .SSN = "", .TPAPolicyNo = "", .ZipFileName = "P-SE2-04-05-2018_10-00-12.zip", .FileOutputPath = "\\modern-woodmen\gdrive\MWA\Dev\CustomerService\CommunicationManagement\SE2FileImport\NeeviaOutFolder\P-SE2-04-05-2018_10-00-12\18093SE2P279418-3.tif"}
        Dim saga As New FileConversionImagingPolicy With {.Data = sagaData}

        Await saga.Timeout(message, _Context).ConfigureAwait(False)


        Assert.AreEqual(saga.Data.FileName, message.FileName)


        Dim processedMessage As Se2FileConversionToTiffFailed = _Context.PublishedMessages(0).Message


        Assert.AreEqual("File failed to conversion after 30 minutes", processedMessage.Ex)
        Assert.AreEqual(saga.Data.FileDate, processedMessage.FileDate)
        Assert.AreEqual(saga.Data.FileName, processedMessage.FileName)
        Assert.AreEqual(saga.Data.ZipFileName, processedMessage.ZipFileName)
        Assert.IsTrue(saga.Completed)

    End Function


    Private Class TestSe2FileSentToNeeviaFolderForConversion : Implements Se2FileSentToNeeviaFolderForConversion

        Public Property FileName As String Implements Se2FileSentToNeeviaFolderForConversion.FileName


        Public Property OutputFileLocation As String Implements Se2FileSentToNeeviaFolderForConversion.OutputFileLocation

        Public Property ZipFileName As String Implements Se2FileSentToNeeviaFolderForConversion.ZipFileName

    End Class


End Class