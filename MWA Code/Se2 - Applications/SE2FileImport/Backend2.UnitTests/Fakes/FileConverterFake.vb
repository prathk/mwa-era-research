﻿Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain

Public Class FileConverterFake : Implements IFileConverter

    Public Function ConvertFileToTiffAndReturnPathToNewFile(filePath As String, mimeType As String, fileName As String, zipFileName As String) As String Implements IFileConverter.ConvertFileToTiffAndReturnPathToNewFile

        Dim extension = My.Computer.FileSystem.GetFileInfo(filePath).Extension

        Return System.IO.Directory.GetCurrentDirectory().Replace("\bin\Debug", "") & "\TestFile\" & zipFileName & "\" & fileName.Replace(extension, ".tif")
    End Function


End Class
