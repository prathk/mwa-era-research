﻿Public Interface SE2FileImaged

    Property ZipFileName As String
    Property Timestamp As DateTime
    Property ImagedFileName As String

End Interface
