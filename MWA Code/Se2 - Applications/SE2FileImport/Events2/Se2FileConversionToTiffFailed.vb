﻿Public Interface Se2FileConversionToTiffFailed

    Property FileName As String
    Property ZipFileName As String
    Property ErrorDateTime As DateTime
    Property FileDate As DateTime
    Property Ex As String

End Interface
