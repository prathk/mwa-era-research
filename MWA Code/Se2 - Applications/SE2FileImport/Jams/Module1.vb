﻿Imports System.IO
Imports mwa.CustomerService.CommunicationManagement.SE2FileImport.Commands
Imports NServiceBus

Module Module1

    Sub Main()
        Dim fileRepository As IFileRepository = New FileRepository


        Dim filePath As String = System.Configuration.ConfigurationManager.AppSettings("PendingDirectoryPath")

        Console.WriteLine(filePath)

        Dim dInfo As New DirectoryInfo(filePath)
        Dim files = dInfo.GetFiles()

        If (files IsNot Nothing) Then
            Console.WriteLine(files.Count)
        End If


        Dim sendCo As New SendCommand
        sendCo.SendCommand(files).GetAwaiter().GetResult()
        Console.WriteLine("Finished Processing")
    End Sub

End Module


Public Class SendCommand

    Public Async Function SendCommand(files As FileInfo()) As Task
        Console.WriteLine("Getting the Bus")
        Dim context = BusConfig.GetBus()
        Console.WriteLine("Before looping through files to process")
        For Each fi In files
            Console.WriteLine("Process file: " & fi.Name)
            Dim command = New ProcessZippedFileReceivedFromSE2() With {.ZipFileName = fi.Name, .Timestamp = DateTime.Now}
            Await context.Send(command)
        Next
    End Function


End Class
