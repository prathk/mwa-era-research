﻿Imports mwa.ITOps.Monitoring.CustomChecks.WebService

Public Class SE2FileImportImagingWSMonitor : Inherits WebServiceCheck


    Public Sub New(imgWs As ImagingWS.ImagingWS)
        MyBase.New(String.Format("SE2FileImport Imaging Web Service Monitor"), ConfigurationManager.AppSettings("ImagingWSURL"), TimeSpan.FromMinutes(30))

    End Sub



End Class