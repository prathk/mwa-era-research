﻿Imports mwa.ITOps.Monitoring.CustomChecks.FileSystem

Public Class SE2FileImportFileWriteLocationMonitor : Inherits FileSystemAccessCheck

    Protected Overrides Property DirectoryAccessControlList As List(Of DirectoryAccessControlEntry)
    Public Sub New()
        MyBase.New("SE2FileImport File Directory Monitor", TimeSpan.FromHours(1))

        LoadDirectoryControlList()
    End Sub


    Public Sub LoadDirectoryControlList()

        Dim se2DirectoryPath = ConfigurationManager.AppSettings("FolderPath")
        Dim fileShareGroup = ConfigurationManager.AppSettings("FileShareGroupName")
        DirectoryAccessControlList = New List(Of DirectoryAccessControlEntry)

        'InProcessFolder
        Dim inProcessFolderPath = ConfigurationManager.AppSettings("InProcessFolderPath")
        DirectoryAccessControlList.Add(New DirectoryAccessControlEntry With {.FolderPath = inProcessFolderPath, .UserOrGroupName = fileShareGroup, .Permission = System.Security.AccessControl.FileSystemRights.Read})
        DirectoryAccessControlList.Add(New DirectoryAccessControlEntry With {.FolderPath = inProcessFolderPath, .UserOrGroupName = fileShareGroup, .Permission = System.Security.AccessControl.FileSystemRights.CreateFiles})
        
        'Archive folder
        Dim archiveFolderPath = se2DirectoryPath & "\Archive"
        DirectoryAccessControlList.Add(New DirectoryAccessControlEntry With {.FolderPath = archiveFolderPath, .UserOrGroupName = fileShareGroup, .Permission = System.Security.AccessControl.FileSystemRights.Read})
        DirectoryAccessControlList.Add(New DirectoryAccessControlEntry With {.FolderPath = archiveFolderPath, .UserOrGroupName = fileShareGroup, .Permission = System.Security.AccessControl.FileSystemRights.CreateFiles})
        
        'Folder
        Dim fileErrorFolderPath = se2DirectoryPath & "\FileError"
        DirectoryAccessControlList.Add(New DirectoryAccessControlEntry With {.FolderPath = fileErrorFolderPath, .UserOrGroupName = fileShareGroup, .Permission = System.Security.AccessControl.FileSystemRights.Read})
        DirectoryAccessControlList.Add(New DirectoryAccessControlEntry With {.FolderPath = fileErrorFolderPath, .UserOrGroupName = fileShareGroup, .Permission = System.Security.AccessControl.FileSystemRights.CreateFiles})

        'Folder
        Dim pendingFolderPath = se2DirectoryPath & "\Pending"
        DirectoryAccessControlList.Add(New DirectoryAccessControlEntry With {.FolderPath = pendingFolderPath, .UserOrGroupName = fileShareGroup, .Permission = System.Security.AccessControl.FileSystemRights.Read})
        DirectoryAccessControlList.Add(New DirectoryAccessControlEntry With {.FolderPath = pendingFolderPath, .UserOrGroupName = fileShareGroup, .Permission = System.Security.AccessControl.FileSystemRights.CreateFiles})


    End Sub



End Class