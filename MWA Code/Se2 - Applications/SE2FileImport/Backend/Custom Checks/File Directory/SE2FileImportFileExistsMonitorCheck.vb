﻿Imports NServiceBus.CustomChecks
Imports System.IO


Public Class SE2FileImportFileExistsMonitorCheck : Inherits CustomCheck

    Public Sub New()
        MyBase.New("SE2 File Exists Monitor", "SE2 File Import", TimeSpan.FromMinutes(30))
    End Sub

    Public Overrides Function PerformCheck() As Task(Of CheckResult)

        If (DateTime.Now < New Date(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 1, 0, 0) Or DateTime.Now > New Date(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 7, 0, 0)) Then
            Dim se2Folder = ConfigurationManager.AppSettings("FolderPath") & "\Pending"

            Dim files = Directory.GetFiles(se2Folder)

            If (files.Count > 0) Then
                'Log.Logger.Error("There is a pending SE2 File that hasn't been ran. :  " & se2Folder)
                Return CheckResult.Failed("There is a pending SE2 File that hasn't been ran. :  " & se2Folder)
            End If

        End If

        Return CheckResult.Pass

    End Function

End Class