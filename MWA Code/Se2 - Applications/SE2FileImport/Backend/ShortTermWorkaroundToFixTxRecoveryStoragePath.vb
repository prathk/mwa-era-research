﻿Public Class ShortTermWorkaroundToFixTxRecoveryStoragePath : Implements IWantToRunWhenConfigurationIsComplete

    Public Sub Run1(config As Configure) Implements IWantToRunWhenConfigurationIsComplete.Run
        Dim docstore = TryCast(config.Settings.GetOrDefault(Of IDocumentStore)("RavenDbDocumentStore"), DocumentStore)
        If docstore IsNot Nothing Then
            Dim transactionRecoveryPath = System.Configuration.ConfigurationManager.AppSettings("RavenDBTransactionRecoveryStorageLocation")
            docstore.TransactionRecoveryStorage = New LocalDirectoryTransactionRecoveryStorage(transactionRecoveryPath)
        End If
    End Sub

End Class
