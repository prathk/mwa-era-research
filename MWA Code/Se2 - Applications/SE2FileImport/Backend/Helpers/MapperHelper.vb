﻿Imports AutoMapper
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Events

Public Class MapperHelper

    Public Sub New()

        Mapper.CreateMap(Of ProcessZippedFileReceivedFromSE2, ProcessSE2File)().IgnoreAllPropertiesWithAnInaccessibleSetter().IgnoreAllSourcePropertiesWithAnInaccessibleSetter()
        Mapper.CreateMap(Of ProcessSE2File, ProcessSe2FileXml)().IgnoreAllPropertiesWithAnInaccessibleSetter().IgnoreAllSourcePropertiesWithAnInaccessibleSetter()

    End Sub



End Class
