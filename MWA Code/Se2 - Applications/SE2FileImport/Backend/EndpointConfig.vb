﻿
Imports NServiceBus
Imports mwa.OnBase
Imports mwa.DataAccess.Common
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain
Imports mwa.ITOps.SoftwareDevelopment.NServiceBusExtensions

Public Class EndpointConfig : Implements IConfigureThisEndpoint


    Public Sub New()
        LoggingConfiguration.ConfigureLogDirectory(System.Configuration.ConfigurationManager.AppSettings("LogFilePathFolder"))
    End Sub


    Public Sub Customize(configuration As EndpointConfiguration) Implements IConfigureThisEndpoint.Customize

        SqlPersistence.ConfigureSqlPersistence(configuration, ConfigurationManager.ConnectionStrings("SqlPersistenceConnectionString").ConnectionString)

        configuration.SendHeartbeatTo(System.Configuration.ConfigurationManager.AppSettings("ServiceControl/Queue"), TimeSpan.FromSeconds(30))
        ConfigureUnobtrusiveMessageConventions(configuration)


        configuration.RegisterComponents(Sub(r) r.ConfigureComponent(Of ISE2FileRepository)(
                                               Function()
                                                   Return New SE2FileRepository()
                                               End Function, DependencyLifecycle.InstancePerCall)
                                        )


        configuration.RegisterComponents(Sub(r) r.ConfigureComponent(Of IAcquisitionsRepository)(
                                      Function()
                                          Return New AcquisitionRepository(New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString))
                                      End Function, DependencyLifecycle.InstancePerCall)
                               )

        configuration.RegisterComponents(Sub(r) r.ConfigureComponent(Of IZipService)(
                                      Function()
                                          Return New ZipFolderFactory()
                                      End Function, DependencyLifecycle.InstancePerCall)
                               )


        configuration.RegisterComponents(Sub(r) r.ConfigureComponent(Of ISE2XmlParser)(
                                      Function()
                                          Return New Se2XmlFileParser(New AcquisitionRepository(New ConnectionStringFactory(ConfigurationManager.ConnectionStrings("ImagingDBConnectionString").ConnectionString)))
                                      End Function, DependencyLifecycle.InstancePerCall)
                               )

        configuration.RegisterComponents(Sub(r) r.ConfigureComponent(Of MapperHelper)(Function()
                                                                                          Return New MapperHelper()
                                                                                      End Function, DependencyLifecycle.InstancePerCall))

        configuration.RegisterComponents(Sub(r) r.ConfigureComponent(Of ImagingWS.ImagingWS)(Function()
                                                                                                 Return New ImagingWS.ImagingWS() With {.Url = ConfigurationManager.AppSettings("ImagingWSUrl")}
                                                                                             End Function, DependencyLifecycle.InstancePerCall))


    End Sub
End Class
