﻿Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports NServiceBus
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain

Public Class UnzipSe2FileHandler : Implements IHandleMessages(Of UnzipSe2File)


    Private Se2FileRepository As ISE2FileRepository
    Private ZipFactory As IZipService
    Private Se2XmlFileParser As ISE2XmlParser
    Public Sub New(aSe2FileRepository As ISE2FileRepository,
                   aZipFactory As IZipService,
                   aSe2XmlFileParser As ISE2XmlParser)
        Se2FileRepository = aSe2FileRepository
        ZipFactory = aZipFactory
        Se2XmlFileParser = aSe2XmlFileParser

    End Sub
    Public Async Function Handle(message As UnzipSe2File, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of UnzipSe2File).Handle
        Dim archiveFolderPath = Se2FileRepository.GetFolderPath("Archive", message.ZipFileName)
        Dim unZipFolderPath = Se2FileRepository.GetFolderPath("InProcess", message.ZipFileName.Replace(".zip", ""))


        ZipFactory.UnZip(archiveFolderPath, unZipFolderPath)

        Await context.Send(New ProcessSe2FileXml With {.TimeStamp = DateTime.Now, .ZipFileName = message.ZipFileName})
    End Function
End Class
