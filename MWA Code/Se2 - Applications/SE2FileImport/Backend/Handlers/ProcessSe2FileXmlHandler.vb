﻿Imports NServiceBus
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Events
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain

Public Class ProcessSe2FileXmlHandler
    Implements IHandleMessages(Of ProcessSe2FileXml)



    Private Se2FileRepository As ISE2FileRepository
    Private ZipFactory As IZipService
    Private Se2XmlFileParser As ISE2XmlParser

    Public Sub New(aSe2FileRepository As ISE2FileRepository,
               aZipFactory As IZipService,
               aSe2XmlFileParser As ISE2XmlParser)
        Se2FileRepository = aSe2FileRepository
        ZipFactory = aZipFactory
        Se2XmlFileParser = aSe2XmlFileParser
    End Sub


    Public Async Function Handle(message As ProcessSe2FileXml, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of ProcessSe2FileXml).Handle
        Dim unZipFolderPath = Se2FileRepository.GetFolderPath("InProcess", message.ZipFileName.Replace(".zip", ""))

        Dim commandList = Se2XmlFileParser.ParseSe2XmlToListOfImageFileAndSendToOnbase(unZipFolderPath & "\" & message.ZipFileName.Replace(".zip", ".xml"), message.ZipFileName)

        Await context.Publish(Of SE2FileCount)(Function(r)
                                                   r.ZipFileName = message.ZipFileName
                                                   r.FileCount = commandList.Count
                                                   r.Timestamp = DateTime.Now
                                                   Return r
                                               End Function).ConfigureAwait(False)


        For Each aCommand In commandList
            Await context.Send("mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2", aCommand).ConfigureAwait(False)
        Next

    End Function
End Class
