﻿Imports NServiceBus
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain


Public Class ProcessZippedFileReceivedFromSE2Handler
    Implements IHandleMessages(Of ProcessZippedFileReceivedFromSE2)

    Private Se2FileRepository As ISE2FileRepository
    Private ZipFactory As IZipService
    Private Se2XmlFileParser As ISE2XmlParser

    Public Sub New(aSe2FileRepository As ISE2FileRepository,
                   aZipFactory As IZipService,
                   aSe2XmlFileParser As ISE2XmlParser)

        Se2FileRepository = aSe2FileRepository
        ZipFactory = aZipFactory
        Se2XmlFileParser = aSe2XmlFileParser
    End Sub

    Public Async Function Handle(message As ProcessZippedFileReceivedFromSE2, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of ProcessZippedFileReceivedFromSE2).Handle
        Dim pendingFolderPath = Se2FileRepository.GetFolderPath("Pending", message.ZipFileName)

        Dim archiveFolderPath = Se2FileRepository.GetFolderPath("Archive", message.ZipFileName)

        ''Copy Zip folder to Archive
        Se2FileRepository.CopyFile(pendingFolderPath, archiveFolderPath)

        If (My.Computer.FileSystem.FileExists(archiveFolderPath)) Then
            Se2FileRepository.DeleteFile(pendingFolderPath)
            Dim m As New ProcessSE2File With {.TimeStamp = message.Timestamp, .ZipFileName = message.ZipFileName}
            Await context.Send(m).ConfigureAwait(False)
        Else
            Throw New ArgumentNullException()
        End If

    End Function
End Class