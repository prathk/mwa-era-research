﻿Imports NServiceBus
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Events
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain

Public Class SE2ZipFileProcessedHandler : Implements IHandleMessages(Of SE2ZipFileProcessed)

    Private Se2FileRepository As ISE2FileRepository

    Public Sub New(aSe2FileRepository As ISE2FileRepository)
        Se2FileRepository = aSe2FileRepository
    End Sub


    Public Function Handle(message As SE2ZipFileProcessed, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of SE2ZipFileProcessed).Handle
        Dim inProcessFolderPath = Se2FileRepository.GetFolderPath("InProcess", message.FileName.Replace(".zip", ""))

        If (IO.Directory.Exists(inProcessFolderPath)) Then
            Se2FileRepository.DeleteDirectory(inProcessFolderPath)
        End If

        Dim neeviaError = Se2FileRepository.GetFolderPath("NeeviaErrorFolder", message.FileName.Replace(".zip", ""))
        If (IO.Directory.Exists(neeviaError)) Then
            Se2FileRepository.DeleteDirectory(neeviaError)
        End If

        Dim neeviaIn = Se2FileRepository.GetFolderPath("NeeviaInFolder", message.FileName.Replace(".zip", ""))
        If (IO.Directory.Exists(neeviaIn)) Then
            Se2FileRepository.DeleteDirectory(neeviaIn)
        End If

        Dim neeviaOut = Se2FileRepository.GetFolderPath("NeeviaOutFolder", message.FileName.Replace(".zip", ""))
        If (IO.Directory.Exists(neeviaOut)) Then
            Se2FileRepository.DeleteDirectory(neeviaOut)
        End If

        Return Task.CompletedTask
    End Function
End Class
