﻿Option Strict Off

Imports NServiceBus
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Events
Imports System.Net.Mail
Imports System.Text
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Domain
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports System.IO


Public Class SendSe2FileEmailReportHandler : Implements IHandleMessages(Of SendSe2FileEmailReport)
    Private Se2FileRepository As ISE2FileRepository
    Private Se2XmlFileParser As ISE2XmlParser


    Public Sub New(aSe2FileRepository As ISE2FileRepository, aSe2XmlFileParser As ISE2XmlParser)
        Me.Se2FileRepository = aSe2FileRepository
        Me.Se2XmlFileParser = aSe2XmlFileParser
    End Sub

    Public Async Function Handle(message As SendSe2FileEmailReport, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of SendSe2FileEmailReport).Handle
        If (message.Errors IsNot Nothing) Then
            If message.Errors.Count > 0 Then

                Dim fileErrorPath As String = ConfigurationManager.AppSettings("FolderPath") & "\FileError\" & message.ZipFileName.Replace(".zip", "") & "\"

                Dim inProcessPath = Se2FileRepository.GetFolderPath("InProcess", message.ZipFileName.Replace(".zip", ""))
                Dim commandList = Se2XmlFileParser.ParseSe2XmlToListOfImageFileAndSendToOnbase(inProcessPath & "\" & message.ZipFileName.Replace(".zip", ".xml"), message.ZipFileName)

                If (Not Directory.Exists(fileErrorPath)) Then
                    Directory.CreateDirectory(fileErrorPath)
                End If


                Se2FileRepository.CopyFile(inProcessPath & "\" & message.ZipFileName.Replace(".zip", ".xml"), fileErrorPath & message.ZipFileName.Replace(".zip", ".xml"))

                Dim attachmentContent As New StringBuilder()
                attachmentContent.Append(GetCsvHeader())
                Using ms As New MemoryStream
                    Using sw As New StreamWriter(ms)
                        sw.WriteLine(GetCsvHeader().ToString)

                        For Each item In message.Errors

                            'Dim errorFolder = ConfigurationManager.AppSettings("FolderPath") & "\FileError\" & item.Key

                            'Get the Keywords for this file to include in email
                            Dim fileKeywords = commandList.Where(Function(r) r.FileName = item.Key).FirstOrDefault

                            'Copy the file into the error folder for processing later
                            Dim filePath = Se2FileRepository.GetFilePath(fileKeywords.SE2ObjectId, item.Key, message.ZipFileName.Replace(".zip", ""))

                            sw.WriteLine(GetCsvBody(message, item.Key, fileKeywords, item.Value, fileErrorPath))


                            Se2FileRepository.CopyFile(filePath, fileErrorPath & item.Key)
                        Next

                        'Writing xml file to ZipFileName folder
                        sw.Flush()
                        ms.Position = 0


                        Using file As New FileStream(fileErrorPath & "Exceptions.csv", FileMode.Create, FileAccess.Write)
                            ms.WriteTo(file)
                            ''file.Close()
                        End Using

                        Dim mailMessage As Net.Mail.MailMessage = New Net.Mail.MailMessage(ConfigurationManager.AppSettings("FileConversionErrorEmailFrom"), ConfigurationManager.AppSettings("FileConversionErrorEmailTo"))
                        Dim smtp As New SmtpClient(ConfigurationManager.AppSettings("SmtpServer"))
                        mailMessage.CC.Add(ConfigurationManager.AppSettings("FileConversionErrorEmailCC"))
                        mailMessage.Subject = ConfigurationManager.AppSettings("FileConversionErrorEmailSubject")
                        mailMessage.Body = "Please find attachment for exceptions"


                        Dim attachment As New Attachment(ms, "Exceptions.csv", "text/csv")
                        attachment.Name = "Exceptions.csv"
                        mailMessage.Attachments.Add(attachment)
                        smtp.Send(mailMessage)
                    End Using


                End Using

            End If

        End If
        'Increment Saga File count so it can close
        Await context.Publish(Of Se2FileEmailReportSent)(Function(r)
                                                             r.CurrentDateTIme = DateTime.Now
                                                             r.ZipFileName = message.ZipFileName
                                                             Return r
                                                         End Function)
    End Function

    Private Function GetCsvHeader() As String
        Dim sb As New StringBuilder
        sb.Append("File Name ,")
        sb.Append("File Error Location ,")
        sb.Append("File Error Message ,")
        sb.Append("Error DateTime ,")
        sb.Append("Orig Zip File Name ,")
        sb.Append("Certificate Number ,")
        sb.Append("Doc Process ,")
        sb.Append("Doc Type ,")
        sb.Append("Employer Id ,")
        sb.Append("Document Date ,")
        sb.Append("Mime Type ,")
        sb.Append("Onbase Doc Id ,")
        sb.Append("Scan Date ,")
        sb.Append("SE2 Doc Id ,")
        sb.Append("SE2 Object Id ,")
        sb.Append("SSN ,")
        sb.Append("TPA Policy No")

        Return sb.ToString
    End Function
    Private Function GetCsvBody(message As SendSe2FileEmailReport, fileName As String, fileKeywords As Commands.ImageFileAndSendToOnbase, errorMsg As String, fileErrorLocation As String) As String



        Dim sb As New StringBuilder
        sb.Append(fileName & ",")
        sb.Append(fileErrorLocation & ",")
        sb.Append(errorMsg & ",")
        sb.Append(message.DateCompleted & ",")
        sb.Append(message.ZipFileName & ",")
        sb.Append(fileKeywords.CertNumber & ",")
        sb.Append(fileKeywords.DocProcess & ",")
        sb.Append(fileKeywords.DocType & ",")
        sb.Append(fileKeywords.EmployerId & ",")
        sb.Append(fileKeywords.FileDate & ",")
        sb.Append(fileKeywords.MimeType & ",")
        sb.Append(fileKeywords.OnbaseDocId & ",")
        sb.Append(fileKeywords.ScanDate & ",")
        sb.Append(fileKeywords.SE2DocId & ",")
        sb.Append(fileKeywords.SE2ObjectId & ",")
        sb.Append(fileKeywords.SSN & ",")
        sb.Append(fileKeywords.TPAPolicyNo & ",")


        Return sb.ToString
    End Function

End Class
