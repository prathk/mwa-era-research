﻿
Imports NServiceBus
Imports NServiceBus.Persistence.Sql
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Commands
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Events
Imports mwa.CustomerService.CommunicationManagement.Se2FileImport.Backend2.Events


Public Class SE2FileProcessor : Inherits SqlSaga(Of SE2FileProcessorDatas)
    Implements IAmStartedByMessages(Of ProcessSE2File),
               IHandleMessages(Of SE2FileImaged),
               IHandleMessages(Of SE2FileCount),
               IHandleMessages(Of Se2FileConversionToTiffFailed),
               IHandleMessages(Of Se2FileEmailReportSent)

    Protected Overrides ReadOnly Property CorrelationPropertyName As String
        Get
            Return NameOf(SE2FileProcessorDatas.ZipFileName)
        End Get
    End Property

    Protected Overrides Sub ConfigureMapping(mapper As IMessagePropertyMapper)
        mapper.ConfigureMapping(Of ProcessSE2File)(Function(r) r.ZipFileName)
        mapper.ConfigureMapping(Of SE2FileCount)(Function(r) r.ZipFileName)
        mapper.ConfigureMapping(Of SE2FileImaged)(Function(r) r.ZipFileName)
        mapper.ConfigureMapping(Of Se2FileConversionToTiffFailed)(Function(r) r.ZipFileName)
        mapper.ConfigureMapping(Of Se2FileEmailReportSent)(Function(r) r.ZipFileName)
    End Sub

    Public Async Function Handle(message As ProcessSE2File, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of ProcessSE2File).Handle
        If (Me.Data.HasStarted = False) Then
            Me.Data.HasStarted = True
            Me.Data.ZipFileName = message.ZipFileName

            Dim m As New UnzipSe2File With
                {.TimeStamp = message.TimeStamp,
                 .ZipFileName = message.ZipFileName
                }

            Await context.Send(m).ConfigureAwait(False)
        End If
    End Function

    Public Async Function Handle(message As SE2FileImaged, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of SE2FileImaged).Handle
        Me.Data.FilesCompletedCount += 1

        If (Me.Data.FilesCompletedCount = Me.Data.FileCount) Then
            Await context.Send(Of SendSe2FileEmailReport)(Function(r)
                                                              r.ZipFileName = Me.Data.ZipFileName
                                                              r.Errors = Me.Data.Errors
                                                              r.FilesCompletedCount = Me.Data.FilesCompletedCount
                                                              r.DateCompleted = DateTime.Now
                                                              Return r
                                                          End Function).ConfigureAwait(False)
        End If
    End Function

    Public Function Handle(message As SE2FileCount, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of SE2FileCount).Handle
        Me.Data.FileCount = message.FileCount
        Return Task.CompletedTask()
    End Function

    Public Async Function Handle(message As Se2FileConversionToTiffFailed, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of Se2FileConversionToTiffFailed).Handle
        Me.Data.FilesCompletedCount += 1

        If (message.Ex IsNot Nothing) Then
            Me.Data.Errors.Add(message.FileName, message.Ex)
        End If

        If (Me.Data.FilesCompletedCount = Me.Data.FileCount) Then
            Await context.Send(Of SendSe2FileEmailReport)(Function(r)
                                                              r.ZipFileName = Me.Data.ZipFileName
                                                              r.Errors = Me.Data.Errors
                                                              r.FilesCompletedCount = Me.Data.FilesCompletedCount
                                                              r.DateCompleted = DateTime.Now
                                                              Return r
                                                          End Function).ConfigureAwait(False)
        End If
    End Function

    Public Async Function Handle(message As Se2FileEmailReportSent, context As IMessageHandlerContext) As Task Implements IHandleMessages(Of Se2FileEmailReportSent).Handle
        Await context.Publish(Of SE2ZipFileProcessed)(Function(r)
                                                          r.FileName = Me.Data.ZipFileName
                                                          r.Timestamp = DateTime.Now
                                                          Return r
                                                      End Function).ConfigureAwait(False)
        Me.MarkAsComplete()
    End Function

End Class


Public Class SE2FileProcessorDatas : Inherits ContainSagaData

    Public Property ZipFileName As String
    Public Property HasStarted As Boolean
    Public Property FileCount As Integer
    Public Property FilesCompletedCount As Integer
    Public Property Errors As New Dictionary(Of String, String)

End Class