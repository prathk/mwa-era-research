﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("mwa.CustomerService.CommunicationManagement.Se2FileImport")>
<Assembly: AssemblyDescription("Backend processing SE2 correspondence into OnBase")>
<Assembly: AssemblyCompany("Modern Woodmen of America")>
<Assembly: AssemblyProduct("mwa.CustomerService.CommunicationManagement.Se2FileImport")>
<Assembly: AssemblyCopyright("Copyright © Modern Woodmen of America 2015")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("e439a9a4-ba02-4ca9-be15-87d9f57fddfe")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
