﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSe2DocType = New System.Windows.Forms.TextBox()
        Me.txtSE2DocProcess = New System.Windows.Forms.TextBox()
        Me.lblSe2DocProcess = New System.Windows.Forms.Label()
        Me.txtMWADocType = New System.Windows.Forms.TextBox()
        Me.lblMwaDocType = New System.Windows.Forms.Label()
        Me.txtMWADocProcess = New System.Windows.Forms.TextBox()
        Me.lblMWADocProcess = New System.Windows.Forms.Label()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.lbEnvironment = New System.Windows.Forms.ListBox()
        Me.btnCreate = New System.Windows.Forms.Button()
        Me.lblErrorMessages = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(49, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Non Doc Type"
        '
        'txtSe2DocType
        '
        Me.txtSe2DocType.Location = New System.Drawing.Point(197, 55)
        Me.txtSe2DocType.Name = "txtSe2DocType"
        Me.txtSe2DocType.Size = New System.Drawing.Size(217, 20)
        Me.txtSe2DocType.TabIndex = 1
        '
        'txtSE2DocProcess
        '
        Me.txtSE2DocProcess.Location = New System.Drawing.Point(197, 93)
        Me.txtSE2DocProcess.Name = "txtSE2DocProcess"
        Me.txtSE2DocProcess.Size = New System.Drawing.Size(217, 20)
        Me.txtSE2DocProcess.TabIndex = 3
        '
        'lblSe2DocProcess
        '
        Me.lblSe2DocProcess.AutoSize = True
        Me.lblSe2DocProcess.Location = New System.Drawing.Point(49, 93)
        Me.lblSe2DocProcess.Name = "lblSe2DocProcess"
        Me.lblSe2DocProcess.Size = New System.Drawing.Size(94, 13)
        Me.lblSe2DocProcess.TabIndex = 2
        Me.lblSe2DocProcess.Text = "Non Doc Process:"
        '
        'txtMWADocType
        '
        Me.txtMWADocType.Location = New System.Drawing.Point(197, 129)
        Me.txtMWADocType.Name = "txtMWADocType"
        Me.txtMWADocType.Size = New System.Drawing.Size(217, 20)
        Me.txtMWADocType.TabIndex = 5
        '
        'lblMwaDocType
        '
        Me.lblMwaDocType.AutoSize = True
        Me.lblMwaDocType.Location = New System.Drawing.Point(49, 129)
        Me.lblMwaDocType.Name = "lblMwaDocType"
        Me.lblMwaDocType.Size = New System.Drawing.Size(87, 13)
        Me.lblMwaDocType.TabIndex = 4
        Me.lblMwaDocType.Text = "MWA Doc Type:"
        '
        'txtMWADocProcess
        '
        Me.txtMWADocProcess.Location = New System.Drawing.Point(197, 163)
        Me.txtMWADocProcess.Name = "txtMWADocProcess"
        Me.txtMWADocProcess.Size = New System.Drawing.Size(217, 20)
        Me.txtMWADocProcess.TabIndex = 7
        '
        'lblMWADocProcess
        '
        Me.lblMWADocProcess.AutoSize = True
        Me.lblMWADocProcess.Location = New System.Drawing.Point(49, 163)
        Me.lblMWADocProcess.Name = "lblMWADocProcess"
        Me.lblMWADocProcess.Size = New System.Drawing.Size(98, 13)
        Me.lblMWADocProcess.TabIndex = 6
        Me.lblMWADocProcess.Text = "MWA Doc Process"
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(197, 199)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(326, 65)
        Me.txtDescription.TabIndex = 9
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Location = New System.Drawing.Point(49, 199)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(63, 13)
        Me.lblDescription.TabIndex = 8
        Me.lblDescription.Text = "Description:"
        '
        'lbEnvironment
        '
        Me.lbEnvironment.FormattingEnabled = True
        Me.lbEnvironment.Items.AddRange(New Object() {"Development", "Staging / UAT", "Production"})
        Me.lbEnvironment.Location = New System.Drawing.Point(711, 87)
        Me.lbEnvironment.Name = "lbEnvironment"
        Me.lbEnvironment.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lbEnvironment.Size = New System.Drawing.Size(120, 95)
        Me.lbEnvironment.TabIndex = 10
        '
        'btnCreate
        '
        Me.btnCreate.Location = New System.Drawing.Point(447, 302)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(153, 48)
        Me.btnCreate.TabIndex = 11
        Me.btnCreate.Text = "Create Mapping"
        Me.btnCreate.UseVisualStyleBackColor = True
        '
        'lblErrorMessages
        '
        Me.lblErrorMessages.AutoSize = True
        Me.lblErrorMessages.ForeColor = System.Drawing.Color.DarkRed
        Me.lblErrorMessages.Location = New System.Drawing.Point(52, 302)
        Me.lblErrorMessages.Name = "lblErrorMessages"
        Me.lblErrorMessages.Size = New System.Drawing.Size(0, 13)
        Me.lblErrorMessages.TabIndex = 12
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 413)
        Me.Controls.Add(Me.lblErrorMessages)
        Me.Controls.Add(Me.btnCreate)
        Me.Controls.Add(Me.lbEnvironment)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.txtMWADocProcess)
        Me.Controls.Add(Me.lblMWADocProcess)
        Me.Controls.Add(Me.txtMWADocType)
        Me.Controls.Add(Me.lblMwaDocType)
        Me.Controls.Add(Me.txtSE2DocProcess)
        Me.Controls.Add(Me.lblSe2DocProcess)
        Me.Controls.Add(Me.txtSe2DocType)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSe2DocType As System.Windows.Forms.TextBox
    Friend WithEvents txtSE2DocProcess As System.Windows.Forms.TextBox
    Friend WithEvents lblSe2DocProcess As System.Windows.Forms.Label
    Friend WithEvents txtMWADocType As System.Windows.Forms.TextBox
    Friend WithEvents lblMwaDocType As System.Windows.Forms.Label
    Friend WithEvents txtMWADocProcess As System.Windows.Forms.TextBox
    Friend WithEvents lblMWADocProcess As System.Windows.Forms.Label
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lbEnvironment As System.Windows.Forms.ListBox
    Friend WithEvents btnCreate As System.Windows.Forms.Button
    Friend WithEvents lblErrorMessages As System.Windows.Forms.Label

End Class
