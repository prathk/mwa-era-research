﻿Imports mwa.DataAccess.Common
Imports mwa.DataAccess.Sql
Imports System.Data.Common

Public Class DocumentMappingIncomingRepository
    Implements IDocumentMappingIncoming

    Private ConnectionString As IConnectionStringFactory
    Public Sub New(aConnectionString As IConnectionStringFactory)

        Me.ConnectionString = aConnectionString

    End Sub

    Public Sub Add(aDocumentMappingIncoming As DocumentMappingIncoming) Implements IDocumentMappingIncoming.Add
        Using dac As New SqlDataAccess(Me.ConnectionString)
            Dim params As New List(Of DbParameter)
            params.Add(dac.ParameterFactory.Create("@MwaProcess", aDocumentMappingIncoming.MwaProcess, DbType.String, ParameterDirection.Input))
            params.Add(dac.ParameterFactory.Create("@MwaDocType", aDocumentMappingIncoming.MwaDocType, DbType.String, ParameterDirection.Input))
            params.Add(dac.ParameterFactory.Create("@NonMwaProcess", aDocumentMappingIncoming.NonMwaProcess, DbType.String, ParameterDirection.Input))
            params.Add(dac.ParameterFactory.Create("@NonMwaDocType", aDocumentMappingIncoming.NonMwaDocType, DbType.String, ParameterDirection.Input))
            params.Add(dac.ParameterFactory.Create("@Description", aDocumentMappingIncoming.Description, DbType.String, ParameterDirection.Input))
            params.Add(dac.ParameterFactory.Create("@CompanyId", aDocumentMappingIncoming.CompanyId, DbType.String, ParameterDirection.Input))
            params.Add(dac.ParameterFactory.Create("@Id", Nothing, DbType.Int32, ParameterDirection.Input))
            dac.ExecuteNonQuery("Acquisitions", "dbo", "DocumentMapping_Incoming_ins", params)
        End Using
    End Sub



End Class
