﻿Imports System.Text
Imports System.Configuration

Public Class Form1

    Private AcquisitionsRepo As IDocumentMappingIncoming

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click

        Dim hasErrors = ValidateInput()

        If Not (hasErrors) Then


            Dim documentMapping = New DocumentMappingIncoming With
                                  {.CompanyId = 1, _
                                   .Description = Me.txtDescription.Text, _
                                   .MwaDocType = Me.txtMWADocType.Text, _
                                   .MwaProcess = Me.txtMWADocProcess.Text, _
                                   .NonMwaDocType = Me.txtSe2DocType.Text, _
                                   .NonMwaProcess = Me.txtSE2DocProcess.Text}

            ''Insert into Database

            Dim messageList As New StringBuilder
            For Each env In lbEnvironment.SelectedItems

                Select Case env

                    Case "Development"
                        Dim connectionString = ConfigurationManager.ConnectionStrings("AcquisitionsDev").ConnectionString
                        Dim connectionStringFactory = New mwa.DataAccess.Common.ConnectionStringFactory(connectionString)

                        AcquisitionsRepo = New DocumentMappingIncomingRepository(connectionStringFactory)
                        AcquisitionsRepo.Add(documentMapping)
                        messageList.AppendLine("Development Database Insert Successful")
                    Case "Staging / UAT"
                        Dim connectionString = ConfigurationManager.ConnectionStrings("AcquisitionsUAT").ConnectionString
                        Dim connectionStringFactory = New mwa.DataAccess.Common.ConnectionStringFactory(connectionString)

                        AcquisitionsRepo = New DocumentMappingIncomingRepository(connectionStringFactory)
                        AcquisitionsRepo.Add(documentMapping)
                        messageList.AppendLine("UAT/Stage Database Insert Successful")
                    Case "Production"
                        Dim connectionString = ConfigurationManager.ConnectionStrings("AcquisitionsPRD").ConnectionString
                        Dim connectionStringFactory = New mwa.DataAccess.Common.ConnectionStringFactory(connectionString)

                        AcquisitionsRepo = New DocumentMappingIncomingRepository(connectionStringFactory)
                        AcquisitionsRepo.Add(documentMapping)
                        messageList.AppendLine("Production Database Insert Successful")
                    Case Else
                        messageList.AppendLine("Invalid Environment passed in: " & env.ToString)

                End Select

            Next

            Me.lblErrorMessages.Text = messageList.ToString

        End If



    End Sub



    Private Function ValidateInput() As Boolean

        Dim hasErrors As Boolean = False
        Dim errorList As New StringBuilder

        Dim mwaDocProcess As String = txtMWADocProcess.Text
        Dim mwaDocType As String = txtMWADocType.Text
        Dim description As String = txtDescription.Text



        If (mwaDocProcess IsNot Nothing) Then
            If (mwaDocProcess.Length = 0) Then
                errorList.AppendLine("MWA Doc Process must have a value")
                hasErrors = True
            End If
        Else
            errorList.AppendLine("MWA Doc Process must have a value")
            hasErrors = True
        End If

        If (mwaDocType IsNot Nothing) Then
            If (mwaDocType.Length = 0) Then
                errorList.AppendLine("MWA Doc Type must have a value")
                hasErrors = True
            End If
        Else
            errorList.AppendLine("MWA Doc Type must have a value")
            hasErrors = True
        End If

        If (description IsNot Nothing) Then
            If (description.Length = 0) Then
                errorList.AppendLine("Description must have a value")
                hasErrors = True
            End If
        Else
            errorList.AppendLine("Description must have a value")
            hasErrors = True
        End If

        If (Me.lbEnvironment.SelectedItems.Count = 0) Then
            errorList.AppendLine("An environment must be selected")
            hasErrors = True
        End If


        Me.lblErrorMessages.Text = errorList.ToString

        Return hasErrors


    End Function


End Class
