create table Plan_Class(
 Id int identity(1,1),
 ClassName nvarchar(50),
   PRIMARY KEY (Id)
);

insert into Plan_Class values ('Preferred');
insert into Plan_Class values ('Standard');
/*
create table Current_COI_Factors(
 PlanCode int,
 ClassId int,
 Gender char(1),
 Age int,
	Y1	decimal(6,4),
	Y2	decimal(6,4),
	Y3	decimal(6,4),
	Y4	decimal(6,4),
	Y5	decimal(6,4),
	Y6	decimal(6,4),
	Y7	decimal(6,4),
	Y8	decimal(6,4),
	Y9	decimal(6,4),
	Y10	decimal(6,4),
	Y11	decimal(6,4),
	Y12	decimal(6,4),
	Y13	decimal(6,4),
	Y14	decimal(6,4),
	Y15	decimal(6,4),
	Y16	decimal(6,4),
	Y17	decimal(6,4),
	Y18	decimal(6,4),
	Y19	decimal(6,4),
	Y20	decimal(6,4),
	Y21	decimal(6,4),
	Y22	decimal(6,4),
	Y23	decimal(6,4),
	Y24	decimal(6,4),
	Y25	decimal(6,4),
	Y26	decimal(6,4),
	Y27	decimal(6,4),
	Y28	decimal(6,4),
	Y29	decimal(6,4),
	Y30	decimal(6,4),
	Y31	decimal(6,4),
	Y32	decimal(6,4),
	Y33	decimal(6,4),
	Y34	decimal(6,4),
	Y35	decimal(6,4),
	Y36	decimal(6,4),
	Y37	decimal(6,4),
	Y38	decimal(6,4),
	Y39	decimal(6,4),
	Y40	decimal(6,4),
	Y41	decimal(6,4),
	Y42	decimal(6,4),
	Y43	decimal(6,4),
	Y44	decimal(6,4),
	Y45	decimal(6,4),
	Y46	decimal(6,4),
	Y47	decimal(6,4),
	Y48	decimal(6,4),
	Y49	decimal(6,4),
	Y50	decimal(6,4),
	Y51	decimal(6,4),
	Y52	decimal(6,4),
	Y53	decimal(6,4),
	Y54	decimal(6,4),
	Y55	decimal(6,4),
	Y56	decimal(6,4),
	Y57	decimal(6,4),
	Y58	decimal(6,4),
	Y59	decimal(6,4),
	Y60	decimal(6,4),
	Y61	decimal(6,4),
	Y62	decimal(6,4),
	Y63	decimal(6,4),
	Y64	decimal(6,4),
	Y65	decimal(6,4),
	Y66	decimal(6,4),
	Y67	decimal(6,4),
	Y68	decimal(6,4),
	Y69	decimal(6,4),
	Y70	decimal(6,4),
	Y71	decimal(6,4),
	Y72	decimal(6,4),
	Y73	decimal(6,4),
	Y74	decimal(6,4),
	Y75	decimal(6,4),
	Y76	decimal(6,4),
	Y77	decimal(6,4),
	Y78	decimal(6,4),
	Y79	decimal(6,4),
	Y80	decimal(6,4),
	Y81	decimal(6,4),
	Y82	decimal(6,4),
	Y83	decimal(6,4),
	Y84	decimal(6,4),
	Y85	decimal(6,4),
	Y86	decimal(6,4),
	Y87	decimal(6,4),
	Y88	decimal(6,4),
	Y89	decimal(6,4),
	Y90	decimal(6,4),
	Y91	decimal(6,4),
	Y92	decimal(6,4),
	Y93	decimal(6,4),
	Y94	decimal(6,4),
	Y95	decimal(6,4),
	Y96	decimal(6,4),
	Y97	decimal(6,4),
	Y98	decimal(6,4),
	Y99	decimal(6,4),
	Y100	decimal(6,4),
	Y101	decimal(6,4),
	Y102	decimal(6,4),
	Y103	decimal(6,4),
	Y104	decimal(6,4),
	Y105	decimal(6,4),
	Y106	decimal(6,4),
	Y107	decimal(6,4),
	Y108	decimal(6,4),
	Y109	decimal(6,4),
	Y110	decimal(6,4),
	Y111	decimal(6,4),
	Y112	decimal(6,4),
	Y113	decimal(6,4),
	Y114	decimal(6,4),
	Y115	decimal(6,4),
	Y116	decimal(6,4),
	Y117	decimal(6,4),
	Y118	decimal(6,4),
	Y119	decimal(6,4),
	Y120	decimal(6,4),
	PRIMARY KEY (PlanCode, ClassId),
	CONSTRAINT FK_ClassId FOREIGN KEY (ClassId)
	REFERENCES Plan_Class(Id)
);

*/