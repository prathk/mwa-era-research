create table ModalTypes
( 
   Id int identity(1,1),
   [Type] nvarchar(255),
       PRIMARY KEY (Id)
);

create table ModalFactors
(
   ModalTypeId int,
   PlanCode int,
   Factor decimal(18,4),
   PRIMARY KEY (ModalTypeId, PlanCode),
	CONSTRAINT FK_ModalTypeId FOREIGN KEY (ModalTypeId)
	REFERENCES ModalTypes(Id)
);

insert into ModalTypes Values ('Semiannual');
insert into ModalTypes Values ('Monthly');
insert into ModalTypes Values ('Quarterly');
insert into ModalTypes Values ('ABC');

--2,3 and 5 plans
insert into ModalFactors values (1,2000,0.517);
insert into ModalFactors values (2,2000,0.095);
insert into ModalFactors values (3,2000,0.259);
insert into ModalFactors values (4,2000,0.0865);
insert into ModalFactors values (1,3000,0.517);
insert into ModalFactors values (2,3000,0.095);
insert into ModalFactors values (3,3000,0.259);
insert into ModalFactors values (4,3000,0.0865);
insert into ModalFactors values (1,5000,0.517);
insert into ModalFactors values (2,5000,0.095);
insert into ModalFactors values (3,5000,0.259);
insert into ModalFactors values (4,5000,0.0865);

insert into ModalFactors values (1,1153,0.51);
insert into ModalFactors values (2,1153,0.0933);
insert into ModalFactors values (3,1153,0.26);
insert into ModalFactors values (4,1153,0.0833);

insert into ModalFactors values (1,1154,0.51);
insert into ModalFactors values (2,1154,0.0933);
insert into ModalFactors values (3,1154,0.26);
insert into ModalFactors values (4,1154,0.0833);

insert into ModalFactors values (1,1167,0.51);
insert into ModalFactors values (2,1167,0.0933);
insert into ModalFactors values (3,1167,0.26);
insert into ModalFactors values (4,1167,0.0833);

insert into ModalFactors values (1,1169,0.51);
insert into ModalFactors values (2,1169,0.0933);
insert into ModalFactors values (3,1169,0.26);
insert into ModalFactors values (4,1169,0.0833);

insert into ModalFactors values (1,1171,0.51);
insert into ModalFactors values (2,1171,0.0933);
insert into ModalFactors values (3,1171,0.26);
insert into ModalFactors values (4,1171,0.0833);

insert into ModalFactors values (1,1168,0.51);
insert into ModalFactors values (2,1168,0.09);
insert into ModalFactors values (3,1168,0.26);
insert into ModalFactors values (4,1168,0.0833);

insert into ModalFactors values (1,1193,0.51);
insert into ModalFactors values (2,1193,0.09);
insert into ModalFactors values (3,1193,0.26);
insert into ModalFactors values (4,1193,0.0833);