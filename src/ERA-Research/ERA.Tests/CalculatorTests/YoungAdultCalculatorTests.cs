﻿using System;
using ERA.Core;
using ERA.Core.Calculators;
using ERA.Core.Entities;
using ERA.Core.Plans;
using Shouldly;
using Xunit;

namespace ERA.Tests.CalculatorTests
{
    public class YoungAdultCalculatorTests
    {
        [Fact]
        public void Less_Than_27_Year_Should_Be_30()
        {
            var systemClock = new TestSystemClock();
            var factory = new CalculatorFactory(systemClock);
            var calculator = factory.GetCalculatorFor<P1193>();
            var policy = new Policy
            {
                IssueDate = new DateTime(2006, 1, 1),
                PrimIssueAge = 1
            };

            calculator.AnnualBasePremium(policy).ShouldBe(30);
        }

        [Fact]
        public void Greater_Than_27_And_Less_Than_65_Year_Should_Be_162()
        {
            var systemClock = new TestSystemClock();
            var factory = new CalculatorFactory(systemClock);
            var calculator = factory.GetCalculatorFor<P1193>();

            var policy = new Policy
            {
                IssueDate = new DateTime(2006, 1, 1),
                PrimIssueAge = 25
            };

            calculator.AnnualBasePremium(policy).ShouldBe((decimal)162.50);
        }

        [Fact]
        public void Age_27_Should_Be_162()
        {
            var systemClock = new TestSystemClock();
            var factory = new CalculatorFactory(systemClock);
            var calculator = factory.GetCalculatorFor<P1193>();

            var policy = new Policy
            {
                IssueDate = new DateTime(2016, 1, 1),
                PrimIssueAge = 25
            };

            calculator.AnnualBasePremium(policy).ShouldBe((decimal)162.50);
        }
        [Fact]
        public void Age_64_Should_Be_162()
        {
            var systemClock = new TestSystemClock();
            var factory = new CalculatorFactory(systemClock);
            var calculator = factory.GetCalculatorFor<P1193>();

            var policy = new Policy
            {
                IssueDate = new DateTime(1979, 1, 1),
                PrimIssueAge = 25
            };

            calculator.AnnualBasePremium(policy).ShouldBe((decimal)162.50);
        }
        [Fact]
        public void Age_65_Should_Be_0()
        {
            var systemClock = new SystemClock();
            var factory = new CalculatorFactory(systemClock);
            var calculator = factory.GetCalculatorFor<P1193>();

            var policy = new Policy
            {
                IssueDate = new DateTime(1978, 1, 1),
                PrimIssueAge = 25
            };

            calculator.AnnualBasePremium(policy).ShouldBe(0);
        }

    }
}
