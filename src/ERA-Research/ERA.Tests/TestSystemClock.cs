﻿using System;
using ERA.Core;

namespace ERA.Tests
{
    public class TestSystemClock: ISystemClock
    {
        private DateTime _now;

        public TestSystemClock()
        {
            _now = DateTime.Now;
        }

        public void SetNow(DateTime nowDateTime) => _now = nowDateTime;

        public DateTime GetNow()
        {
            return _now;
        }
    }
}
