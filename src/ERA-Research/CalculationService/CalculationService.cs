﻿using System;

namespace CalculationService
{
   internal class CalculationService
    {
        public bool Start()
        {
            Console.WriteLine("--------------------------------");
            Console.WriteLine("Premium Service Started...");
            Console.WriteLine("--------------------------------");
            return true;
        }

        public bool Stop()
        {
            return true;
        }

    }
}
