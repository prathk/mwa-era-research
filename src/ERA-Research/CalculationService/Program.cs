﻿using CalculationService.Jobs;
using ERA.Core.Infrastructure;
using Quartz;
using StructureMap;
using Topshelf;
using Topshelf.Quartz.StructureMap;
using Topshelf.StructureMap;

namespace CalculationService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            HostFactory.Run(c =>
            {
                var container = new Container(new ServiceRegistry());

                // Init StructureMap container
                c.UseStructureMap(container);

                c.UseLog4Net("log4net.config");

                c.Service<CalculationService>(s =>
                {
                    s.ConstructUsingStructureMap();

                    //Construct topshelf service instance with StructureMap
                    s.WhenStarted((service, control) => service.Start());
                    s.WhenStopped((service, control) => service.Stop());

                    //Construct IJob instance with StructureMap
                    s.UseQuartzStructureMap();

                    s.ScheduleQuartzJob(q =>
                        q.WithJob(() =>
                                JobBuilder.Create<MonthiversaryCalculations>().Build())
                            .AddTrigger(() =>
                                TriggerBuilder.Create()
                                    .WithSimpleSchedule(builder => builder
                                        .WithIntervalInMinutes(5)
                                        .RepeatForever())
                                    .Build())
                    );
                });

                //c.SetServiceName(Const.ServiceName);
                //c.SetDisplayName("ERA Calculation Service");
                //c.SetDescription("Runs various jobs for plan valuations");
                //c.EnablePauseAndContinue();
                //c.StartAutomatically();
                //c.RunAsPrompt();
            });
        }
    }
}
