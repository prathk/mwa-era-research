﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using ERA.Core;
using ERA.Core.Infrastructure;
using ERA.Core.Infrastructure.DataAccess;
using log4net;
using Quartz;

namespace CalculationService.Jobs
{
    internal class MonthiversaryCalculations : IJob
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly PolicyRepository _policyRepository;
        private readonly PlanDictionary _planDictionary;
        private readonly ISystemClock _systemClock;

        public MonthiversaryCalculations(PolicyRepository policyRepository, PlanDictionary planDictionary, ISystemClock systemClock)
        {
            _policyRepository = policyRepository;
            _planDictionary = planDictionary;
            _systemClock = systemClock;
        }

        Task IJob.Execute(IJobExecutionContext context)
        {
            Log.Debug($"MonthiversaryCalculations Started");

            var allPolicies = _policyRepository.GetAllPolicies();

            //Debug - uncomment the following line, to make it easier to test just certain plan codes
            allPolicies = allPolicies.Where(x => x.Plancode == 1193);

            foreach (var policy in allPolicies)
            {
                Log.Info($"Processing Policy {policy.PolicyNumber}: Plan Code  {policy.Plancode}, Issue Date  {policy.IssueDate}");

                var isAnniversary = policy.IssueDate.Date == _systemClock.GetNow().Date; //TODO: Leap Years
                var isMonthiversary = policy.IssueDate.Day == _systemClock.GetNow().Day; //TODO: Leap Years

                isAnniversary = true; //debug to make it always run, to make premium calcs more useful
                isMonthiversary = true; //debug to make it always run, to make premium calcs more useful

                Log.Debug($"Anniversary? {isAnniversary}");
                Log.Debug($"Monthiversary? {isMonthiversary}");
                if (isAnniversary)
                {
                    //Illustrations? (not for ERA, but noteworthy)
                    if (isMonthiversary)
                    {
                        //Billing
                        if (!_planDictionary.ContainsKey(policy.Plancode))
                        {
                            Log.Warn("No calculator is defined for this plan code, continuing.");
                            continue;
                        }
                        var planCalculations = _planDictionary[policy.Plancode];

                        planCalculations.CalculateAndSetNewPremium(policy);
                        _policyRepository.UpdatePolicyToDb(policy);
                        if (policy.PremAmount != policy.NewPremium)
                        {
                            Log.Error($"Premium discrepency for {policy.PremAmount} doesn't match expected {policy.NewPremium}");
                        }
                    }
                }
                else
                {
                    Log.Debug($"Not a *versary; No calculations required.");
                    continue;
                }
            }

            Log.Debug("Calculate Premium job executed.");
            return Task.CompletedTask;
        }
    }
}
