﻿namespace BillingHelper
{
    public class Model
    {
        public string CertNumber { get; set; }

        public string Amount { get; set; }

        public ModelType Type { get; set; }

    }

    public enum ModelType
    {
        Ips = 0,
        Lis,
        None
    }


}
