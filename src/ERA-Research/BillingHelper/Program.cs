﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using OfficeOpenXml;

namespace BillingHelper
{
    public class Program
    {
        private static readonly Dictionary<string, int> CertLookupDictionary = new Dictionary<string, int>();
        private static readonly Dictionary<string, int> LoanCertLookupDictionary = new Dictionary<string, int>();

        public static void Main(string[] args)
        {
            Start();
        }

        private static void Start()
        {

            using (var package = new ExcelPackage(new FileInfo(GetExcelFileName())))
            {
                var worksheet = package.Workbook.Worksheets[1];
                var insuranceStaWorksheet = package.Workbook.Worksheets["Loan Insurance Statement"] ?? package.Workbook.Worksheets.Add("Loan Insurance Statement");
                insuranceStaWorksheet.Cells[1, 1].Value = "Policy Number";
                AddExistingCertificatestoDictionary(worksheet);

                const int row = 2;
                var col = 10;
                var col2 = 2;
                foreach (var fname in Directory.EnumerateFiles(AssemblyDirectory, "*.pdf"))
                {

                    var dateMatchFromFileName = Regex.Match(fname, "[0-9]{1,2}\\-[0-9]{1,2}\\-[0-9][0-9]");
                    if (!dateMatchFromFileName.Success) continue;

                    var parts = dateMatchFromFileName.Value.Split('-');
                    var date = new DateTime(int.Parse("20" + parts[2]),
                        int.Parse(parts[0]),
                        int.Parse(parts[1]));

                    worksheet.Cells[row, col].Value = date.ToShortDateString();
                    worksheet.Cells[row, col].Style.Font.SetFromFont(new Font("Tahoma", 11, FontStyle.Bold));
                    insuranceStaWorksheet.Cells[1, col2].Value = date.ToShortDateString();

                    var fileValues = ReadPdfFile(fname, date);

                    foreach (var fv in fileValues)
                    {
                        switch (fv.Type)
                        {
                            case ModelType.Ips:
                                if (!CertLookupDictionary.ContainsKey(fv.CertNumber))
                                {
                                    var newRow = CertLookupDictionary.Keys.Count + 2;
                                    worksheet.Cells[newRow, 2].Value = fv.CertNumber;
                                    worksheet.Cells[newRow, col].Value = fv.Amount;
                                    CertLookupDictionary.Add(fv.CertNumber, newRow);

                                }
                                else
                                {
                                    var certRow = CertLookupDictionary[fv.CertNumber];
                                    worksheet.Cells[certRow, col].Value = fv.Amount;
                                }

                                break;
                            case ModelType.Lis:
                                AddLoanInsuranceStatement(insuranceStaWorksheet, fv, col2);
                                break;
                            case ModelType.None:
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }

                    col++;
                    col2++;

                }

                worksheet.Cells[row, col].Value = "Reviewed?";
                worksheet.Cells[row, col].Style.Font.SetFromFont(new Font("Tahoma", 11, FontStyle.Bold));
                //worksheet.Cells.AutoFitColumns(50);
                package.Save();

            }

        }

        private static void AddLoanInsuranceStatement(ExcelWorksheet worksheet, Model model, int col)
        {
            if (!LoanCertLookupDictionary.ContainsKey(model.CertNumber))
            {
                var newRow = LoanCertLookupDictionary.Keys.Count + 2;
                worksheet.Cells[newRow, 1].Value = model.CertNumber;
                worksheet.Cells[newRow, col].Value = model.Amount;
                LoanCertLookupDictionary.Add(model.CertNumber, newRow);

            }
            else
            {
                var certRow = LoanCertLookupDictionary[model.CertNumber];
                worksheet.Cells[certRow, col].Value = model.Amount;
            }
        }

        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return System.IO.Path.GetDirectoryName(path);
            }
        }

        private static void AddExistingCertificatestoDictionary(ExcelWorksheet worksheet)
        {
            var startRowNum = 3;
            const int certColumnNum = 2;

            while (worksheet.Cells[startRowNum, certColumnNum].Value != null)
            {
                CertLookupDictionary.Add(worksheet.Cells[startRowNum, certColumnNum].Value.ToString(), startRowNum);
                startRowNum++;
            }
        }

        private static string GetExcelFileName()
        {
            return Directory.EnumerateFiles(AssemblyDirectory, "*.xlsx").First();
        }


        public static List<Model> ReadPdfFile(string fileName, DateTime d)
        {

            var toRet = new List<Model>();



            var pdfReader = new PdfReader(fileName);

            for (var page = 1; page <= pdfReader.NumberOfPages; page++)
            {
                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                var currentText = PdfTextExtractor.GetTextFromPage(pdfReader, page, strategy);

                currentText = Encoding.UTF8.GetString(Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.UTF8.GetBytes(currentText)));

                var isInsurancePremiumStatement = Regex.Match(currentText, "insurance premium statement");
                Match isLoanStatement = null;

                if (!isInsurancePremiumStatement.Success)
                {
                    isLoanStatement = Regex.Match(currentText, "loan interest statement");

                    if (!isLoanStatement.Success) continue;

                };

                var amt = Regex.Match(currentText, "(?:EUR|[$])\\s*\\d+(?:\\.\\d{2})?");

                var certNumber = Regex.Match(currentText, "(?:CONTRACTINDEX:)*[0-9]+");

                if (certNumber.Success && amt.Success)
                {
                    toRet.Add(new Model
                    {
                        Amount = amt.Value,
                        CertNumber = certNumber.Value,
                        Type = isInsurancePremiumStatement.Success ? ModelType.Ips : isLoanStatement.Success ? ModelType.Lis : ModelType.None

                    });
                }

            }
            pdfReader.Close();
            return toRet;
        }
    }
}
