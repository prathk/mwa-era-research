﻿using System.Collections.Generic;
using ERA.Core.Entities;

namespace ERA.Core.Infrastructure.DataAccess
{
    public class PolicyRepository: BaseRepository
    {

        private const string PoliciesByPlanCode =
        @"select
        a.Policy_Number,
        a.PLANCODE,
        a.PRODUCT,
        a.PRIM_ISSUE_AGE,
        a.ISSUE_DATE,
        a.TERM_DATE,
        a.DEATH_DATE,
        a.DEATH_NOTICE,
        PREM_PAID_DATE,
        PREM_AMOUNT,
        PREM_MODE,
        [STATUS],
        a.CONT_STATUS
        from  PMWER_DW_Policy a
        inner join PMWER_LIFE_RESERVE b
        on a.POLICY_NUMBER = b.POL_NUM
            where PLANCODE = @Id";

        private const string Policies =
        @"select
        a.Policy_Number,
        a.PLANCODE,
        a.PRODUCT,
        a.PRIM_ISSUE_AGE,
        a.ISSUE_DATE,
        a.TERM_DATE,
        a.DEATH_DATE,
        a.DEATH_NOTICE,
        PREM_PAID_DATE,
        PREM_AMOUNT,
        PREM_MODE,
        [STATUS],
        a.CONT_STATUS
        from  PMWER_DW_Policy a
        inner join PMWER_LIFE_RESERVE b
        on a.POLICY_NUMBER = b.POL_NUM
		ORDER BY PlanCode, [STATUS], ISSUE_DATE
        ";


        public IEnumerable<Policy> GetAllPoliciesByPlanCode(int planCode) => GetAllById<Policy, int>(planCode, PoliciesByPlanCode);
        public IEnumerable<Policy> GetAllPolicies() => GetAll<Policy>(Policies);

        public void UpdatePolicyToDb(Policy policy) => UpdateData<Policy>(policy);
    }
}
