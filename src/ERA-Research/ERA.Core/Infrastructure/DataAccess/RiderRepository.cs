﻿using System.Collections.Generic;
using ERA.Core.Entities;

namespace ERA.Core.Infrastructure.DataAccess
{
    public class RiderRepository: BaseRepository
    {
        private const string GetRidersByPolicyId =
            @"select
                CONT,
                STATUS,
                RIDER_TYPE_ID,
                RIDER_TYPE_DESC,
                RIDER_DATE,
                PREM,
                PREM_MODE
                from PMWER_Crider
                where CONT = @Id";

        public IEnumerable<Rider> GetAllRidersForPolicy(int policyNumber) => GetAllById<Rider, int>(policyNumber, GetRidersByPolicyId);

    }
}
