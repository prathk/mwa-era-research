﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using NPoco;

namespace ERA.Core.Infrastructure.DataAccess
{
    //ToDO: Use EF.
    public abstract class BaseRepository
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["Mwa_Era"].ConnectionString;

        protected IDatabase Connection => new Database(_connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);

        protected IEnumerable<TR> GetAll<TR>(string sql)
        {
            using (var db = Connection)
            {
                return db.Fetch<TR>(sql);
            }
        }
        protected IEnumerable<TR> GetAllById<TR, TQ>(TQ param, string sql)
        {
            using (var db = Connection)
            {
                return db.Fetch<TR>(sql, new { Id = param });
            }
        }
        protected TR GetSingleById<TR, TQ>(TQ param, string sql)
        {
            using (var db = Connection)
            {
                return db.SingleOrDefault<TR>(sql, new { Id = param });
            }
        }

        protected void UpdateData<T>(T data)
        {
            using (var db = Connection)
            {
                db.Update(data);
            }
        }

    }
}
