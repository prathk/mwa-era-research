﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ERA.Core.Plans;
using StructureMap;

namespace ERA.Core.Infrastructure
{
    public class PlanDictionary : Dictionary<int,IPlan>
    {
        public PlanDictionary(IContainer container)
        {
            var types = Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => t.IsClass && t.GetInterfaces().Contains(typeof(IPlan)));

            foreach (var t in types)
            {
                var instance = (IPlan) container.GetInstance(t);
                this.Add(instance.GetPlanCode(), instance);
            }
        }
    }
}
