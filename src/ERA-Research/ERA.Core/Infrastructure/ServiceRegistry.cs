﻿using System.Linq;
using System.Reflection;
using ERA.Core.Plans;
using StructureMap;

namespace ERA.Core.Infrastructure
{
    public class ServiceRegistry: Registry
    {
        public ServiceRegistry()
        {
            Scan(s =>
            {
                s.TheCallingAssembly();
                s.WithDefaultConventions();
            });

            ForSingletonOf<PlanDictionary>();
            var types = Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => t.IsClass && t.GetInterfaces().Contains(typeof(IPlan)));
            foreach (var t in types)
            {
                ForSingletonOf(t);
            }

        }
    }
}
