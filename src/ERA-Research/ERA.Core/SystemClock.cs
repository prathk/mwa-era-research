﻿using System;

namespace ERA.Core
{
    public interface ISystemClock
    {
        DateTime GetNow();
    }

    public class SystemClock : ISystemClock
    {
        public DateTime GetNow()
        {
//            return new DateTime(2018, 6, 19);// Billing Manual Check date;
            return new DateTime(2018, 6, 26);// SE2 snapshot date;
            return DateTime.Now;
        }
    }
}