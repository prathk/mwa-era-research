﻿using System.Collections.Generic;
using ERA.Core.Entities;

namespace ERA.Core.Plans
{
    public interface IPlan
    {
        int GetPlanCode();
        void CalculateAndSetNewPremium(Policy policy);
    }
}
