﻿using System;
using System.Linq;
using System.Reflection;
using ERA.Core.Calculators;
using ERA.Core.Entities;
using ERA.Core.Infrastructure.DataAccess;
using log4net;

namespace ERA.Core.Plans
{
    public class P1193 : IPlan
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly RiderRepository _riderRepository;
        private readonly YoungAdultCalculator _youngAdultCalculator;

        public P1193(RiderRepository riderRepository, ICalculatorFactory calculatorFactory)
        {
            _riderRepository = riderRepository;
            _youngAdultCalculator = (YoungAdultCalculator)calculatorFactory.GetCalculatorFor<P1193>();
        }

        int IPlan.GetPlanCode()
        {
            return 1193;
        }

        public void CalculateAndSetNewPremium(Policy policy)
        {
            var annualPremium = _youngAdultCalculator.AnnualBasePremium(policy);

            var riderPremium = GetRiderPremium(policy.PolicyNumber);
            Log.Debug($"Rider Premium Amount (modal adjusted): {riderPremium}");

            switch (policy.PremMode)
            {
                case "A":
                    policy.NewPremium = annualPremium + riderPremium;
                    break;
                case "Q":
                    policy.NewPremium = (annualPremium * (decimal)0.25) + riderPremium;
                    break;
                case "M":
                    policy.NewPremium = (annualPremium /12) + riderPremium;
                    break;
                case "DO NOT BILL":
                    policy.NewPremium = 0;
                    break;
                case "S":
                    policy.NewPremium = (annualPremium * (decimal)0.51) + riderPremium;
                    break;
                default: throw new Exception($"Premium mode not mapped for policy {policy.PolicyNumber}");
            }
            policy.NewPremium = Math.Round(policy.NewPremium, 2);

            Log.Debug($"Modal Premium Amount: {policy.NewPremium}");

        }

        private decimal GetRiderPremium(int policyNumber)
        {
            var riders = _riderRepository.GetAllRidersForPolicy(policyNumber);
            var distinctRiders = riders
                .Where(r => r.RiderPremium > 0)
                .GroupBy(r => r.RiderPremium)
                .Select(r => r.First())
                .ToList();

            if (distinctRiders.Count > 1)
            {
                throw new Exception($"Multiple Rider Premiums Found for policy {policyNumber}");
            }
            else if (distinctRiders.Count == 0)
            {
                return 0;
            }
            else
            {
                return distinctRiders[0].RiderPremium;
            }
        }
    }
}