﻿using System;
using ERA.Core.Plans;

namespace ERA.Core.Calculators
{
    public class CalculatorFactory : ICalculatorFactory
    {
        private readonly ISystemClock _systemClock;

        public CalculatorFactory(ISystemClock systemClock)
        {
            _systemClock = systemClock;
        }
        public ICalculator GetCalculatorFor<T>()
        {
            if (typeof(T) == typeof(P1193)) return new YoungAdultCalculator(_systemClock, 27, 65, (decimal) 30, (decimal) 162.50);

            throw new Exception($"Creation of {typeof(T)} is not supported yet");
        }
}
}
