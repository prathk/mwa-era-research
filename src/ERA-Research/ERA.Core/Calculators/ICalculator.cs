﻿using ERA.Core.Entities;

namespace ERA.Core.Calculators
{
    public interface ICalculator
    {
        decimal AnnualBasePremium(IPremiumContext policy);
    }
}
