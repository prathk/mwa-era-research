﻿namespace ERA.Core.Calculators
{
    public interface ICalculatorFactory
    {
        ICalculator GetCalculatorFor<T>();
    }
}