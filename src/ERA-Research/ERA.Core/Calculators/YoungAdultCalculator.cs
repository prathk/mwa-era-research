﻿using System;
using System.Reflection;
using ERA.Core.Entities;
using log4net;

namespace ERA.Core.Calculators
{
    public class YoungAdultCalculator: ICalculator
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ISystemClock _systemClock;

        private readonly int _age1;
        private readonly int _age2;
        private readonly decimal _premium1;
        private readonly decimal _premium2;

        public YoungAdultCalculator(ISystemClock systemClock, int age1, int age2, decimal premium1, decimal premium2)
        {
            _systemClock = systemClock;
            _age1 = age1;
            _age2 = age2;
            _premium1 = premium1;
            _premium2 = premium2;
        }

        public decimal AnnualBasePremium(IPremiumContext policy)
        {
            var zeroTime = new DateTime(1, 1, 1);

            var span = _systemClock.GetNow() - policy.IssueDate;

            var policyYear = (zeroTime + span).Year - 1;

            var attainedAge = policy.PrimIssueAge + policyYear;

            decimal annualPremium = 0;
            if (attainedAge < _age1)
                annualPremium = _premium1;
            else if (attainedAge >= _age1 && attainedAge < _age2)
                annualPremium = _premium2;
            else if (attainedAge >= _age2)
                annualPremium = 0;

            Log.Debug($"Annual Premium Based Amount: {annualPremium}");
            return annualPremium;
        }
    }
}