﻿using System;

namespace ERA.Core.Entities
{
    public class Transaction
    {

        public Type TransactionType { get; set; }

        public  DateTime TransactionDate { get; set; }

        public decimal TransactionAmount { get; set; }

        public enum Type
        {
            Premium = 0,
            Anniversary
        }
    }
}
