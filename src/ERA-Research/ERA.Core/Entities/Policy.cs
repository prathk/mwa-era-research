﻿using System;
using NPoco;

namespace ERA.Core.Entities
{
    [TableName("PMWER_DW_Policy")]
    [PrimaryKey("Policy_Number")]
    public class Policy : IPremiumContext
    {
        [Column("Policy_Number")] public int PolicyNumber { get; set; }

        [Column("PLANCODE")] public int Plancode { get; set; }

        [Column("PRODUCT")] public string Product { get; set; }

        [Column("PRIM_ISSUE_AGE")] public int PrimIssueAge { get; set; }

        [Column("ISSUE_DATE")] public DateTime IssueDate { get; set; }

        [Column("TERM_DATE")] public DateTime TermDate { get; set; }

        [Column("DEATH_DATE")] public DateTime DeathDate { get; set; }

        [Column("DEATH_NOTICE")] public DateTime DeathNotice { get; set; }

        [Column("PREM_PAID_DATE")] public DateTime PremPaidDate { get; set; }

        [Column("PREM_AMOUNT")] public decimal PremAmount { get; set; }

        [Column("PREM_MODE")] public string PremMode { get; set; }

        [ResultColumn] public string Status { get; set; }

        [Column("CONT_STATUS")] public string ContStatus { get; set; }

        [Column("NEW_PREMIUM")] public decimal NewPremium { get; set; }
    }
}