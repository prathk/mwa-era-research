﻿namespace ERA.Core.Entities
{
    public enum PremiumMode
    {
        Annually,
        Semiannually,
        Quarterly,
        Monthly
    }
}
