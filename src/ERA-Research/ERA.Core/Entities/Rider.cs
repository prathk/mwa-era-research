﻿using System;
using NPoco;

namespace ERA.Core.Entities
{
    [TableName("PMWER_Crider")]
    public class Rider
    {
        [Column("CONT")] public int PolicyNumber { get; set; }

        [Column("STATUS")] public string Status { get; set; }

        [Column("RIDER_TYPE_ID")] public string RiderTypeId { get; set; }

        [Column("RIDER_TYPE_DESC")] public string RiderTypeDesc { get; set; }

        [Column("RIDER_DATE")] public DateTime RiderDate { get; set; }

        [Column("PREM")] public decimal RiderPremium { get; set; }

        [Column("PREM_MODE")] public string PremiumMode { get; set; }

    }
}
