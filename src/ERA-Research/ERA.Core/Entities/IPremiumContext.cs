﻿using System;

namespace ERA.Core.Entities
{
    public interface IPremiumContext
    {
        int PrimIssueAge { get; set; }
        DateTime IssueDate { get; set; }
        string PremMode { get; set; }
    }
}